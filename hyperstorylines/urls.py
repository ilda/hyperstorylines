from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from . import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', TemplateView.as_view(template_name='hyperstorylines/hyperstorylines.html'), name='hyperstorylines_start'),
    path('search/', views.search, name='search'),
    path('datasets-available/', views.get_available_datasets, name='available_datasets'),
    path('get_data/', views.get_data, name='get_data'),
    path('get_source/', views.get_source, name='get_source'),
    path('get_scenes/', views.get_scenes, name='get_scenes'),
    path('get_nested_scenes/', views.get_nested_scenes, name='get_nested_scenes'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)