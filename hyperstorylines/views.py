from os import confstr
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
import datetime
from .index import Index
import json
import re
from django.templatetags.static import static
from hyperstorylines.libs.scenes.generator import ScenesGenerator
from hyperstorylines.libs.scenes.generator_nested import NestedScenesGenerator
from hyperstorylines.libs.scenes.ids_cleaner import IdsCleaner
from pathlib import Path


def search(request):
    if request.method == 'GET':
        query = request.GET.get('q', None)
        index_name = request.GET.get('i', None)
        if query is not None and index_name is not None:
            i = Index(index_name.lower())
            sources_ids = i.search(query)
            sources_ids = list(set(sources_ids))
            data = {'sources_ids': sources_ids}
            return HttpResponse(json.dumps(data), content_type='application/json')


def get_available_datasets(request):
    txt_folder = Path(static('hyperstorylines/datasets/storylines/')).rglob('*.json')
    files = [x for x in txt_folder]
    datasets = []
    for f in files:
        datasets.append(f.name)
    datasets.sort()
    return HttpResponse(json.dumps({"datasets": datasets}), content_type='application/json')


def get_data(request):
    if request.method == 'GET':
        name = request.GET.get('q', None)
        if name is not None:
            file_data = open(static('hyperstorylines/datasets/storylines/' + name))
            # TODO: with the good backend, it should be no need to load 
            # the whole dataset just to get the dimensions
            data = json.load(file_data)
            return HttpResponse(json.dumps(data['dimensions']), content_type='application/json')


def get_source(request):
    if request.method == 'GET':
        name = request.GET.get('q', None)
        index_name = request.GET.get('i', None)  # Dataset name
        if name is not None:
            # I AM ASUMING THAT ALL DATASETS ARE IN JSON FORM
            index_name = index_name[:-5]
            file_data = open(
                static('hyperstorylines/datasets/sources/' + index_name + '/' + name))
            data = json.load(file_data)
            return HttpResponse(json.dumps(data), content_type='application/json')


def get_scenes(request):
    if request.method == 'GET':
        name = request.GET.get('name', None)
        dim_x = request.GET.get('xDim', None)
        dim_y = request.GET.get('yDim', None)
        dim_z = request.GET.get('zDim', None)
        aggr_x = request.GET.get('xAggr', None)
        order_x = request.GET.get('xDimOrder', None)

        filtered_entities = request.GET.get('filteredEntities', None)
        if filtered_entities is not None:
            filtered_entities = filtered_entities.split(' ')
        else:
            filtered_entities = []

        terms_for_entities = request.GET.get('termsForEntities', None)
        if terms_for_entities is not None:
            terms_for_entities = terms_for_entities.split(' ')
        else:
            terms_for_entities = []

        terms_for_relationships = request.GET.get(
            'termsForRelationships', None)
        if terms_for_relationships is not None:
            terms_for_relationships = terms_for_relationships.split(' ')
        else:
            terms_for_relationships = []
        if name is not None:
            scenes, nodes_dict = ScenesGenerator().get_scenes(
                name, dim_x, dim_y, dim_z,
                filtered_entities, terms_for_entities, terms_for_relationships,
                aggr_x, order_x)
            return HttpResponse(json.dumps({'scenes': scenes, 'nodes_dict': nodes_dict}), content_type='application/json')


def get_nested_scenes(request):
    if request.method == 'GET':
        name = request.GET.get('name', None)
        dim_x = request.GET.get('xDim', None)
        dim_y = request.GET.get('yDim', None)
        dim_z = request.GET.get('zDim', None)
        aggr_x = request.GET.get('xAggr', None)
        aggr_z = request.GET.get('zAggr', None)
        order_z = request.GET.get('zDimOrder', None)
        scene_edges = request.GET.get('scene_edges', None)
        scene_keynode_name = request.GET.get('keyEntityName', None)

        filtered_entities = request.GET.get('filteredEntities', None)
        if filtered_entities is not None:
            filtered_entities = filtered_entities.split(' ')
        else:
            filtered_entities = []

        terms_for_entities = request.GET.get('termsForEntities', None)
        if terms_for_entities is not None:
            terms_for_entities = terms_for_entities.split(' ')
        else:
            terms_for_entities = []
        terms_for_relationships = request.GET.get(
            'termsForRelationships', None)
        if terms_for_relationships is not None:
            terms_for_relationships = terms_for_relationships.split(' ')
        else:
            terms_for_relationships = []
        if name is not None:
            #file_data = open(static('/datasets/storylines/' + name))
            #data = json.load(file_data)
            scene_edges = scene_edges.split(' ')
            scenes, nodes_dict = NestedScenesGenerator().get_scenes(
                name, dim_x, dim_y, dim_z,
                filtered_entities, terms_for_entities, terms_for_relationships,
                aggr_x, aggr_z, order_z, 
                scene_edges, scene_keynode_name)
            # TODO: TEST THIS:
            #scenes, nodes_dict = IdsCleaner.clean(scenes, nodes_dict)
            return HttpResponse(json.dumps({'scenes': scenes, 'nodes_dict': nodes_dict}), content_type='application/json')
