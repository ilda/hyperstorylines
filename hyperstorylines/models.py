from django.db import models
from django.conf import settings

class City(models.Model):
    # Do we need the codes or with the names is enough?
    name = models.CharField(max_length=200)
    region = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    
