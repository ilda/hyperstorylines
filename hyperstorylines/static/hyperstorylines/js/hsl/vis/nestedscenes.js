import sceneStyle from './style/scenes.js'
import {
  addExtraLinks,
  getOpenLinksSize,
  removeExtraLinks,
} from './nestedlines.js'
import {
  addInnerElements,
  updateInnerElements,
  addInnerXAxis,
} from './nestedinnerelements.js'

export default function nestedScenes() {
  var nestedScenes = {}
  var _activeEdges = []
  var _verticalShifts = {}
  nestedScenes.dimensions = function (dimensions) {
    return nestedScenes
  }

  nestedScenes.unfold = function (
    scene,
    animationDuration,
    nestedScenesData,
    entityInteractions,
    narrative,
    isXDimAggr
  ) {

    const openWidth = getOpenWidth(nestedScenesData, isXDimAggr)
    const openGMargin = 5
    createNestedContainer(scene, nestedScenesData, openWidth, openGMargin)
    const openStory = d3
      .narrative()
      .scenes(nestedScenesData)
      .size([openWidth, scene.height * 2])
      .pathSpace(sceneStyle('pathSpace'))
      .labelSize([0, 0])
      .groupMargin(sceneStyle('groupMargin'))
      .scenePadding([
        5,
        sceneStyle('sceneWidth') / 2,
        5,
        sceneStyle('sceneWidth') / 2,
      ])
      .labelPosition('left')
      .layout()

    addInnerElements(
      scene,
      openStory,
      entityInteractions,
      isXDimAggr
    )
    if (isXDimAggr)
      addInnerXAxis(scene, nestedScenesData, openStory, entityInteractions)
    scene.edge.original_edges_ids.forEach((edgeId) => {
      var index = _activeEdges.indexOf(edgeId)
      if (index < 0) {
        _activeEdges.push(edgeId)
      }
    })
    updateOuterElement(scene, animationDuration, nestedScenesData)
    addExtraLinks(scene, nestedScenesData, openStory, entityInteractions, narrative)
  }
  nestedScenes.fold = function (
    scene,
    animationDuration,
    narrative,
    selections,
  ) {
    scene.edge.original_edges_ids.forEach((edgeId) => {
      var index = _activeEdges.indexOf(edgeId)
      if (index > -1) {
        _activeEdges.splice(index, 1)
      }
    })
    removeExtraLinks(scene, animationDuration, narrative)
    removeOpenScene(scene)
    updateOuterElement(scene, animationDuration)
    d3.select('#hsl-scenes-1')
      .select('#scene_' + scene.id)
      .selectAll('.appearance')
      .attr('visibility', 'visible')
  }

  nestedScenes.getShiftValues = function (scene, direction) {
    var shiftingWidth = scene.openSize.width + 5
    if (scene.isOpen) {
      _verticalShifts[scene.id] = { 'scene': scene, 'value': scene.originalY + scene.openSize.difY }
    } else {
      delete _verticalShifts[scene.id]
    }
    var finalVerticalShift = 0
    var finalVerticalShiftKey
    if (Object.keys(_verticalShifts).length > 1) {
      finalVerticalShiftKey = Object.keys(_verticalShifts).reduce(function (max, key) {
        return (max === undefined || _verticalShifts[key]['value'] > _verticalShifts[max]['value']) ? max : key;
      })
    } else {
      finalVerticalShiftKey = Object.keys(_verticalShifts)[0]
    }

    if (finalVerticalShiftKey !== undefined) {
      const currentShift = _verticalShifts[finalVerticalShiftKey]['value']
      if (currentShift < 0) {
        finalVerticalShift = Math.abs(currentShift)

      }
    }
    return [shiftingWidth * direction, finalVerticalShift]
  }

  nestedScenes.reorderExpandedScenes = function (data, selections) {
    const narrative = data.narrative
    const activeScenes = _active[xDim][yDim]
    _activeEdges.forEach((edgeId) => {
      var scene = narrative.getSceneByEdgeId(edgeId)
      var nestedScenes = data.getNestedScenes(scene, selections)
      const openWidth = getOpenWidth(nestedScenes)
      updateInnerElements(openWidth, scene, nestedScenes)
    })
  }

  nestedScenes.getActiveScenes = function () {
    return _activeEdges
  }

  function getOpenWidth(nestedScenes, isXDimAggr) {
    if (!isXDimAggr) return nestedScenes.length * 30 + getOpenLinksSize()
    else return nestedScenes.length * 40 + getOpenLinksSize() + 195
  }

  function getOpenSceneVerticalOffset() {
    return 10
  }

  function getOpenSceneHorizontalOffset() {
    return 5
  }

  function createNestedContainer(scene, nestedScenes, openWidth) {
    const openStoryGroup = d3
      .select('#hsl-scenes-1')
      .append('g')
      .data([scene])
      .attr('id', 'open-scene-' + scene.id)
      .attr('class', 'open-scene')
      .attr('sceneId', scene.id)
      .attr('transform', 'translate(' + (scene.x + 5) + ',' + scene.y + ')')
      .style('overflow', 'hidden')

    const openStoryGroupExtra0 = d3
      .select('#hsl-scenes-open-0')
      .append('g')
      .data([scene])
      .attr('id', 'open-scene-' + scene.id)
      .attr('class', 'open-scene')
      .attr('sceneId', scene.id)
      .attr('transform', 'translate(' + (scene.x + 5) + ',' + scene.y + ')')
      .style('width', openWidth)
      .style('overflow', 'hidden')

    const openStoryGroupExtra1 = d3
      .select('#hsl-scenes-open-1')
      .append('g')
      .data([scene])
      .attr('id', 'open-scene-' + scene.id)
      .attr('class', 'open-scene')
      .attr('sceneId', scene.id)
      .attr('transform', 'translate(' + (scene.x + 5) + ',' + scene.y + ')')
      .style('width', openWidth)
      .style('overflow', 'hidden')

    const openStoryGroupLines = d3
      .select('#hsl-lines-open-inside-0')
      .append('g')
      .data([scene])
      .attr('id', 'open-scene-' + scene.id)
      .attr('class', 'open-scene')
    //.attr('transform', 'translate(' + (scene.x + 5) + ',' + scene.y + ')')
    const elto = d3.select('#hsl-scenes-1').select('#sceneRect_' + scene.id)
    openStoryGroup.append(function () {
      return elto.node()
    })
    d3.select('#hsl-scenes-1')
      .select('#scene_' + scene.id)
      .selectAll('.appearance')
      .attr('visibility', 'hidden')

    openStoryGroupExtra0
      .append('g')
      .attr('id', 'innerXAxis')
      .attr('transform', 'translate(0, -100)')
    openStoryGroupExtra1
      .append('g')
      .attr('id', 'innerXAxis')
      .attr('transform', 'translate(0, -100)')
    openStoryGroupLines
      .append('g')
      .attr('id', 'innerXAxis')
      .attr('transform', 'translate(0, -100)')
    openStoryGroupExtra1
      .append('g')
      .attr('class', 'extraLinksContainer')
      .attr('transform', 'translate(0,0)')
    openStoryGroupExtra0
      .append('g')
      .attr('class', 'openStoryContainer openStoryContainer_' + scene.id)
      .attr('transform', 'translate(0, 0)')
    openStoryGroupExtra1
      .append('g')
      .attr('class', 'openStoryContainer openStoryContainer_' + scene.id)
      .attr('transform', 'translate(0, 0)')

    d3.select('#hsl-lines-open-inside-0')
      .append('g')
      .attr('id', 'links-open-scene-' + scene.id)

    d3.select('#hsl-lines-open-inside-1')
      .append('g')
      .attr('id', 'links-open-scene-' + scene.id)
  }

  function updateOuterElement(
    scene,
    animationDuration,
    nestedScenes,
    equalAggr,
  ) {
    if (scene.isOpen) {
      var bbox = d3
        .select('#hsl-scenes-open-0')
        .select('#open-scene-' + scene.id)
        .node()
        .getBBox()
      computeSceneOpenSize(scene, nestedScenes)
      d3.select('#scene_' + scene.id)
        .selectAll('.appearance')
        .attr('visibility', 'hidden')
      d3.select('#scene_' + scene.id)
        .selectAll('.nNested')
        .attr('visibility', 'hidden')
    } else {
      d3.select('#scene_' + scene.id)
        .selectAll('.appearance')
        .attr('visibility', 'visible')
      d3.select('#scene_' + scene.id)
        .selectAll('.nNested')
        .attr('visibility', 'visible')
      delete scene.openExtraValues
      delete scene.openSize
    }
    var element = d3.select('#hsl-scenes-1').select('#sceneRect_' + scene.id)
    element.classed('openRectScene', scene.isOpen)
    element
      .transition()
      .duration(animationDuration)
      .attr('width', function (d) {
        if (d.isOpen) {
          return d.openSize.width
        } else {
          return sceneStyle('sceneWidth')
        }
      })
      .attr('height', function (d) {
        if (d.isOpen) {
          return d.openSize.height
        } else {
          return d.height
        }
      })
      .attr('y', function (d) {
        if (d.isOpen) {
          return d.openSize.y
        } else {
          return 0
        }
      })

    if (scene.isOpen) {
      d3.select('#open-scene-' + scene.id)
        .selectAll('.relationshipId')
        .attr('x', bbox.width + getOpenLinksSize() + 20 - 5 + sceneStyle('leftPadding'))
        .attr('y', scene.characters.length * 25 + 35)
        .style('text-anchor', 'end')
        .text('ID: ' + scene.edgeId)

      d3
        .select('#hsl-lines-open-inside-0')
        .select('g#open-scene-' + scene.id)
        .select('#innerXAxis')
        .selectAll('line')
        .attr('y2', (d) => scene.openSize.lineSize + 100)

      d3.select('#hsl-scenes-0').select('#scene_' + scene.id).select('rect').attr('visibility', 'hidden')
    } else {
      d3.select('#hsl-scenes-0').select('#scene_' + scene.id).select('rect').attr('visibility', 'visible')
    }
    d3.select('#open-scene-' + scene.id)
      .select('#innerXAxis')
      .selectAll('line')
      .attr('y2', () => {
        if (bbox != undefined) return bbox.height + 15
        else return 15
      })
  }

  function computeSceneOpenSize(scene, nestedScenes) {
    var bbox = d3
      .select('#hsl-scenes-open-0')
      .select('#open-scene-' + scene.id)
      .node()
      .getBBox()
    var bbox_c = d3
      .select('#hsl-scenes-open-0')
      .select('#open-scene-' + scene.id)
      .selectAll('.openStoryContainer_' + scene.id)
      .node()
      .getBBox()
    var bbox1 = d3
      .select('#hsl-scenes-open-1')
      .select('#open-scene-' + scene.id)
      .node()
      .getBBox()

    const extraY = 5
    const extraX = 0
    var firstSceneX = 1000000
    nestedScenes.forEach((e) => {
      if (e.x < firstSceneX) firstSceneX = e.x
    })
    const h = Math.max(bbox.height, bbox1.height) + getOpenSceneVerticalOffset() + extraY
    scene.openSize = {
      width: Math.max(bbox.width, bbox1.width) + firstSceneX + extraX,
      height: h,
      y: Math.min(bbox.y, bbox1.y) - extraY,
      difY: scene.height - h,
      lineSize: bbox_c.height,
    }
  }

  nestedScenes.update = function (animationDuration) {
    const promises = []

    var transition = d3
      .selectAll('#hsl-scenes-open-1')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + (d.x + 5 + sceneStyle('leftPadding')) + ',' + d.y + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#hsl-scenes-open-0')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + (d.x + 5 + sceneStyle('leftPadding')) + ',' + d.y + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#hsl-lines-open-inside-0')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + (d.x + 5 + sceneStyle('leftPadding')) + ',' + (d.y - 100) + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    return promises
  }

  function removeOpenScene(scene) {
    var elto = d3.select('#hsl-scenes-1').select('#sceneRect_' + scene.id)
    elto.classed('openRectScene', false)
    d3.select('#hsl-scenes-1')
      .select('#scene_' + scene.id)
      .append(function () {
        return elto.node()
      })
    d3.selectAll('.childScene_' + scene.id).sort(function (a, b) {
      if (a.id != elto.id) return -1
      else return 1
    })
    d3.selectAll('#open-scene-' + scene.id).remove()
  }

  nestedScenes.reset = function () {
    _activeEdges = []
    _verticalShifts = {}
  }
  return nestedScenes
}
