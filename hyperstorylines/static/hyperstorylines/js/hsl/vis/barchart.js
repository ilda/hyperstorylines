
import * as style from './style/stories.js'

export default function barchart() {
  var barchart = {}
  var _x
  var _y
  var _hsl
  barchart.init = function () {
    return barchart
  }

  barchart.hsl = function (hsl) {
    _hsl = hsl
    return barchart
  }

  barchart.reset = function () {
    d3.select('#summarySelect').selectAll('.dropdown-item').remove('*')
  }

  barchart.dimensions = function (dimensions, selections) {
    d3.select('#summarySelect').selectAll().remove('*')
    const yDim = selections.dim.y
    const div = d3.select('#summarySelect')
    for (let index in dimensions) {
      var d = dimensions[index]
      div
        .append('a')
        .attr('class', () => {
          if (d == yDim) {
            return 'dropdown-item active'
          } else {
            return 'dropdown-item'
          }
        })
        .attr('id', d)
        .html(d.charAt(0).toUpperCase() + d.slice(1))
        .on('click', function () {
          d3
            .select('#summarySelect')
            .selectAll('.dropdown-item')
            .classed('active', false)
          d3
            .select(this)
            .classed('active', true)
          const type = d3.select(this).node().id
          d3
            .select('#summarySelect_label')
            .html('Type of entity (' + type + ')')
          _hsl.drawSummary(type)
        })
    }
    d3
      .select('#summarySelect_label')
      .html('Type of entity (' + yDim + ')')
  }

  barchart.draw = function (entitiesDict, backgroundInteractions, entityInteractions) {
    const type = d3
      .select('#summarySelect')
      .selectAll('.dropdown-item.active')
      .node()
      .id
    d3.select('.hsl-barchart').selectAll('*').remove()
    // CHECK: MAKE SURE THINGS ARE FILTERED
    var data = []
    for (let entityId in entitiesDict) {
      const entity = entitiesDict[entityId]
      if (entity.isTerm) continue
      if (entity.type == type) {
        data.push(entity)
      }
    }
    if (type != 'time') {
      function compareValue(a, b) {
        if (a.original_edges_ids.length < b.original_edges_ids.length) {
          return -1
        }
        if (a.original_edges_ids.length > b.original_edges_ids.length) {
          return 1
        }
        return 0
      }
      data.sort(compareValue)
      data.reverse()
    } else {
      function compareValue(a, b) {
        if (a.name < b.name) {
          return -1
        }
        if (a.name > b.name) {
          return 1
        }
        return 0
      }
      data.sort(compareValue)
    }
    const margin = ({ top: 15, right: 0, bottom: 10, left: 60 })
    const width = 200
    const barHeight = 20
    const height = Math.ceil((data.length + 0.1) * barHeight) + margin.top + margin.bottom
    const color = '#bdbdbd'
    const selectedColor = style.selectedColor()

    _x = d3.scaleLinear()
      .domain([0, d3.max(data, d => d.original_edges_ids.length)])
      .range([margin.left, width - margin.right - margin.left])

    _y = d3.scaleBand()
      .domain(d3.range(data.length))
      .rangeRound([margin.top, height - margin.bottom])
      .padding(0.1)


    const xAxis = g => g
      .attr("transform", `translate(0,${margin.top})`)
      .call(d3.axisTop(_x).ticks(4))
      .call(g => g.select(".domain").remove())

    const yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(_y)
        .tickFormat(i => data[i].name).tickSizeOuter(0))
      .classed('yAxisLabel', true)

    const svg = d3.select('.hsl-barchart')
      .append('svg')
      .attr('height', height + 50)
      .attr('width', width + 10)
      .on('click', d => backgroundInteractions.click())
      .append('g')
      .attr('transform',
        'translate(' + margin.left + ',' + margin.top + ')')

    svg.append('g')
      .call(xAxis)
    svg.append('g')
      .call(yAxis)

    d3
      .select('.yAxisLabel')
      .selectAll('.tick')
      .on('mouseover', i => {
        const entity = data[i]
        const text = barchart.entityDescriptionText(entity)
        entityInteractions.mouseOver(text)
      })
      .on('mouseout', d => {
        entityInteractions.mouseOut()
      })
      .on('click', i => entityInteractions.click('barchart', data[i], 'barchart'))
    const groups = svg.append('g')
      .selectAll('.bar_g')
      .data(data)
      .enter()
      .append('g')
      .attr('id', d => 'bar_g_' + d.id)
      .attr('class', 'bar_g')
      .attr('transform', (d, i) => 'translate(0, ' + _y(i) + ')')
    groups.append('rect')
      .attr('class', 'bar_entity_all')
      .attr('x', _x(0))
      .attr('y', 0)
      .attr('height', _y.bandwidth())
      .attr('width', d => _x(d.original_edges_ids.length) - _x(0))
      .style('fill', color)
      .style('stroke', '#3182bd')
      .style('stroke-width', 0)
      .style('opacity', 0.3)
      .on('mouseover', d => {
        const text = barchart.entityDescriptionText(d)
        entityInteractions.mouseOver(text)
      })
      .on('mouseout', d => {
        entityInteractions.mouseOut()
      })
      .on('click', d => entityInteractions.click('barchart', d, 'barchart'))
    groups.append('rect')
      .attr('class', 'bar_entity')
      .attr('x', _x(0))
      .attr('y', 0)
      .attr('height', _y.bandwidth())
      .attr('width', d => _x(d.related_edges.length) - _x(0))
      .style('fill', color)
      .style('stroke', '#3182bd')
      .style('stroke-width', 0)
      .on('mouseover', d => {
        const text = barchart.entityDescriptionText(d)
        entityInteractions.mouseOver(text)
      })
      .on('mouseout', d => {
        entityInteractions.mouseOut()
      })
      .on('click', d => entityInteractions.click('barchart', d, 'barchart'))
    //})
  }

  barchart.updateSelected = function (data) {
    const isSelectionActive = data.isSelectionActive()
    d3
      .selectAll('.bar_entity')
      .style('fill', d => {
        const isSelected = data.isSelected(d.id)
        if (isSelected) {
          return style.selectedColor()
        } else {
          return '#bdbdbd'
        }
      })
      .style('opacity', d => {
        const isSelected = data.isSelected(d.id)
        const isRelatedSelected = data.isRelatedSelected(d.id)
        return style.getOpacity(isSelectionActive, isSelected, isRelatedSelected)
      })
    d3
      .selectAll('.bar_entity_all')
      .style('fill', d => {
        const isSelected = data.isSelected(d.id)
        if (isSelected) {
          return style.selectedColor()
        } else {
          return '#bdbdbd'
        }
      })
    return barchart
  }

  barchart.updateAfterFilterAndSearch = function (selectedEntities) {
    d3.selectAll('.bar_filtered_rect').remove()
    if (selectedEntities.length == 0) {
      return barchart
    }
    selectedEntities.forEach(entityId => {
      const g = d3.selectAll('#bar_g_' + entityId)
      if (!g.empty()) {
        g
          .append('rect')
          .attr('class', 'bar_filtered_rect')
          .attr('x', d => {
            const w = d.name.length * 6.5
            return _x(0) - w - 5
          })
          .attr('y', -2)
          .attr('height', _y.bandwidth() + 4)
          .attr('width', d => d.name.length * 6.5)
          .style('fill', 'none')
          .style('stroke-width', 2)
          .style('stroke', '#3182bd')
      }
    })
    return barchart
  }
  barchart.entityDescriptionText = function (entity) {
    const visibleEdges = entity.related_edges.length
    const originalEdges = entity.original_edges_ids.length
    var text = entity.name + ': ' + visibleEdges + '/'
    text +=  originalEdges + ' relationships'
    return text
  }

  return barchart.init()
}