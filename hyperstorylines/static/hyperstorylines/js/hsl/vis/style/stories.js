var _highlightColor = '#4292c6'
var _defaultOpa = 1
var _obscureOpa = 0.15
var _lineColor = '#bdbdbd'
var _dotColor = '#525252'
var _sceneRectangleColor = 'grey'
var _termColor = '#d94801'


export function selectedColor() {
  return _highlightColor
}
export function termColor() {
  return _termColor
}
export function defaultOpacity() {
  return _defaultOpa
}
export function obscureOpacity() {
  return _obscureOpa
}

// LINES
export function lineColor() {
  return _lineColor
}
export function getLineColor(isSelected, isTerm) {
  if (isSelected) {
    if (isTerm) {
      return termColor()
    } else {
      return selectedColor()
    }
  } else {
    return lineColor()
  }
}
export function getOpacity(isSelectionActive, isSelected, isRelatedSelected) {
  if (isSelectionActive) {
    if (isSelected || isRelatedSelected) {
      return defaultOpacity()
    } else {
      return obscureOpacity()
    }
  } else {
    return defaultOpacity()
  }
}
// SCENES RECTS
export function sceneRectangleColor() {
  return _sceneRectangleColor
}
export function getSceneRectColor(isSelected) {
  if (isSelected) {
    return selectedColor()
  } else {
    return sceneRectangleColor()
  }
}
export function getSceneRectStrokeWidth(isSelected) {
  if (isSelected) {
    return 2
  } else {
    return 1
  }
}
// SCENES CIRCLES
export function dotColor() {
  return _dotColor
}
export function getSceneCircleFillColor(isSelected, isTerm) {
  if (isSelected) {
    if(isTerm) 
      return termColor()
    else 
      return selectedColor()
  } else {
    return dotColor()
  }
}
export function getSceneCircleStrokeColor() {
  return _dotColor
}
export function getSceneCircleRadius(scene) {
  return 3
}

// TEXT 
export function getTextColor(isSelected, isTerm) {
  if (isSelected) {
    if (isTerm) {
      return termColor()
    } else {
      return selectedColor()
    }
  } else {
    return '#000'
  }
}
export function getTextFontWeight(isSelected) {
  if (isSelected) {
    return 'bold'
  } else {
    return 'normal'
  }
}

// FILTER RECTS
export function getFilterRectColor(isTerm) {
  if (isTerm) {
    return termColor()
  } else {
    return selectedColor()
  }
}
