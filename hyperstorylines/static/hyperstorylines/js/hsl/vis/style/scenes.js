export default function scenesStyle(feature) {
  var style = {
    labelSize: [195, 30],
    pathSpace: 25,
    groupMargin: 10,
    leftPadding: 20,
    rightPadding: 10,
    sceneWidth: 7
  }
  return style[feature]
}
