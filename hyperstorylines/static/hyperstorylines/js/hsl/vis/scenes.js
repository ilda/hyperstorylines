import * as style from './style/stories.js'
import sceneStyle from './style/scenes.js'

export default function scenes() {
  var scenes = {}

  scenes.createContainers = function (level) {
    d3.select('#hsl-main')
      .append('g')
      .attr('id', 'hsl-scenes-' + level)
  }
  scenes.createContainersOpenScenes = function (level) {
    d3.select('#hsl-main')
      .append('g')
      .attr('id', 'hsl-scenes-open-' + level)
  }

  scenes.draw = function (narrative, scenesInteractions) {
    drawLayer0(narrative)
    drawLayer1(narrative, scenesInteractions)
    return scenes
  }

  scenes.clean = function () {
    d3.select('#hsl-scenes-0').selectAll('*').remove()
    d3.select('#hsl-scenes-1').selectAll('*').remove()
    d3.select('#hsl-scenes-open-0').selectAll('*').remove()
    d3.select('#hsl-scenes-open-1').selectAll('*').remove()
    return scenes
  }
  scenes.update = function (narrative, animationDuration) {
    const promises = []
    var transition = d3
      .selectAll('#hsl-scenes-0')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#hsl-scenes-1')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#hsl-scenes-1')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', (d) => 'translate(' + (d.x + 5 + sceneStyle('leftPadding')) + ',' + d.y + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    return promises
  }
  scenes.updateSelected = function (data, selections) {
    // TODO: THE UPDATE OF THE OPEN SCENES SHOULD BE SOMEWHERE ELSE
    // TODO: KNOWING WHEN IF THERE ARE LEVELS TO IGNORE SHOULD NOT BE HERE
    const xDim = selections.dim.x
    const xAggr = selections.aggr.x
    const isXDimAggr = data.isXDimAggregated(xDim, xAggr)

    const isSelectionActive = data.isSelectionActive()
    // CLOSED SCENES
    d3.selectAll('.scene')
      .selectAll('.sceneRect')
      .style('stroke', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
    d3.selectAll('.scene')
      .selectAll('text')
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
    d3.selectAll('.scene')
      .selectAll('.appearance')
      .attr('r', style.getSceneCircleRadius())
      .style('stroke', style.getSceneCircleStrokeColor())
      .style('fill', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isTerm = (d.character.node_type == 'term')
        return style.getSceneCircleFillColor(isSelected, isTerm)
      })
      .style('opacity', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.character,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
    // OPEN RECTANGLE OF THE ORIGINAL SCENES
    d3.selectAll('.open-scene')
      .selectAll('.openRectScene')
      .style('stroke', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
    // INNER SCENES RECTANGLES
    d3.selectAll('.open-scene')
      .selectAll('.openStoryContainer')
      .selectAll('.scene')
      .selectAll('.sceneRect')
      .style('stroke', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
    // OPEN SCENES INNER MARKERS
    d3.selectAll('.openStoryContainer')
      .selectAll('.appearance')
      .attr('r', style.getSceneCircleRadius())
      .style('stroke', style.getSceneCircleStrokeColor())
      .style('fill', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getSceneCircleFillColor(isSelected)
      })
      .style('opacity', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.character,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
    // OPEN SCENES INNER TEXT
    d3.selectAll('.openStoryContainer')
      .selectAll('.scene-nested')
      .selectAll('text')
      .style('fill', (d) => {
        if (isXDimAggr) {
          const isSelected = data.isEntitySelected(d.keyEntityX, selections)
          return style.getTextColor(isSelected)
        } else {
          const isSelected = data.isEntitySelected(d.keyEntityZ, selections)
          return style.getTextColor(isSelected)
        }
      })
      .style('font-weight', (d) => {
        if (isXDimAggr) {
          const isSelected = data.isEntitySelected(d.keyEntityX, selections)
          return style.getTextFontWeight(isSelected)
        } else {
          const isSelected = data.isEntitySelected(d.keyEntityZ, selections)
          return style.getTextFontWeight(isSelected)
        }
      })
    // OPEN SCENES INNER AXIS
    d3.selectAll('g.open-scene')
      .selectAll('.xDimLegend')
      .selectAll('text')
      .style('fill', (d) => {
        const isSelected = data.isEntitySelected(d.keyEntityZ, selections)
        return style.getTextColor(isSelected)
      })
      .style('font-weight', (d) => {
        const isSelected = data.isEntitySelected(d.keyEntityZ, selections)
        return style.getTextFontWeight(isSelected)
      })
    return scenes
  }
  scenes.transitionFilter = function (visibleEntities) {
    // SCENES
    var selection = d3.selectAll('.scene').filter(function (d) {
      var flag = false
      d.characters.forEach(function (element) {
        if (element === undefined) {
          console.log(d)
        }
        if (visibleEntities.indexOf(element.id) >= 0) {
          flag = true
        }
      })
      return !flag
    })
    selection.selectAll('rect').style('opacity', 0)

    selection.selectAll('text').style('opacity', 0)
    // RELATIONSHIPS INNER MARKERS
    d3.selectAll('.scene')
      .selectAll('.appearance')
      .filter(function (d) {
        if (!d.character) return false
        return visibleEntities.indexOf(d.character.id) < 0
      })
      .style('opacity', 0)
    return scenes
  }

  function drawLayer0(narrative) {
    var sceneG = d3
      .select('#hsl-scenes-0')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .enter()
      .append('g')
      .attr('class', 'scene')
      .attr('id', (d) => 'scene_' + d.id)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    sceneG
      .append('text')
      .attr('class', 'nNested')
      .text((d) => {
        if (d.nNested > 1) {
          return d.nNested
        } else {
          return ''
        }
      })
      .attr('transform', 'translate(-5, -3)')
    sceneG
      .append('rect')
      .attr('x', -sceneStyle('sceneWidth') / 2 - 5)
      .attr('y', -1)
      .attr('width', sceneStyle('sceneWidth') + 10)
      .attr('height', (d) => d.height + 2)
      .attr('rx', 3)
      .attr('ry', 3)
      .style('fill', '#f7f7f7')
      .style('stroke', 'none')
      .style('opacity', 0.7)
  }

  function drawLayer1(narrative, scenesInteractions) {
    var sceneG = d3
      .select('#hsl-scenes-1')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .enter()
      .append('g')
      .attr('class', 'scene')
      .attr('id', (d) => 'scene_' + d.id)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    drawRelationshipsContainers(sceneG, scenesInteractions)
    drawRelationshipsInnerMarkers(sceneG)
    return scenes
  }

  function drawRelationshipsContainers(sceneG, scenesInteractions) {
    var sceneRect = sceneG
      .append('rect')
      .attr('class', (d) => 'sceneRect childScene_' + d.id)
      .attr('id', (d) => 'sceneRect_' + d.id)
      .attr('sceneId', (d) => d.id)
      .attr('width', sceneStyle('sceneWidth'))
      .attr('height', (d) => d.height)
      .attr('y', 0)
      .attr('x', -sceneStyle('sceneWidth') / 2)
      .attr('rx', 3)
      .attr('ry', 3)
      .style('fill', 'white')
      .style('fill-opacity', 1)
      .style('stroke-dasharray', (d) => {
        if (d.nNested > 0) {
          return null
        } else {
          return '3, 3'
        }
      })
      .style('cursor', 'pointer')
      .style('pointer-events', 'visible')
      .on('click', (d) => scenesInteractions.click(d))
    return sceneRect
  }

  function drawRelationshipsInnerMarkers(sceneG) {
    sceneG
      .selectAll('.appearance')
      .data((d) => d.appearances)
      .enter()
      .append('circle')
      .attr(
        'class',
        (d) =>
          'appearance' +
          ' childScene_' +
          d.scene.id +
          ' entityDom_' +
          d.character.id,
      )
      .attr('id', (d) => 'sceneCircle_' + d.character.id)
      .attr('parentScene', (d) => d.scene.id)
      .attr('typeDom', 'scene')
      .attr('cx', 0)
      .attr('cy', (d) => d.y)
  }
  return scenes
}
