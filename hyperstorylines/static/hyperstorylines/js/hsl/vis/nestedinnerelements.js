import sceneStyle from './style/scenes.js'
import {
  getTranslateCoordinates,
  getLinkGroupId,
  nameCutter,
} from '../hsl-utils.js'

export function addInnerElements(
  scene,
  openStory,
  entityInteractions,
  isXDimAggr
) {
  ////////////////////////////////////
  // LOWER LEVEL AT hsl-scenes-0
  var openStoryGroup = d3
    .select('#hsl-scenes-open-0')
    .select('#open-scene-' + scene.id)

  const openStoryGroupLines = d3
    .select('#hsl-lines-open-inside-0')
    .select('#links-open-scene-' + scene.id)
  //.attr('transform', 'translate(' + (sceneStyle('leftPadding')+5)+',0)')

  openStoryGroupLines
    .selectAll('.linkGInner')
    .data(() => {
      var filtered = []
      var links = openStory.links()
      links.forEach((e) => {
        if ('scene' in e.source) {
          e.parentScene = scene
          filtered.push(e)
        }
      })
      return filtered
    })
    .enter()
    .append('g')
    .attr('class', (d) => 'link-inner link-' + d.character.id)
    .append('path')
    .attr('class', 'link')
    .attr('d', openStory.link(scene.x, scene.y))
    .style('fill', 'none')
    .style('opacity', 0)
    .style('stroke-width', '3px')
    .style('cursor', 'pointer')
    .on('mouseover', (d) => entityInteractions.mouseOver(d.character.name))
    .on('mouseout', (d) => entityInteractions.mouseOut())
    .on('click', (d) =>
      entityInteractions.click('chart', d.character, 'yDimInnerLine'),
    )

  // DRAW WHITE SQUARES
  var sceneG = openStoryGroup
    .select('.openStoryContainer')
    .selectAll('.scene')
    .data(openStory.scenes())
    .enter()
    .append('g')
    .attr('class', 'scene')
    .attr('id', (d) => 'scene_' + d.id)
    .attr('transform', function (d) {
      var x, y
      x = Math.round(d.x)
      if (d.offsetY) {
        y = Math.round(d.y) + 0.5 + d.offsetY
      } else {
        y = Math.round(d.y) + 0.5
      }
      return 'translate(' + [x, y] + ')'
    })
  sceneG
    .append('rect')
    .attr('x', -5)
    .attr('y', -1)
    .attr('width', sceneStyle('sceneWidth') + 10)
    .attr('height', (d) => d.height + 2)
    .style('fill', '#f7f7f7')
    .style('stroke', 'none')
    .attr('rx', 3)
    .attr('ry', 3)
    .style('opacity', 0.7)

  ////////////////////////////////////
  // HIGHER LEVEL AT hsl-scenes-open-1
  openStoryGroup = d3
    .select('#hsl-scenes-open-1')
    .select('#open-scene-' + scene.id)

  // CREATE LINKS BETWEEN RECTANGLES
  var linkG = openStoryGroup
    .select('.openStoryContainer')
    .selectAll('.link')
    .data(() => {
      var filtered = []
      var links = openStory.links()
      links.forEach((e) => {
        if ('scene' in e.source) {
          filtered.push(e)
        }
      })
      return filtered
    })
    .enter()
    .append('g')
    .attr('class', (d) => 'link link-' + d.character.id)

  // MASK
  var mask = linkG
    .append('clipPath')
    .attr('id', (d, i) => 'clipPath_inside-intra-1-' + getLinkGroupId(d))

  // SOURCE
  mask
    .append('rect')
    .attr('id', 'source')
    .attr('x', (d) => {
      const x0 = d.source.scene ? d.source.scene.x + d.source.x : d.source.x
      return x0 + 2
    })
    .attr('y', (d) => {
      const y0 = d.source.scene ? d.source.scene.y + d.source.y : d.source.y
      return y0 - 20
    })
    .attr('width', 8)
    .attr('height', 40)
    .style('fill', 'pink')

  // TARGET
  mask
    .append('rect')
    .attr('id', 'target')
    .attr('x', (d) => {
      const x1 = d.target.scene ? d.target.scene.x + d.target.x : d.target.x
      return x1 - sceneStyle('sceneWidth') - 4
    })
    .attr('y', (d) => {
      const y1 = d.target.scene ? d.target.scene.y + d.target.y : d.target.y
      return y1 - 20
    })
    .attr('width', 8)
    .attr('height', 40)
    .style('fill', 'green')

  linkG
    .append('path')
    .attr('class', 'link')
    .attr('d', openStory.link())
    .attr('clip-path', (d, i) => {
      return 'url(#clipPath_inside-intra-1-' + getLinkGroupId(d) + ')'
    })
    .style('stroke', 'red')
    .style('opacity', 0)
    .style('fill', 'none')
    .style('stroke-width', '3px')

  linkG
    .append('path')
    .attr('class', 'link-ghost')
    .attr('d', openStory.link())
    .style('stroke', 'red')
    .style('opacity', 0)
    .style('fill', 'none')
    .style('stroke-width', '3px')
    .style('visibility', 'hidden')

  // CREATE SCENES
  sceneG = openStoryGroup
    .select('.openStoryContainer_' + scene.id)
    .selectAll('.scene')
    .data(openStory.scenes())
    .enter()
    .append('g')
    .attr('class', 'scene scene-nested')
    .attr('id', (d) => 'sceneNested_' + d.id)
    .attr('transform', (d) => {
      var x, y
      x = Math.round(d.x)
      if (d.offsetY) {
        y = Math.round(d.y) + 0.5 + d.offsetY
      } else {
        y = Math.round(d.y) + 0.5
      }
      return 'translate(' + [x, y] + ')'
    })
    .attr('fill', 'white')

  sceneG
    .append('rect')
    .attr('class', 'sceneRect sceneRectNested')
    .attr('y', 0)
    .attr('x', 0)
    .attr('rx', 3)
    .attr('ry', 3)
    .attr('width', (d) => sceneStyle('sceneWidth'))
    .attr(
      'height',
      (d) =>
        sceneStyle('pathSpace') * d.appearances.length +
        sceneStyle('pathSpace') / 2,
    )
    .style('stroke', 'black')

  sceneG
    .append('text')
    .attr('transform', 'translate(0, -10) rotate(-45)')
    .text((d) => {
      if (isXDimAggr) {
        return nameCutter(d.keyEntityX.name)
      } else {
        return nameCutter(d.keyEntityZ.name)
      }
    })
    .style('fill', (d) => {
      return 'black'
    })
    .on('mouseover', (d) => entityInteractions.mouseOver(d.keyEntity.name))
    .on('mouseout', (d) => entityInteractions.mouseOut())
    .on('click', (d) => {
      if (isXDimAggr) {
        entityInteractions.click('chart', d.keyEntityX, 'yDimInnerLine')
      } else {
        entityInteractions.click('chart', d.keyEntityZ, 'yDimInnerLine')
      }
    })

  sceneG
    .selectAll('circle')
    .data((d) => d.appearances)
    .enter()
    .append('circle')
    .attr('class', 'appearance appearanceNested')
    .attr('cx', sceneStyle('sceneWidth') / 2)
    .attr('cy', (d) => d.y)
    .attr('r', 3)
    .style('fill', 'black')
    .style('stroke', 'black')
}

export function updateInnerElements(openWidth, scene, newNestedScenes) {
  var newOpenStory = d3
    .narrative()
    .scenes(newNestedScenes)
    .size([openWidth, scene.height])
    .pathSpace(sceneStyle('pathSpace'))
    .labelSize([0, 0])
    .scenePadding([5, 0, 0, 0])
    .labelPosition('left')
    .layout()

  if (!scene.isOpen) {
    return
  }

  // Draw the scenes
  const openG = d3
    .selectAll('.openStoryContainer_' + scene.id)
    .selectAll('.scene')
    .data(newOpenStory.scenes())
    .attr('id', (d) => 'sceneNested_' + d.id)
    .attr('transform', (d) => 'translate(' + [d.x, d.y] + ')')
  openG.select('rect').attr('id', (d) => 'rectNested_' + d.id)

  // Draw intro nodes
  d3.selectAll('.openStoryContainer_' + scene.id)
    .selectAll('.introNested')
    .data(newNestedScenes)
    .select('text')
    .text((d) => nameCutter(d.key))
}

export function addInnerXAxis(
  scene,
  nestedScenes,
  openStory,
  entityInteractions,
) {
  var values = computeValues(nestedScenes)

  const openStoryGroupExtra = d3
    .select('#hsl-scenes-open-0')
    .select('g#open-scene-' + scene.id)
  const openStoryGroupLines = d3
    .select('#hsl-lines-open-inside-0')
    .selectAll('#open-scene-' + scene.id)
    .selectAll('#innerXAxis')
    .attr('transform', 'translate(' + -sceneStyle('leftPadding') + ',0)')
  openStoryGroupLines
    .selectAll('xDimLegend')
    .data(values)
    .enter()
    .append('g')
    .attr('class', 'xDimLegend')
    .attr('transform', function (d, i) {
      return 'translate(' + d.xpos + ',' + 0 + ')'
    })
    .append('line')
    .style('stroke-width', 5)
    .style('stroke', '#fff')
    .attr('x1', function (d) {
      return 0
    })
    .attr('x2', function (d) {
      return 0
    })
    .attr('y1', function (d) {
      return 10
    })
    .attr('y2', function (d) {
      return 20
    })
  //.on('click', d => backgroundInteractions.click())*/
  var legendGroups = openStoryGroupExtra
    .select('#innerXAxis')
    .selectAll('.xDimLegend')
    .data(values)
    .enter()
    .append('g')
    .attr('class', 'xDimLegend')
    .attr('transform', function (d, i) {
      return 'translate(' + d.xpos + ',' + 0 + ')'
    })
  legendGroups
    .append('text')
    .text((d) => nameCutter(d.label))
    .attr('transform', 'rotate(-45)')
    .on('mouseover', (d) => entityInteractions.mouseOver(d.label))
    .on('mouseout', (d) => entityInteractions.mouseOut())
    .on('click', (d) =>
      entityInteractions.click('chart', d.keyEntityZ, 'zDim', d.label),
    )
}
function computeValues(nestedScenes) {
  const scenes = nestedScenes
  var _values = []
  var listXDict = {}
  for (var index in scenes) {
    var aScene = scenes[index]
    if (aScene.keyZ in listXDict) {
      listXDict[aScene.keyZ]['ids'].push(aScene.id)
      if (aScene.x < 0) {
        continue
      }
      if (listXDict[aScene.keyZ]['xpos'] < 0 && aScene.x > 0) {
        listXDict[aScene.keyZ]['xpos'] = aScene.x
      } else if (listXDict[aScene.keyZ]['xpos'] > aScene.x) {
        listXDict[aScene.keyZ]['xpos'] = aScene.x
      }
    } else {
      listXDict[aScene.keyZ] = { xpos: aScene.x }
      listXDict[aScene.keyZ]['ids'] = [aScene.id]
      listXDict[aScene.keyZ]['keyEntity'] = aScene.keyEntity
      listXDict[aScene.keyZ]['keyEntityZ'] = aScene.keyEntityZ
    }
  }
  for (var keyZ in listXDict) {
    var elto = listXDict[keyZ]
    if (elto.keyEntity.isFake) {
      elto['label'] = keyZ.substring(0, 7)
    } else {
      elto['label'] = keyZ
    }
    _values.push(elto)
  }
  return _values
}
