import axes from './axes.js'
import stories from './stories.js'
import scrollbars from './scrollbars.js'
import * as style from './style/stories.js'

export default function chart() {
  var chart = {
    width: 1000,
    height: 1000,
    padding: 100,
    left: 195
  }
  var _axes
  var _stories
  var _scrollbars
  chart.init = function () {
    _axes = axes()
    _stories = stories()
    _scrollbars = scrollbars().chart(chart)
    return chart
  }
  chart.size = function (width, height) {
    chart.width = width 
    chart.height = height 
    _scrollbars
      .verticalDomain(chart.height + chart.padding)
      .horizontalDomain(chart.width + chart.padding)
    return chart
  }
  chart.createContainers = function () {
    d3.select('.hsl-scene')
      .append('svg')
      .attr('id', 'hsl-scene-svg')
      .attr('width', chart.width + chart.padding)
      .attr('height', chart.height + chart.padding)
      .append('g')
      .attr('id', 'hsl-main')
    d3
      .select('body')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0)
    _axes.createContainers()
    _stories.createContainers()
    _scrollbars.createContainers()
    return chart
  }
  chart.updateSizeNarrative = function (narrative) {
    const scenes = narrative.scenes()
    const nScenes = scenes.length
    const chartDimensions = chart.getDimensions(nScenes)
    chart.width = chartDimensions[0] 
    chart.height = narrative.getGreatestYValue() 
    chart.updateSize(chart.left)
    return chart
  }
  chart.updateSizeShift = function (narrative, width, height) {
    chart.width += width
    chart.height += height
    chart.updateSize(chart.left)
    return chart
  }
  chart.updateSize = function (left) {
    d3.select('#hsl-scene-svg')
      .attr('width', chart.width + chart.padding)
      .attr('height', chart.height + chart.padding)
    _axes.updateSize(chart.width, chart.height + chart.padding, left)
    _scrollbars.updateSize(chart.width + chart.padding, chart.height + chart.padding + 100, left)
    return chart
  }
  chart.getDimensions = function (nScenes) {
    const w = 500 + nScenes * 30
    const h = 500 + nScenes * 1000
    return [w, h]
  }
  chart.stories = function () {
    return _stories
  }
  chart.axes = function () {
    return _axes
  }
  chart.draw = function (narrative, entityInteractions, backgroundInteractions, scenesInteractions) {
    chart.clean()
    d3
      .select('#hsl-main')
      .attr('transform', 'translate(' + (-195) + ',25)')
    d3
      .select('#hsl-scene-svg')
      .style('background-color', '#f7f7f7')
      .on('click', d => backgroundInteractions.click())
    _axes.draw(narrative, chart.height + chart.padding, entityInteractions, backgroundInteractions)
    _stories.draw(narrative, entityInteractions, scenesInteractions)
    return chart
  }
  chart.update = function (narrative, animationDuration) {
    const promises_stories = _stories.update(narrative, animationDuration)
    const promises_axes = _axes.update(narrative, animationDuration)
    return promises_stories.concat(promises_axes)
  }
  chart.clean = function () {
    _stories.clean()
    _axes.clean()
    d3.select('#hsl-main').selectAll('.intro').remove()
    d3.select('#hsl-main').selectAll('.no-data').remove()
    return chart
  }
  chart.updateSelected = function (data, selections) {
    _stories.updateSelected(data, selections)
    _axes.updateSelected(data, selections)
    _scrollbars.updateSelected(data)
    return chart
  }
  chart.updateAfterFilterAndSearch = function (filteredEntities, termsForRelationships) {
    filteredEntities.forEach(entityId => {
      var g = d3.selectAll('#intro_' + entityId)
      if (!g.empty() && g.select('.filteredFrame').empty()) {
        const entityNode = g.selectAll('text').node()
        const bbox = entityNode.getBBox()
        const rect = g
          .append('rect')
          .attr('x', -bbox.width - 10)
          .attr('y', -bbox.height / 2 - 2)
          .attr('class', 'filteredFrame')
          .attr('width', bbox.width + 4)
          .attr('height', bbox.height + 4)
          .style('fill', 'none')
          .style('stroke-width', 2)
          .style('stroke', style.getFilterRectColor(false))
        if (g.classed('introNested')) {
          rect.attr('transform', 'rotate(135), translate(5, 0)')
        }
      }
      g = d3.selectAll('#xaxis_' + entityId)
      if (!g.empty() && g.select('.filteredFrame').empty()) {
        const entityNode = g.selectAll('text').node()
        const bbox = entityNode.getBBox()
        const rect = g
          .append('rect')
          .attr('class', 'filteredFrame')
          .attr('x', -bbox.width - 7)
          .attr('y', -bbox.height / 2 + 5)
          .attr('width', bbox.width + 4)
          .attr('height', bbox.height + 4)
          .style('fill', 'none')
          .style('stroke-width', 2)
          .style('stroke', style.getFilterRectColor(false))

        rect.attr('transform', 'rotate(135), translate(5, 0)')
      }
    })
    termsForRelationships.forEach(entityId => {
      var g = d3.selectAll('#intro_' + entityId)
      if (!g.empty() && g.select('.filteredFrame').empty()) {
        const entityNode = g.selectAll('text').node()
        const bbox = entityNode.getBBox()
        const rect = g
          .append('rect')
          .attr('x', -bbox.width - 10)
          .attr('y', -bbox.height / 2 - 2)
          .attr('class', 'filteredFrame')
          .attr('width', bbox.width + 4)
          .attr('height', bbox.height + 4)
          .style('fill', 'none')
          .style('stroke-width', 2)
          .style('stroke', style.getFilterRectColor(true))
        if (g.classed('introNested')) {
          rect.attr('transform', 'rotate(135), translate(5, 0)')
        }
      }
    })
    return chart
  }
  chart.transitionFilter = function (visibleEntities) {
    d3.select('#hsl-scene-svg')
      .attr('width', 2000)
      .attr('height', 2000)
    d3.select('#hsl-legend-y-svg')
      .attr('height', 2000)
    _stories.transitionFilter(visibleEntities)
    _axes.transitionFilter(visibleEntities)
    return chart
  }
  chart.updateShift = function (narrative, animationDuration, width, height) {
    chart.updateSizeShift(narrative, width, height)
    return chart.update(narrative, animationDuration)
  }
  chart.drawNoData = function (backgroundInteractions) {
    chart.clean()
    const width = $('.hsl-scene').width()
    const height = $('.hsl-scene').height()
    d3.select('#hsl-main')
      .attr('transform', 'translate(' + (-chart.left) + ',25)')
    d3.select('#hsl-scene-svg')
      .attr('width', width + chart.left)
      .attr('height', height)
    d3.select('#hsl-legend-y-svg')
      .attr('width', chart.left)
      .attr('height', height)
    d3.select('#scene_yLegend')
      .attr('width', chart.left)
    d3.select('#scene_yLegend_noreal')
      .attr('width', chart.left)
    _axes.drawNoData(width, height, chart.left, backgroundInteractions)
    d3
      .select('#hsl-main')
      .append('text')
      .text('No data')
      .attr('class', 'no-data')
      .attr('x', width / 2)
      .attr('y', height / 2)
      .style('font-size', '20px')

  }
  chart.resetScrollbars = function () {
    _scrollbars.reset()
  }

  // OPEN SCENES
  chart.expandScene = function (scene, animationDuration, nestedScenes, entityInteractions, narrative, isXDimAggr) {
    return _stories.expandScene(scene, animationDuration, nestedScenes, entityInteractions, narrative, isXDimAggr)
  }

  chart.collapseScene = function (scene, animationDuration, narrative, selections) {
    _stories.collapseScene(scene, animationDuration, narrative, selections)
  }

  chart.getShiftValuesExpandedScene = function (scene, direction) {
    return _stories.getShiftValuesExpandedScene(scene, direction)
  }

  chart.getAllExpandedScenes = function () {
    return _stories.getAllExpandedScenes()
  }

  chart.reorderExpandedScenes = function (data, selections){
    _stories.reorderExpandedScenes(data, selections)
  }

  chart.dimensions = function (dimensions) {
    _stories.dimensions(dimensions)
  }

  chart.reset = function(){
    _stories.reset()
    return chart
  }

  return chart.init()
}