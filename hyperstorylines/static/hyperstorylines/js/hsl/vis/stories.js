import lines from './lines.js'
import scenes from './scenes.js'
import nestedScenes from './nestedscenes.js'

export default function stories() {
  var stories = {}
  var _lines
  var _scenes
  var _nestedScenes
  stories.init = function () {
    _lines = lines()
    _scenes = scenes()
    _nestedScenes = nestedScenes()
    return stories
  }
  stories.draw = function (narrative, entityInteractions, scenesInteractions) {
    _lines.draw(narrative, entityInteractions)
    _scenes.draw(narrative, scenesInteractions)
    return stories
  }
  stories.updateSelected = function (data, selections) {
    _lines.updateSelected(data, selections)
    _scenes.updateSelected(data, selections)
  }
  stories.transitionFilter = function (visibleEntities) {
    _scenes.transitionFilter(visibleEntities)
    _lines.transitionFilter()
  }
  stories.update = function (narrative, animationDuration) {
    const promises_scenes = _scenes.update(narrative, animationDuration)
    const promises_nested_scenes = _nestedScenes.update(animationDuration)
    const promises_lines = _lines.update(narrative, animationDuration)
    return (promises_scenes.concat(promises_lines)).concat(promises_nested_scenes)
  }
  stories.createContainers = function () {
    _lines.createContainers(0)
    _scenes.createContainers(0)
    _lines.createContainers(1)
    _scenes.createContainers(1)

    _lines.createContainersOpenScenes(0)
    _scenes.createContainersOpenScenes(0)
    _lines.createContainersOpenScenes(1)
    _scenes.createContainersOpenScenes(1)
  }

  stories.clean = function () {
    _lines.clean()
    _scenes.clean()
  }

  // OPEN SCENCES
  stories.expandScene = function (scene, animationDuration, nestedScenes, entityInteractions, narrative, isXDimAggr) {
    return _nestedScenes.unfold(scene, animationDuration, nestedScenes, entityInteractions, narrative, isXDimAggr)
  }

  stories.collapseScene = function (scene, animationDuration, narrative, selections){
    _nestedScenes.fold(scene, animationDuration, narrative, selections)
  }

  stories.getShiftValuesExpandedScene = function (scene, direction) {
    return _nestedScenes.getShiftValues(scene, direction)
  }

  stories.getAllExpandedScenes = function () {
    return _nestedScenes.getActiveScenes()
  }

  stories.reorderExpandedScenes = function (data, selections) {
    _nestedScenes.reorderExpandedScenes(data, selections)
  }

  stories.dimensions = function (dimensions) {
    _nestedScenes.dimensions(dimensions)
  }

  stories.reset = function(){
    _nestedScenes.reset()
    return stories
  }
  return stories.init()
}