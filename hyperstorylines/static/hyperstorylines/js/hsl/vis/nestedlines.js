import sceneStyle from './style/scenes.js'
import { getTranslateCoordinates, removeSpecialCharacters } from '../hsl-utils.js'

export async function addExtraLinks(
  scene,
  nestedScenes,
  openStory,
  entityInteractions,
  narrative,
) {
  const firstAppearancesInside = {}
  const lastAppearancesInside = {}

  nestedScenes.forEach((e) => {
    e.appearances.forEach((a) => {
      var cId = a.character.id
      // Get the starting points inside the open scene
      if (cId in firstAppearancesInside) {
        // Why does this work? Should be firstAppearancesInside[cId].scene.x?
        if (a.scene.id != scene.id && a.scene.x < firstAppearancesInside[cId].x)
          firstAppearancesInside[cId] = a
      } else {
        firstAppearancesInside[cId] = a
      }

      // Get the ending points inside the open scene
      if (cId in lastAppearancesInside) {
        if (a.scene.id != scene.id && a.scene.x > lastAppearancesInside[cId].x)
          lastAppearancesInside[cId] = a
      } else {
        lastAppearancesInside[cId] = a
      }
    })
  })

  var nextExtraLinks = []
  var previousExtraLinks = []

  var extraLinks = []

  for (var index in firstAppearancesInside) {
    var anAppe = firstAppearancesInside[index]
    // PREVIOUS EXTRA LINKS
    var previousExtraLink = getExtraLink(
      scene,
      anAppe,
      false,
      openStory,
      entityInteractions,
      narrative,
    )
    if (previousExtraLink !== undefined) {
      previousExtraLinks.push(previousExtraLink)
      extraLinks.push(previousExtraLink)
    }
  }

  for (var index in lastAppearancesInside) {
    var anAppe = lastAppearancesInside[index]
    // PREVIOUS EXTRA LINKS
    var nextExtraLink = getExtraLink(
      scene,
      anAppe,
      true,
      openStory,
      entityInteractions,
      narrative,
    )
    if (nextExtraLink !== undefined) {
      nextExtraLinks.push(nextExtraLink)
      extraLinks.push(nextExtraLink)
    }
  }
  createLinkInsideOpenScene(scene, extraLinks, openStory, entityInteractions)
  createLinkOutsideOpenScene(scene, extraLinks, openStory, entityInteractions)
}

function createLinkInsideOpenScene(
  scene,
  extraLinks,
  openStory,
  entityInteractions,
) {
  // COMPUTE SOME VALUES FOR THE SIZE OF THE CLIPPATH OF LAYER 0
  const openStoryGroup = d3
    .select('#hsl-scenes-open-1')
    .select('#open-scene-' + scene.id)
  var bbox = openStoryGroup.selectAll('.openStoryContainer').node().getBBox()
  var maxHTextBBox = 0
  openStoryGroup
    .select('.openStoryContainer')
    .selectAll('.introNested')
    .each(function (d) {
      var thisB = d3.select(this).node().getBBox()
      maxHTextBBox = Math.max(maxHTextBBox, thisB.height)
    })

  // INSIDE RECTANGLE LAYER 0
  var bbox = d3
    .select('#hsl-scenes-open-1')
    .select('#open-scene-' + scene.id)
    .selectAll('.openStoryContainer_' + scene.id)
    .node()
    .getBBox()

  var firstSceneX = 1000000
  openStory.scenes().forEach((e) => {
    if (e.x < firstSceneX) firstSceneX = e.x
  })

  var sceneGroup = d3
    .select('#hsl-lines-open-inside-0')
    .select('#links-open-scene-' + scene.id)

  sceneGroup
    .selectAll('clipPath')
    .data([scene])
    .enter()
    .append('clipPath')
    .attr('id', (d) => 'clipPath_inside-0-' + scene.id)
    .append('rect')
    .attr('width', (d) => d.openSize.width)
    .attr('height', (d) => d.openSize.height)
    .attr('transform', 'translate(' + sceneStyle('leftPadding') + ',0)')
    .style('fill', 'red')
    .style('opacity', '0.5')

  const linkGroup = sceneGroup
    .selectAll('.newExtraLink')
    .data(extraLinks)
    .enter()
    .append('g')
    .attr('class', (d) => 'link-substitute link-' + d.character.id)

  linkGroup
    .append('path')
    .attr('clip-path', (d) => 'url(#clipPath_inside-0-' + scene.id + ')')
    .attr('class', (d) => 'link')
    .attr('d', openStory.link())
    .style('fill', 'none')
    .style('stroke-width', '3px')
    .style('opacity', 0)
    .style('cursor', 'pointer')
    .on('mouseover', (d) => {
      return entityInteractions.mouseOver(d.character.name)
    })
    .on('mouseout', (d) => entityInteractions.mouseOut())
    .on('click', (d) =>
      entityInteractions.click('chart', d.character, 'yDimInnerLine'),
    )

  linkGroup
    .append('path')
    .attr('class', (d) => 'link-ghost')
    .attr('d', openStory.link())
    .style('fill', 'none')
    .style('stroke-width', '3px')
    .style('opacity', 0)
    .style('visibility', 'hidden')

  // INSIDE RECTANGLE LAYER 1
  sceneGroup = d3
    .select('#hsl-lines-open-inside-1')
    .select('#links-open-scene-' + scene.id)

  var linkG = sceneGroup
    .selectAll('.newExtraLink')
    .data(extraLinks)
    .enter()
    .append('g')
    .attr('class', (d) => 'link-substitute link-' + d.character.id)

  // MASK
  var mask = linkG
    .append('clipPath')
    .attr('id', (d) => 'clipPath_inside-1-' + d.originalPathId)

  // SOURCE
  mask
    .append('rect')
    .attr('id', 'source')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', 6)
    .attr('height', 40)

  // TARGET
  mask
    .append('rect')
    .attr('id', 'target')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', 6)
    .attr('height', 40)

  linkG
    .append('path')
    .attr('class', 'link')
    .attr('d', openStory.link())
    .attr('clip-path', (d, i) => {
      return 'url(#clipPath_inside-1-' + d.originalPathId + ')'
    })
    .style('stroke', 'red')
    .style('opacity', 0)
    .style('fill', 'none')
    .style('stroke-width', '3px')
}

function createLinkOutsideOpenScene(
  scene,
  extraLinks,
  openStory,
  entityInteractions,
) {
  // OUTSIDE THE RECTANGLE!
  var linkGOutside = d3
    .select('#hsl-main')
    .select('#hsl-lines-open-outside-0')
    .append('g')
    .attr('id', 'links-open-scene-' + scene.id)
    .selectAll('.newExtraLink')
    .data(extraLinks)
    .enter()
    .append('g')
    .attr('class', (d) => 'link-substitute link-' + d.character.id)

  linkGOutside
    .append('path')
    .attr('class', (d) => 'link')
    .attr('d', openStory.link())
    .style('fill', 'none')
    .style('stroke-width', '3px')
    .style('opacity', 0)
    .style('cursor', 'pointer')
    .on('mouseover', (d) => {
      return entityInteractions.mouseOver(d.character.name)
    })
    .on('mouseout', (d) => entityInteractions.mouseOut())
    .on('click', (d) =>
      entityInteractions.click('chart', d.character, 'yDimInnerLine'),
    )

  linkGOutside
    .append('path')
    .attr('class', (d) => 'link-ghost')
    .attr('visibility', 'hidden')
    .attr('d', openStory.link())
    .style('fill', 'none')
    .style('stroke-width', '3px')

  var linkGLayer1 = d3
    .select('#hsl-lines-open-outside-1')
    .append('g')
    .attr('id', 'links-open-scene-' + scene.id)
    .selectAll('.newExtraLink')
    .data(extraLinks)
    .enter()
    .append('g')
    .attr('class', (d) => 'link-substitute')

  // MASK
  var mask = linkGLayer1
    .append('clipPath')
    .attr('id', (d, i) => 'clipPath_outside-1-' + d.originalPathId)

  // SOURCE
  mask
    .append('rect')
    .attr('id', 'source')
    .attr('x', 200)
    .attr('y', 0)
    .attr('width', 6)
    .attr('height', 40)

  // TARGET
  mask
    .append('rect')
    .attr('id', 'target')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', 6)
    .attr('height', 40)

  // PATH
  linkGLayer1
    .append('path')
    .attr('class', 'link')
    .attr('d', openStory.link())
    .attr('clip-path', (d, i) => {
      return 'url(#clipPath_outside-1-' + d.originalPathId + ')'
    })
    .style('opacity', 0)
    .style('fill', 'none')
    .style('stroke-width', '3px')
}

function getExtraLink(
  scene,
  anAppe,
  next,
  openStory,
  entityInteractions,
  narrative,
) {
  //return
  var source = d3
    .select('#hsl-main')
    .selectAll('#scene_' + scene.id)
    .selectAll('#sceneCircle_' + anAppe.character.id)
    .node()
  var parentSource = source.parentNode
  var anAppeNodeM = getTranslateCoordinates(parentSource)
  var target
  var nextX

  // CASE IT IS PREVIOUS OR NEXT EXTRA LINK
  if (next) {
    nextX = Number.MAX_SAFE_INTEGER
  } else {
    nextX = -Number.MAX_SAFE_INTEGER
  }

  // SEARCH FOR THE TARGET DOM ELEMENT TO BE LINKED TO
  d3.select('#hsl-main')
    .selectAll('.entityDom_' + anAppe.character.id)
    .each(function (e) {
      var d = d3.select(this).node()
      var m = getTranslateCoordinates(d.parentNode)
      var mX = m.x
      if (next) {
        if (mX > anAppeNodeM.x && mX < nextX) {
          nextX = mX
          target = d
        }
      } else {
        if (mX < anAppeNodeM.x && mX > nextX) {
          nextX = mX
          target = d
        }
      }
    })
  // BORDER CASES: NO TARGET -> NO LINK
  if (next) {
    if (nextX == Number.MAX_SAFE_INTEGER) return undefined
  } else {
    // THERE IS ALWAYS A PREVIOUS, SHOULD NEVER REACH THIS
    if (nextX == -Number.MAX_SAFE_INTEGER) {
      return undefined
    }
  }
  // CREATE DATA FOR THE LINK
  var targetType = target.getAttribute('typeDom')
  var originalPathId
  var targetId
  var targetData
  if (targetType == 'scene') {
    targetId = target.getAttribute('parentScene')
    if (next) {
      originalPathId = 'S-scene-' + scene.id + '-char-' + anAppe.character.id
      originalPathId += '-T-scene-' + targetId + '-char-' + anAppe.character.id
    } else {
      originalPathId = 'S-scene-' + targetId + '-char-' + anAppe.character.id
      originalPathId += '-T-scene-' + scene.id + '-char-' + anAppe.character.id
    }
    targetData = narrative.getScene(target.__data__.scene.id)
  } else {
    targetId = anAppe.character.id
    originalPathId = 'S-char-' + anAppe.character.id
    originalPathId += '-T-scene-' + scene.id + '-char-' + anAppe.character.id
    const thisId = target.__data__.character.id
    targetData = narrative.getIntroduction(thisId)
  }

  var newLink = {}
  newLink.character = anAppe.character
  newLink.sceneId = scene.id
  newLink.originalPathId = originalPathId
  newLink.next = next
  newLink.extraLinkSuffix = extraLinkSuffix(next)
  newLink.targetType = targetType
  newLink.targetId = targetId
  newLink.visibility = 'visible'
  newLink.source = {}
  newLink.target = {}

  if (next) {
    newLink.source.id = anAppe.scene.id
    newLink.source.character = anAppe.character
    newLink.source.x = scene.x + anAppe.scene.x
    newLink.source.y = scene.y + anAppe.scene.y + anAppe.y
    newLink.source.extraY = +anAppe.scene.y + anAppe.y
    newLink.source.extraX = +anAppe.scene.x

    newLink.target.id = anAppe.scene.id
    newLink.target.character = anAppe.character
    newLink.target.x = targetData.x
    newLink.target.y = targetData.y
    newLink.target.extraY = 0
    newLink.target.extraX = 0
    if (targetType == 'scene') {
      newLink.target.y += target.__data__.y
      newLink.target.extraY = target.__data__.y
      newLink.target.x += sceneStyle('leftPadding')
    }
  } else {
    newLink.target.id = anAppe.scene.id
    newLink.target.character = anAppe.character
    newLink.target.x = scene.x + anAppe.scene.x
    newLink.target.y = scene.y + anAppe.scene.y + anAppe.y
    newLink.target.extraY = anAppe.scene.y + anAppe.y
    newLink.target.extraX = anAppe.scene.x

    newLink.source.id = target.id
    newLink.source.character = anAppe.character
    newLink.source.x = targetData.x
    newLink.source.y = targetData.y
    newLink.source.extraY = 0
    newLink.source.extraX = 0

    if (targetType == 'scene') {
      newLink.source.y += target.__data__.y
      newLink.source.extraY = target.__data__.y
      newLink.target.x += sceneStyle('leftPadding')
    }
  }
  // HIDE ORIGINAL LINK
  d3.selectAll('g#link-' + originalPathId)
    .style('visibility', 'hidden')
    .style('active', 'deactivated')
  return newLink
}
export function getOpenLinksSize() {
  return 2 * 25
}

function extraLinkSuffix(next) {
  if (next) {
    return 'n'
  } else {
    return 'p'
  }
}
export function removeExtraLinks(scene, animationDuration, narrative) {
  d3.select('#hsl-lines-open-inside-0')
    .selectAll('g#links-open-scene-' + scene.id)
    .selectAll('.link-substitute')
    .each((d) => {
      // I'm searching if there is a previous
      if (d.next) {
        var target = narrative.getScene(d.targetId)
        if (!target.isOpen) {
          d3
            //.select('#hsl-lines-0')
            .selectAll('g#link-' + d.originalPathId)
            .style('visibility', 'visible')
            .style('active', 'activated')
        }
      } else {
        if (d.visibility != 'hidden')
          d3.selectAll('g#link-' + d.originalPathId)
            .style('visibility', 'visible')
            .style('active', 'activated')
      }
    })
    .remove()

  d3.select('#hsl-lines-open-outside-0')
    .selectAll('g#links-open-scene-' + scene.id)
    .remove()
  d3.select('#hsl-lines-open-outside-1')
    .selectAll('g#links-open-scene-' + scene.id)
    .remove()
  d3.select('#hsl-lines-open-inside-0')
    .selectAll('g#links-open-scene-' + scene.id)
    .remove()
  d3.select('#hsl-lines-open-inside-1')
    .selectAll('g#links-open-scene-' + scene.id)
    .remove()
}

export function updateExtraLinks(narrative, animationDuration) {
  var extraLinks = []
  var promises = []
  var transition
  // GET THE EXTRA LINK DATA
  d3.select('#hsl-lines-open-inside-0')
    .selectAll('g.link-substitute')
    .each((d) => {
      var target
      var extraExtraX = 0
      var extraSourceX = 0
      var scene = narrative.getScene(d.sceneId)
      if (d.targetType == 'scene') {
        target = narrative.getScene(d.targetId)
      } else {
        target = narrative.getIntroduction(d.targetId)
      }
      var newLink = d
      if (d.next) {
        newLink.source.x = scene.x + d.source.extraX + sceneStyle('leftPadding') + sceneStyle('sceneWidth') + 5
        newLink.source.y = scene.y + d.source.extraY
        // TODO: A TARGET WILL NEVER BE DIFFERENT THAN A SCENE. Is this 
        if (d.targetType == 'scene' && target.isOpen) {
          var firstAppearancesInside = undefined
          if (!target.nestedScenes) return
          target.nestedScenes.forEach((e) => {
            e.appearances.forEach((a) => {
              var cId = a.character.id
              if (cId == d.character.id) {
                if (firstAppearancesInside == undefined)
                  firstAppearancesInside = a
                if (a.scene.x < firstAppearancesInside.scene.x) {
                  firstAppearancesInside = a
                }
              }
            })
          })
          newLink.target.x = target.x + firstAppearancesInside.scene.x + sceneStyle('leftPadding') + 5
          newLink.target.y =
            target.y + firstAppearancesInside.scene.y + firstAppearancesInside.y
        } else {
          newLink.target.x = target.x + d.target.extraX + sceneStyle('leftPadding') - 2
          newLink.target.y = target.y + d.target.extraY
        }
        newLink.visibility = 'visible'
      } else {
        newLink.target.x = scene.x + d.target.extraX + sceneStyle('leftPadding') + 5
        newLink.target.y = scene.y + d.target.extraY

        if (d.targetType == 'scene' && target.isOpen) {
          var lastAppearancesInside = undefined
          target.nestedScenes.forEach((e) => {
            e.appearances.forEach((a) => {
              var cId = a.character.id
              if (cId == d.character.id) {
                if (lastAppearancesInside == undefined)
                  lastAppearancesInside = a
                if (a.scene.x > lastAppearancesInside.scene.x) {
                  lastAppearancesInside = a
                }
              }
            })
          })
          newLink.visibility = 'hidden'
          newLink.source.x = target.x + lastAppearancesInside.scene.x + sceneStyle('leftPadding') + sceneStyle('sceneWidth')
          newLink.source.y =
            target.y + lastAppearancesInside.scene.y + lastAppearancesInside.y
        } else {
          newLink.source.x = target.x + d.source.extraX
          if (d.targetType == 'scene') {
            newLink.source.x += sceneStyle('leftPadding') + sceneStyle('sceneWidth') - 4
          }
          newLink.source.y = target.y + d.source.extraY
          newLink.visibility = 'visible'
        }
      }
      extraLinks.push(newLink)
    })

  // UPDATE THE LINKS INSIDE THE RECTANGLE
  d3.select('#hsl-lines-open-inside-0')
    .selectAll('clipPath')
    .select('rect')
    .attr('x', (d) => d.x + 2)
    .attr('y', (d) => d.openSize.y + d.y)

  var linksGroups = d3
    .select('#hsl-lines-open-inside-0')
    .selectAll('g.link-substitute')
    .data(extraLinks)

  transition = linksGroups
    .selectAll('.link')
    .transition()
    .duration(animationDuration)
    .attr('d', narrative.link())
  if (!transition.empty()) {
    promises.push(transition.end())
  }

  linksGroups.selectAll('.link-ghost').attr('d', narrative.link()).style('opacity', 1)
    .style('stroke', 'aqua')
  linksGroups = d3
    .selectAll('#hsl-lines-open-inside-1')
    .selectAll('g.link-substitute')
    .data(extraLinks)

  var mask = linksGroups.selectAll('clipPath')
  transition = mask
    .select('#source')
    .transition()
    .duration(animationDuration)
    .attr('x', (d) => {
      return d.source.x
    })
    .attr('y', (d) => {
      return d.source.y - 20
    })
  if (!transition.empty()) {
    promises.push(transition.end())
  }
  transition = mask
    .select('#target')
    .transition()
    .duration(animationDuration)
    .attr('x', (d) => {
      return d.target.x - sceneStyle('sceneWidth')
    })
    .attr('y', (d) => {
      return d.target.y - 20
    })
  if (!transition.empty()) {
    promises.push(transition.end())
  }

  transition = linksGroups
    .selectAll('.link')
    .transition()
    .duration(animationDuration)
    .attr('d', narrative.link())
    .style('visibility', (d) => d.visibility)
  if (!transition.end()) {
    promises.push(transition.end())
  }

  // UPDATE THE LINKS OUTSIDE THE RECTANGLE
  var linkGOutside = d3
    .select('#hsl-lines-open-outside-0')
    .selectAll('g.link-substitute')
    .data(extraLinks)

  transition = linkGOutside
    .selectAll('.link')
    .transition()
    .duration(animationDuration)
    .attr('d', narrative.link())
    .style('visibility', (d) => d.visibility)
  if (!transition.empty()) {
    promises.push(transition.end())
  }

  linkGOutside.selectAll('.link-ghost').attr('d', narrative.link())

  linksGroups = d3
    .selectAll('#hsl-lines-open-outside-1')
    .selectAll('g.link-substitute')
    .data(extraLinks)

  mask = linksGroups.selectAll('clipPath')

  transition = mask
    .select('#source')
    .transition()
    .duration(animationDuration)
    .attr('x', (d) => d.source.x)
    .attr('y', (d) => {
      const y0 = d.source.scene ? d.source.scene.y + d.source.y : d.source.y
      return d.source.y - 20
    })
  if (!transition.empty()) {
    promises.push(transition.end())
  }

  transition = mask
    .select('#target')
    .transition()
    .duration(animationDuration)
    .attr('x', (d) => {
      const x1 = d.target.scene ? d.target.scene.x + d.target.x : d.target.x
      return d.target.x - sceneStyle('sceneWidth')
    })
    .attr('y', (d) => {
      const y1 = d.target.scene ? d.target.scene.y + d.target.y : d.target.y
      return d.target.y - 20
    })
  if (!transition.empty()) {
    promises.push(transition.end())
  }
  transition = linksGroups
    .selectAll('.link')
    .transition()
    .duration(animationDuration)
    .attr('d', narrative.link())
    .style('visibility', (d) => d.visibility)
  if (!transition.empty()) {
    promises.push(transition.end())
  }

  // UPDATE THE INNER LINKS
  transition = d3
    .select('#hsl-lines-open-inside-0')
    .selectAll('g.link-inner')
    .selectAll('.link')
    .transition()
    .duration(animationDuration)
    .attr('d', (d) => narrative.link(d.parentScene.x + sceneStyle('leftPadding'), d.parentScene.y)(d))
  if (!transition.empty()) {
    promises.push(transition.end())
  }
  return promises
}
