export default function filterManager() {
  var filter = {}
  var _filterActive
  var _filteredEntities
  var _searchedTermsForRelationships
  var _searchedTermsForEntities

  // GENERAL METHODS
  filter.init = function () {
    _filterActive = false
    _filteredEntities = []
    _searchedTermsForRelationships = []
    _searchedTermsForEntities = []
    return filter
  }

  filter.isFilterActive = function () {
    return _filterActive
  }

  filter.resetFilterAndSearch = function () {
    _filteredEntities = []
    _searchedTermsForRelationships = []
    _searchedTermsForEntities = []
    _filterActive = false
  }
  filter.updateFilterActive = function () {
    if (_filteredEntities.length == 0 && _searchedTermsForRelationships.length == 0 && _searchedTermsForEntities.length == 0) {
      _filterActive = false
    } else {
      _filterActive = true
    }
  }

  // FILTER BY CLICKING
  filter.filterByEntity = function (data, entity) {
    if (entity.isFake) {
      return data.searchByEntity(entity.name.substring(0, 7))
    }
    const index = _filteredEntities.indexOf(entity.id)
    if (index >= 0) {
      _filteredEntities.splice(index, 1)
      entity.isFiltered = false
    } else {
      _filteredEntities.push(entity.id)
      entity.isFiltered = true
    }
    filter.updateFilterActive()
  }

  filter.filteredEntities = function () {
    return _filteredEntities
  }

  filter.removeFilteredEntity = function (entityId) {
    const index = _filteredEntities.indexOf(entityId)
    if (index >= 0) {
      _filteredEntities.splice(index, 1)
    }
    filter.updateFilterActive()
  }

  // SEARCH ENTITTIES BY TEXT
  filter.searchEntitiesByTerm = function (text) {
    _filterActive = true
    text = text.toLowerCase()
    const index = _searchedTermsForEntities.indexOf(text)
    if (index >= 0) return
    _searchedTermsForEntities.push(text)
  }

  filter.removeTermForEntities = function (text) {
    if (text == '') {
      return
    }
    const index = _searchedTermsForEntities.indexOf(text)
    if (index >= 0) {
      _searchedTermsForEntities.splice(index, 1)
    }
    filter.updateFilterActive()
  }

  filter.searchedTermsForEntities = function () {
    return _searchedTermsForEntities
  }

  // SEARCH RELATIONSHIPS BY TERM
  filter.searchRelationshipsByTerm = function (text) {
    _filterActive = true
    text = text.toLowerCase()
    const index = _searchedTermsForRelationships.indexOf(text)
    if (index >= 0) return
    _searchedTermsForRelationships.push(text)
  }

  filter.removeTermForRelationships = function (text) {
    if (text == '') {
      return
    }
    const index = _searchedTermsForRelationships.indexOf(text)
    if (index >= 0) {
      _searchedTermsForRelationships.splice(index, 1)
    }
    filter.updateFilterActive()
  }

  filter.searchedTermsForRelationships = function () {
    return _searchedTermsForRelationships
  }

  return filter
}