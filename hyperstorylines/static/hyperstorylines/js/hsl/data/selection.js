export default function selectionManager() {
  var selection = {}
  var _selectedEntities
  var _relatedSelectedEntities

  selection.init = function () {
    _selectedEntities = {}
    _relatedSelectedEntities = []
    return selection
  }

  //////////////////////////////////////////////////////////////////////////////
  // SELECT BY ENTITY
  selection.toggleSelection = function (entity) {
    if (!_selectedEntities[entity.id]) {
      selectEntity(entity)
      if (entity.isFake) {
        selection.toggleSeveralEntities(entity.name.substring(0, 7), true)
      }
    } else {
      unselectEntity(entity)
      if (entity.isFake) {
        selection.toggleSeveralEntities(entity.name.substring(0, 7), false)
      }
    }
  }

  selection.toggleSeveralEntities = function (text, select) {
    for (let nodeId in _nodes) {
      const aNode = _nodes[nodeId]
      const aNodeName = aNode.name.toLowerCase()
      if (aNodeName.includes(text) && !aNode.isFake) {
        if (select) {
          selectEntity(aNode)
        } else {
          unselectEntity(aNode)
        }
      }
    }
    return data
  }

  function selectEntity(entity) {
    entity.isSelected = true
    if (_selectedEntities[entity.id]) {
      return
    }
    // CREATE NEW ITEM IF IT DOESN'T EXIST
    _selectedEntities[entity.id] = {}
    _selectedEntities[entity.id].entity = entity
    _selectedEntities[entity.id].entityRelated = []
    for (var entityType in entity.related_nodes) {
      entity.related_nodes[entityType].forEach(function (id) {
        _relatedSelectedEntities.push(id)
        _selectedEntities[entity.id].entityRelated.push(id)
      })
    }
    if (entity.aggregated_nodes) {
      entity.aggregated_nodes.forEach(n => {
        selectEntity(n)
      })
    }
  }

  function unselectEntity(entity) {
    entity.isSelected = false
    if (!(entity.id in _selectedEntities)) {
      return
    }
    _selectedEntities[entity.id].entityRelated.forEach(function (d) {
      const index = _relatedSelectedEntities.indexOf(d)
      if (index > -1) {
        _relatedSelectedEntities.splice(index, 1)
      }
    })
    delete _selectedEntities[entity.id]
    if (entity.aggregated_nodes) {
      entity.aggregated_nodes.forEach(n => {
        unselectEntity(n)
      })
    }
  }

  selection.resetSelected = function () {
    for (let id in _selectedEntities) {
      _selectedEntities[id].entity.isSelected = false
    }
    _selectedEntities = {}
    _relatedSelectedEntities = []
  }

  //////////////////////////////////////////////////////////////////////////////
  // SELECTION QUERIES
  selection.isSelectionActive = function () {
    return Object.keys(_selectedEntities).length != 0
  }

  // BASE
  selection.isSelected = function (entityId) {
    if (entityId in _selectedEntities) {
      return true
    }
    return false
  }

  selection.isRelatedSelected = function (entityId) {
    if (_relatedSelectedEntities.indexOf(entityId) >= 0) {
      return true
    }
    return false
  }

  // BY ENTITY
  selection.isEntitySelected = function (node) {
    // TODO: THERE SHOULD'T BE AN UNDEFINED NODE
    if (node === undefined) return false
    const rawSelected = selection.isSelected(node.id)
    return rawSelected
  }

  selection.isEntityRelatedSelected = function (node, selections) {
    const xDim = selections.dim.x
    const yDim = selections.dim.y
    const zDim = selections.dim.z
    var flag = false
    if (selection.isSelected(node.id)) return true
    for (let dim_name in selections.dim) {
      const dim = selections.dim[dim_name]
      node.related_nodes[dim].forEach(n_id => {
        flag = flag || selection.isSelected(n_id)
      })
    }
    return flag
  }

  // BY SCENE
  selection.isSceneSelected = function (scene, selections) {
    var flag = false
    const yDim = selections.dim.y
    scene.edge.nodes_ids.forEach(n => {
      flag = flag || selection.isSelected(n)
    })
    for (let n in _selectedEntities) {
      if (n.includes('No ')) {
        const t = n.substring(3)
        if (scene.edge.node_types_ids[t].length == 0) {
          flag = true
        }
        if (scene.edge.original_edges) {
          scene.edge.original_edges.forEach(e => {
            if (e.node_types_ids[t].length == 0) {
              flag = true
            }
          })
        }
      }
    }
    return flag
  }

  selection.isSceneRelatedSelected = function (scene) {
    var flag = false
    scene.characters.forEach(function (element) {
      if (element == undefined)
        return
      if (selection.isRelatedSelected(element.id)) {
        flag = true
      }
    })
    return flag
  }

  return selection
}