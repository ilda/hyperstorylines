export function sortScenes(scenes, orderType) {
  return scenes.sort(function (a, b) {
    if (orderType == 'time' && a.keyNode.type != 'time') {
      if ((a.keyFirstAppearance).localeCompare(b.keyFirstAppearance) == 0) {
        if ((a.key).localeCompare(b.key) == 0) {
          return (a.date).localeCompare(b.date)
        } else {
          return (a.key).localeCompare(b.key)
        }

      } else {
        return (a.keyFirstAppearance).localeCompare(b.keyFirstAppearance)
      }
    } else {
      if ((a.key).localeCompare(b.key) == 0) {
        return (a.date).localeCompare(b.date)
      } else {
        return (a.key).localeCompare(b.key)
      }
    }
  })
}

export function addExtraDates() {
  return 0
  var minDate = '2200-01'
  var maxDate = '1000-01'
  for (let id in _nodes) {
    const aNode = _nodes[id]
    if (aNode == undefined || aNode.type != 'time') {
      continue
    }
    const monthDate = aNode.name.substring(0, 7)
    if (monthDate.localeCompare(minDate) < 0) {
      minDate = monthDate
    }
    if (monthDate.localeCompare(maxDate) > 0) {
      maxDate = monthDate
    }
  }
  minDate = new Date(minDate)
  maxDate = new Date(maxDate)
  const allDates = []
  var currentDate = minDate
  while (currentDate <= maxDate) {
    var cdString = currentDate.getFullYear() + '-'
    var m = (currentDate.getMonth() + 1).toString()
    if (m.length == 1) {
      m = '0' + m
    }
    cdString += m + '-00'
    allDates.push(cdString)
    currentDate = new Date(currentDate.setMonth(currentDate.getMonth() + 1));
  }
  allDates.sort()
  // Create entities fakeNodes
  const allDims = []
  for (let dim in _dimensions) {
    allDims.push(dim)
  }
  const typesEntities = []
  allDims.forEach(dim => {
    if (dim != 'time') {
      typesEntities.push(dim)
    }
  })
  typesEntities.forEach(te => {
    var fakeNode = {}
    fakeNode.first_appearance = allDates[0]
    fakeNode.id = te + '_extra'
    fakeNode.name = te + '_extra'
    fakeNode.related_edges = allDates
    fakeNode.related_nodes = {}
    allDims.forEach(dim => {
      if (dim != 'time') {
        fakeNode.related_nodes[dim] = [dim + '_extra']
      }
    })
    fakeNode.related_nodes.time = allDates
    fakeNode.type = te
    fakeNode.isFake = true
    _nodes[te + '_extra'] = fakeNode
  })

  allDates.forEach(d => {
    var fakeNode = {}
    fakeNode.first_appearance = d + '-01'
    fakeNode.id = d
    fakeNode.name = d
    fakeNode.related_edges = [d]
    fakeNode.related_nodes = {}
    allDims.forEach(dim => {
      if (dim != 'time') {
        fakeNode.related_nodes[dim] = [dim + '_extra']
      }
    })
    fakeNode.related_nodes.time = []
    fakeNode.type = 'time'
    fakeNode.isFake = true
    _nodes[d] = fakeNode

    var fakeEdge = {}
    fakeEdge.id = d
    fakeEdge.node_types_ids = {}
    allDims.forEach(dim => {
      if (dim != 'time') {
        fakeEdge.node_types_ids[dim] = [dim + '_extra']
      }
    })
    fakeEdge.node_types_ids.time = [d]
    fakeEdge.nodes_ids = []
    allDims.forEach(dim => {
      if (dim != 'time') {
        fakeEdge.nodes_ids.push(dim + '_extra')
      }
    })
    fakeEdge.nodes_ids.push(d)
    fakeEdge.sources = ['-1']
    fakeEdge.isFake = true
    _edges[d] = fakeEdge
    allDims.forEach(dim => {
      if (dim != 'time') {
        _relations_matrix['time'][dim].push(d)
        _relations_matrix[dim]['time'].push(d)
      }
    })
  })
}