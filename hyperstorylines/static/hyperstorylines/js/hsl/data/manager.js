import sceneStyle from '../vis/style/scenes.js'
import aggregatorFactory from '../aggregators/factory.js'
import filterManager from './filter.js'
import selectionManager from './selection.js'

export default function data() {
  var data = {}

  // DATA NEW
  var _nodesDict
  var _datasetName
  var _dimensions
  var _visibleEntities
  // TODO: IS THIS TOO UGLY? WHOULD IT BE IN FILTER?
  var _searchedEntities

  // NARRATIVES
  var _narrative

  // SELECT
  var _selection

  // FILTER
  var _filter

  // AGGREGATION
  var _aggregators = {}

  /////////////////////////////////////////////////////////////////////////////
  // INIT
  /////////////////////////////////////////////////////////////////////////////
  data.init = function (name, dimensions) {
    _datasetName = name
    _dimensions = dimensions
    data.createAggregators()
    _filter = filterManager().init()
    _selection = selectionManager().init()
    return data
  }

  // TODO: SEPARATE THIS FUNCTION INTO 2 TO AVOID REPETITION WHEN NESTED
  data.constructScenes = function (raw_data) {
    const nodes = raw_data.nodes_dict
    for (let nId in nodes) {
      const node = nodes[nId]
      if (!node.aggregated_nodes_ids) continue
      nodes[nId].aggregated_nodes = []
      node.aggregated_nodes_ids.forEach(aggrId => {
        nodes[nId].aggregated_nodes.push(nodes[aggrId])
      })
      node.related_edges = []
    }

    // 3) ASSOCIATE CHARACTERS IN EDGES TO REAL NODES
    const rawScenes = raw_data.scenes
    rawScenes.forEach(e => {
      e.characters = []
      e.charactersIds.forEach(cId => {
        if (nodes[cId] != undefined) {
          const node = nodes[cId]
          e.characters.push(node)
        }
      })
      e.edge.nodes_ids.forEach(cId => {
        if (nodes[cId] != undefined) {
          const node = nodes[cId]
          node.related_edges.push(e.edge.id)
        }
      })
      e.keyEntity = nodes[e.keyEntity['id']]
    })
    return nodes
  }

  data.updateEntitiesDict = function (nodes){
    data.nodesDict(nodes)
  }

  // TODO: AS THIS FUNCTION IS CALLED AFTER construcScenes
  // I'M DOING A DOUBLE ITERATION IN NODES AND EDGES.
  // UNNECESARY BUT KETP TO AVOID ANY POSSIBLE CRASH WITH THE
  // CONSTRUCT OF NESTED SCENES + MORE CLEAN CODE
  data.getStats = function (raw_data) {
    _visibleEntities = []
    _searchedEntities = []

    // 1) INITIALIZE THE STATS DICTS
    const termsStats = {}
    termsStats['relationships'] = {}
    termsStats['entities'] = {}
    const termsEntities = data.searchedTermsForEntities()
    termsEntities.forEach(term => {
      termsStats['entities'][term] = {}
      termsStats['entities'][term]['all_relationships'] = []
      termsStats['entities'][term]['visible_relationships'] = []
    })

    
    // 2) MAKE A PASS OVER THE NODES DICTIONATY TO
    // COLLECT STATS ABOUT THE SEARCHED TERMS
    const nodes = _nodesDict
    for (let nId in nodes) {
      const node = nodes[nId]     
      if (node.isTerm) {
        termsStats['relationships'][node.name] = {}
        termsStats['relationships'][node.name]['all_relationships'] = node.all_related_edges.length
        termsStats['relationships'][node.name]['visible_relationships'] = 0
      }
      termsEntities.forEach(term => {
        const name = node.name.toLowerCase()
        if (!node.isTerm && name.includes(term)) {
          if (!(node.type in termsStats['entities'][term])) {
            termsStats['entities'][term][node.type] = 0
          }
          termsStats['entities'][term][node.type] += 1
          _searchedEntities.push(node.id)
          termsStats['entities'][term]['all_relationships'] = termsStats['entities'][term]['all_relationships'].concat(node['original_edges_ids'])
        }
      })
    }
    

    // 3) ASSOCIATE CHARACTERS IN EDGES TO REAL NODES
    const rawScenes = raw_data.scenes
    rawScenes.forEach(e => {
      e.charactersIds.forEach(cId => {
        if (nodes[cId] != undefined) {
          _visibleEntities.push(cId)
        }
      })
      e.edge.nodes_ids.forEach(cId => {
        if (nodes[cId] != undefined) {
          const node = nodes[cId]
          if (node.isTerm && node['name'] in termsStats['relationships']) {
            termsStats['relationships'][node['name']]['visible_relationships'] += 1
          }
          const name = node['name'].toLowerCase()
          termsEntities.forEach(term => {
            if (!node.isTerm && name.includes(term)) {
              termsStats['entities'][term]['visible_relationships'].push(e.edge.id)
            }
          })
        }
      })
    })
    for (let term in termsStats['entities']) {
      termsStats['entities'][term]['visible_relationships'] = Array.from(new Set(termsStats['entities'][term]['visible_relationships']))
      termsStats['entities'][term]['visible_relationships'] = termsStats['entities'][term]['visible_relationships'].length
      termsStats['entities'][term]['all_relationships'] = Array.from(new Set(termsStats['entities'][term]['all_relationships']))
      termsStats['entities'][term]['all_relationships'] = termsStats['entities'][term]['all_relationships'].length
    }

    _visibleEntities = Array.from(new Set(_visibleEntities))
    _searchedEntities = Array.from(new Set(_searchedEntities))
    return termsStats
  }

  /////////////////////////////////////////////////////////////////////////////
  // GETTERS ORIGINAL DATA
  /////////////////////////////////////////////////////////////////////////////
  data.getOriginalnode = function (id) {
    if (id in _nodesDict) {
      return _nodesDict[id]
    }
    return undefined
  }
  data.getOriginalEntity = function (id) {
    return data.getOriginalnode(id)
  }

  data.datasetName = function () {
    return _datasetName
  }

  data.nodesDict = function (aDict) {
    if (aDict == undefined) {
      return _nodesDict
    }
    _nodesDict = aDict
    return data
  }

  data.entitiesDictionary = function () {
    return _nodesDict
  }

  data.getNumberOfRelationships = function (entity) {
    return entity.related_edges.length
  }

  data.getNumberOfAllRelationships = function (entity) {
    return entity.original_edges_ids.length
  }

  data.getAllEntitiesNamesPerDimension = function (edge) {
    const entitiesNamesperDim = {}
    for (let dim in edge.node_types_ids) {
      entitiesNamesperDim[dim] = []
      edge.node_types_ids[dim].forEach(n => {
        const node = data.getOriginalnode(n)
        if (node != undefined) {
          entitiesNamesperDim[dim].push(node.name)
        }
      })
    }
    return entitiesNamesperDim
  }

  data.isEdgeVisible = function (edge, yDim) {
    return edge.node_types_ids[yDim].length == 0
  }

  /////////////////////////////////////////////////////////////////////////////
  // GETTERS FOR SELECTORS
  /////////////////////////////////////////////////////////////////////////////
  data.dimensions = function () {
    return _dimensions
  }

  data.dimensionsNames = function () {
    return Object.keys(_dimensions)
  }

  data.aggregations = function () {
    const r = {}
    for (let d in _dimensions) {
      r[d] = _aggregators[d].getLevels()
    }
    return r
  }

  data.visibleEntities = function () {
    return _visibleEntities
  }

  /////////////////////////////////////////////////////////////////////////////
  // AGGREGATION
  /////////////////////////////////////////////////////////////////////////////
  data.createAggregators = function () {
    for (let d in _dimensions) {
      _aggregators[d] = aggregatorFactory(d)
    }
  }

  data.getAggregator = function (typeEntity) {
    return _aggregators[typeEntity]
  }

  data.getAllAggregators = function () {
    return _aggregators
  }

  data.isXDimAggregated = function (xDim, xAggr) {
    return _aggregators[xDim].ignoredLevels().indexOf(xAggr) < 0
  }

  /////////////////////////////////////////////////////////////////////////////
  // NARRATIVE
  /////////////////////////////////////////////////////////////////////////////
  data.narrative = function (newNarrative) {
    if (newNarrative == undefined) {
      return _narrative
    } else {
      _narrative = newNarrative
      return data
    }
  }
  data.createNarrative = function (newScenes, width, height) {
    const narrative = d3
      .narrative()
      .scenes(newScenes)
      .size([width, height])
      .pathSpace(sceneStyle('pathSpace'))
      .groupMargin(sceneStyle('groupMargin'))
      .labelSize(sceneStyle('labelSize'))
      .scenePadding([
        5,
        sceneStyle('sceneWidth') / 2,
        5,
        sceneStyle('sceneWidth') / 2 + sceneStyle('leftPadding'),
      ])
      .labelPosition('left')
      .layout()
    return narrative
  }

  data.updateNarrative = function (newScenes, width, height) {
    _narrative = data.createNarrative(newScenes, width, height)
    return _narrative
  }

  /////////////////////////////////////////////////////////////////////////////
  // SEARCH AND FILTERING COMMON
  /////////////////////////////////////////////////////////////////////////////
  data.resetFilterAndSearch = function () {
    _filter.resetFilterAndSearch()
  }

  data.filteredAndSearchedEntities = function () {
    const r = _searchedEntities.concat(data.filteredEntities())
    return Array.from(new Set(r))
  }

  /////////////////////////////////////////////////////////////////////////////
  // FILTERING
  /////////////////////////////////////////////////////////////////////////////
  data.isFilterActive = function () {
    return _filter.isFilterActive()
  }

  // FILTERING BY ENTITY
  data.filterByEntity = function (entity) {
    _filter.filterByEntity(data, entity)
    return data
  }

  data.filteredEntities = function () {
    return _filter.filteredEntities()
  }

  data.removeFilteredEntity = function (entityId) {
    return _filter.removeFilteredEntity(entityId)
  }
  /////////////////////////////////////////////////////////////////////////////
  // SEARCH BY TERM ENTITIES
  /////////////////////////////////////////////////////////////////////////////
  data.searchEntitiesByTerm = function (text) {
    return _filter.searchEntitiesByTerm(text)
  }

  data.removeTermForEntities = function (text) {
    _filter.removeTermForEntities(text)
    return data
  }

  data.searchedTermsForEntities = function () {
    return _filter.searchedTermsForEntities()
  }

  /////////////////////////////////////////////////////////////////////////////
  // SEARCH BY TERM RELATIONSHIPS
  /////////////////////////////////////////////////////////////////////////////
  data.searchRelationshipsByTerm = function (text) {
    return _filter.searchRelationshipsByTerm(text)
  }

  data.removeTermForRelationships = function (text) {
    _filter.removeTermForRelationships(text)
    return data
  }

  data.searchedTermsForRelationships = function () {
    return _filter.searchedTermsForRelationships()
  }

  data.termEntities = function () {
    const r = []
    for (let nId in _nodesDict) {
      const node = _nodesDict[nId]
      if (node.isTerm) {
        r.push(nId)
      }
    }
    return r
  }

  /////////////////////////////////////////////////////////////////////////////
  // SELECTION
  /////////////////////////////////////////////////////////////////////////////
  data.isSelectionActive = function () {
    return _selection.isSelectionActive()
  }

  data.toggleSelection = function (entity) {
    _selection.toggleSelection(entity)
    return data
  }

  data.toggleSeveralEntities = function (text, select) {
    _selection.toggleSeveralEntities(text, select)
    return data
  }

  data.resetSelected = function () {
    _selection.resetSelected()
  }

  data.isSceneSelected = function (scene, selections) {
    return _selection.isSceneSelected(scene, selections)
  }

  data.isSceneRelatedSelected = function (scene) {
    return _selection.isSceneRelatedSelected(scene)
  }

  data.isEntitySelected = function (node) {
    return _selection.isEntitySelected(node)
  }

  data.isEntityRelatedSelected = function (node, selections) {
    return _selection.isEntityRelatedSelected(node, selections)
  }

  data.isSelected = function (entityId) {
    return _selection.isSelected(entityId)
  }

  data.isRelatedSelected = function (entityId) {
    return _selection.isRelatedSelected(entityId)
  }

  return data
}
