import data from './data/manager.js'
import chart from './vis/chart.js'
import barchart from './vis/barchart.js'
import uiSelectors from './ui/selectors.js'
import { addArticles, removeArticles } from './hsl-articles.js'
import { getScenesURL, getNestedScenesURL, removeSpecialCharacters, getDataURL, getDatasetsAvailableURL, loading, ready } from './hsl-utils.js'
// HERE
import entityInteractions from './interactions/entity.js'
import backgroundInteractions from './interactions/background.js'
import scenesInteractions from './interactions/scenes.js'
import { updateExtraLinks } from './vis/nestedlines.js'



// TODO: this is horrible
//import scenes from './vis/scenes.js'

export default function hsl() {
  var _data
  var _uiSelectors
  var _chart
  var _entityInteractions
  var _backgroundInteractions
  var _scenesInteractions
  var _barchart

  //////////////////////////////////////////////////////////////////////////////
  // INITIALIZE
  //////////////////////////////////////////////////////////////////////////////
  var hsl = {}
  hsl.init = function () {
    // SETUP INTERNAL VARIABLES
    _barchart = barchart().hsl(hsl)
    _backgroundInteractions = backgroundInteractions().hsl(hsl)
    _entityInteractions = entityInteractions().hsl(hsl)
    _scenesInteractions = scenesInteractions().hsl(hsl)
    hsl.addSearchByTermListeners()
    _chart = chart()
    _uiSelectors = uiSelectors().hsl(hsl)
    const url = getDatasetsAvailableURL()
    loading()
    d3.json(url).then(function (data) {
      hsl.changeDataset(data.datasets[0])
      ready()
    })
    return hsl
  }

  hsl.show = function () {
    hsl.createContainers()
    _uiSelectors = uiSelectors()
      .hsl(hsl)
      .dimensions(_data.dimensions())
      .aggregations(_data.aggregations())
      .addHelp()
    _chart.dimensions(_uiSelectors.getAllDimensions())
    _barchart.dimensions(
      _uiSelectors.getAllDimensions(),
      _uiSelectors.getAllSelections(),
    )
    hsl.updateNarrative()
    return hsl
  }

  hsl.data = function (name, dimensions) {
    _data = data().init(name, dimensions)
    return hsl
  }

  hsl.createContainers = function () {
    const main = d3.select('.hsl-container')
    const selectors = main
      .append('div')
      .attr('class', 'hsl-selectors')
      .attr('id', 'dimension-select')
    const dims = ['x', 'y', 'z']
    dims.forEach((d) => {
      selectors.append('div').attr('class', 'hsl-' + d + 'selector')
    })
    const extraSelectors = selectors
      .append('div')
      .attr('class', 'hsl-extraselector')
    extraSelectors
      .append('button')
      .attr('id', 'help')
      .attr('class', 'btn btn-secondary helpContent_open')
      .html('Help')
    main.append('div').attr('class', 'hsl-legend-x')
    main.append('div').attr('class', 'hsl-legend-y')
    main.append('div').attr('class', 'hsl-scene')
    main.append('div').attr('class', 'hsl-vertical-scroll')
    main.append('div').attr('class', 'hsl-horizontal-scroll')
    _chart.createContainers()
    d3.select('#hsl-main').select('#hsl-legend-y-g').raise()
  }

  //////////////////////////////////////////////////////////////////////////////
  // DRAWING AND CLEAN
  //////////////////////////////////////////////////////////////////////////////
  hsl.draw = function () {
    d3.select('#hsl-scene-svg').attr('visibility', 'hidden')
    if (_data.narrative().scenes().length == 0) {
      _chart.drawNoData(_backgroundInteractions)
    } else {
      _chart.draw(
        _data.narrative(),
        _entityInteractions,
        _backgroundInteractions,
        _scenesInteractions,
      )
    }

    _barchart.draw(
      _data.entitiesDictionary(),
      _backgroundInteractions,
      _entityInteractions,
    )
    hsl.updateAfterFilterAndSearch()
    hsl.updateSelected()
    _chart.resetScrollbars()
    // TODO: when reopen scenes open those articles too
    hsl.removeArticles()
    hsl.expandAllScenes()
    return hsl
  }

  hsl.reset = function () {
    d3.select('.hsl-container').selectAll('*').remove()
    d3.select('#searchByTermContainer').selectAll('*').remove()
    _barchart.reset()
    _uiSelectors.reset()
    _chart.reset()
    return hsl
  }

  hsl.drawSummary = function () {
    _barchart.draw(
      _data.entitiesDictionary(),
      _backgroundInteractions,
      _entityInteractions,
    )
    _barchart.updateAfterFilterAndSearch(_data.filteredEntities())
    _barchart.updateSelected(_data)
  }

  //////////////////////////////////////////////////////////////////////////////
  // UPDATE
  //////////////////////////////////////////////////////////////////////////////
  hsl.updateNarrative = function () {
    const selections = _uiSelectors.getAllSelections()
    const datasetName = _data.datasetName()
    var url = getScenesURL(selections,
      datasetName,
      _data.filteredEntities(),
      _data.searchedTermsForEntities(),
      _data.searchedTermsForRelationships())
    loading()
    d3.json(url)
      .then(function (data) {
        const entitiesDict = hsl.constructScenes(data)
        _data.updateEntitiesDict(entitiesDict)
        const termsStats = _data.getStats(data)
        hsl.updateFilterAndSearchBoxes(termsStats)
        const newScenes = data.scenes
        const nScenes = newScenes.length
        const chartDimensions = _chart.getDimensions(nScenes)
        _chart.size(chartDimensions[0], chartDimensions[1])
        _data.updateNarrative(newScenes, _chart.width, _chart.height)
        _chart.updateSizeNarrative(_data.narrative())
        hsl.draw()
        ready()
      })
  }

  hsl.update = function (narrative) {
    const animationDuration = 1000
    return _chart.update(narrative, animationDuration)
  }

  hsl.constructScenes = function (data) {
    return _data.constructScenes(data)
  }

  hsl.updateFilterAndSearchBoxes = function (termsStats) {
    hsl.addAllSearchedEntitiesTermBoxes(termsStats['entities'])
    hsl.addAllSearchedRelationshipsTermBoxes(termsStats['relationships'])
    hsl.addAllFilteredEntitiesBoxes()
  }

  hsl.animatedTransition = function () {
    d3.selectAll('#hsl-main').attr('transform', 'translate(-195, 25)')
    d3.select('#hsl-legend-y-svg')
      .selectAll('#hsl-legend-y-g')
      .attr('transform', 'translate(0, 25)')
    const selections = _uiSelectors.getAllSelections()
    const datasetName = _data.datasetName()
    var url = getScenesURL(selections,
      datasetName,
      _data.filteredEntities(),
      _data.searchedTermsForEntities(),
      _data.searchedTermsForRelationships())
    d3.json(url)
      .then(function (data) {
        const entitiesDict = hsl.constructScenes(data)
        _data.updateEntitiesDict(entitiesDict)
        const termsStats = _data.getStats(data)
        hsl.updateFilterAndSearchBoxes(termsStats)
        const newScenes = data.scenes
        const nScenes = newScenes.length
        const chartDimensions = _chart.getDimensions(nScenes)
        _chart.size(chartDimensions[0], chartDimensions[1])
        const oldNarrative = _data.narrative()
        const newNarrative = _data.updateNarrative(newScenes, _chart.width, _chart.height)
        _chart.transitionFilter(_data.visibleEntities())
        setTimeout(function () {
          if (oldNarrative.scenes().length == 0) {
            _chart.updateSizeNarrative(newNarrative)
            hsl.draw()
          } else {
            oldNarrative.translateAll(newNarrative)
            Promise.all(hsl.update(oldNarrative)).then(() => {
              _chart.updateSizeNarrative(newNarrative)
              hsl.draw()
            })
          }
        }, 200)
      })
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO UI SELECTORS
  //////////////////////////////////////////////////////////////////////////////
  hsl.changeDimension = function () {
    _uiSelectors.updateAggregators()
    hsl.updateNarrative()
  }

  hsl.changeAggregation = function () {
    hsl.updateNarrative()
  }

  hsl.changeOrder = function () {
    hsl.updateNarrative()
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO SELECTION
  //////////////////////////////////////////////////////////////////////////////
  hsl.select = function (entity) {
    _data.toggleSelection(entity)
    hsl.updateSelected()
  }

  hsl.updateSelected = function () {
    _chart.updateSelected(_data, _uiSelectors.getAllSelections())
    _barchart.updateSelected(_data)
  }

  hsl.resetSelected = function () {
    _data.resetSelected()
    hsl.updateSelected()
  }

  //////////////////////////////////////////////////////////////////////////////
  // SEARCH AND FILTER COMMON
  //////////////////////////////////////////////////////////////////////////////
  hsl.addSearchedTermBox = function (text, entityId, stats, type) {
    const clean_text = removeSpecialCharacters(text)
    // Don't add a new box if there is already one
    var box = d3.select('#searchbyTermContainer').select('#search-kw-' + clean_text + '_' + type)
    if (box.empty()) {
      box = d3
        .select('#searchByTermContainer')
        .append('div')
        .attr('id', 'search-kw-' + clean_text + '_' + type)
        .style('padding', '3px')
        .style('margin', '3px')
        .style('background-color', () => {
          if (type == 'term_relationship') {
            return '#d94801'
          } else {
            return '#3182bd'
          }
        })
        .style('display', 'inline-block')
        .style('color', '#fff')
      box
        .append('div')
        .style('float', 'right')
        .style('display', 'inline-block')
        .style('margin-left', '3px')
        .append('a')
        .attr('id', 'search-kw-x-' + clean_text)
        .on('click', (d) => hsl.removeSearchedTerm(text, entityId, type))
        .on('mouseover', (d) => {
          d3.select('#search-kw-x-' + clean_text).style(
            'text-decoration',
            'underline',
          )
        })
        .on('mouseout', (d) => {
          d3.select('#search-kw-x-' + clean_text).style(
            'text-decoration',
            'none',
          )
        })
        .style('color', '#fff')
        .html('x')
      box
        .append('div')
        .attr('class', 'search-stats')
        .style('display', 'inline-block')
    }
    var statsString = ''
    for (let type in stats) {
      statsString += ', ' + stats[type] + ' ' + type
    }
    if (statsString == '') {
      statsString = '  0 results'
    }
    box
      .select('.search-stats')
      .html(text + ' (' + statsString.substring(2) + ')')
  }

  hsl.updateAfterFilterAndSearch = function () {
    _chart.updateAfterFilterAndSearch(_data.filteredAndSearchedEntities(), _data.termEntities())
    _barchart.updateAfterFilterAndSearch(_data.filteredAndSearchedEntities())
  }

  hsl.resetFilterAndSearch = function () {
    _data.resetFilterAndSearch()
    d3.select('#searchByTermContainer').selectAll('*').remove()
  }
  //////////////////////////////////////////////////////////////////////////////
  // SEARCH BY TERMS
  //////////////////////////////////////////////////////////////////////////////
  hsl.addSearchByTermListeners = function () {
    document
      .getElementById('searchEntityButton')
      .addEventListener('click', function () {
        const text = d3.select('#searchInput').node().value
        hsl.searchEntitiesByTerm(text)
      })
    document
      .getElementById('searchByTermButton')
      .addEventListener('click', function () {
        const text = d3.select('#searchInput').node().value
        hsl.searchRelationshipsByTerm(text)
      })
    d3.selectAll('#clearAllSearchsButton').on('click', function () {
      const text = d3.select('#searchInput').node().value
      hsl.clearSearchByTerm()
    })
  }

  hsl.addAllSearchedRelationshipsTermBoxes = function (terms_stats) {
    for (let term in terms_stats) {
      const aStat = terms_stats[term]
      aStat['relationships'] = aStat['visible_relationships'] + '/' + aStat['all_relationships']
      delete aStat['visible_relationships']
      delete aStat['all_relationships']
      hsl.addSearchedTermBox(term, undefined, aStat, 'term_relationship')
    }
  }

  hsl.addAllSearchedEntitiesTermBoxes = function (terms_stats) {
    for (let term in terms_stats) {
      const aStat = terms_stats[term]
      aStat['relationships'] = aStat['visible_relationships'] + '/' + aStat['all_relationships']
      delete aStat['visible_relationships']
      delete aStat['all_relationships']
      hsl.addSearchedTermBox(term, undefined, aStat, 'term_entity')
    }
  }

  hsl.addAllFilteredEntitiesBoxes = function () {
    const filteredEntities = _data.filteredEntities()
    filteredEntities.forEach(eId => {
      const entity = _data.getOriginalEntity(eId)
      const stats = {}
      stats[entity.type] = 1
      stats['relationships'] = _data.getNumberOfRelationships(entity) + '/' + _data.getNumberOfAllRelationships(entity)
      hsl.addSearchedTermBox(entity.name, entity.id, stats, 'entity')
    })
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO FILTERING
  //////////////////////////////////////////////////////////////////////////////
  hsl.filter = function (entity, reset) {
    if (entity == undefined) {
      if (_data.isFilterActive()) {
        hsl.resetFilterAndSearch()
      } else {
        return
      }
    } else {
      if (reset) {
        hsl.resetFilterAndSearch()
      }
      hsl.filterByEntity(entity)
    }
    hsl.animatedTransition()
  }

  hsl.filterByEntity = function (entity) {
    _data.filterByEntity(entity)
    const stats = {}
    stats[entity.type] = 1
    stats['relationships'] = _data.getNumberOfRelationships(entity)
    hsl.addSearchedTermBox(entity.name, entity.id, stats, 'entity')
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO SEARCH
  //////////////////////////////////////////////////////////////////////////////
  // SEARCH ENTITY BY TERM
  hsl.searchEntitiesByTerm = function (text) {
    _data.searchEntitiesByTerm(text)
    hsl.cleanSearchInput()
    hsl.animatedTransition()
  }

  // SEARCH BY KEYWORDS
  hsl.searchRelationshipsByTerm = function (text) {
    _data.searchRelationshipsByTerm(text)
    hsl.cleanSearchInput()
    hsl.animatedTransition()
  }

  hsl.cleanSearchInput = function () {
    document.getElementById('searchInput').value = ''
  }

  // SEARCH HISTORY
  hsl.removeSearchedTerm = function (text, entityId, type) {
    const regex = new RegExp(' ', 'g')
    const text_no_spaces = text.replace(regex, '-')
    if (type == 'term_relationship') {
      _data.removeTermForRelationships(text)
    } else if (type == 'term_entity') {
      _data.removeTermForEntities(text)
    } else if (type == 'entity') {
      _data.removeFilteredEntity(entityId)
    } else {
      // THERE IS AN ERROR IF THERE IS SOMETHING ELSE
    }
    d3.select('#search-kw-' + text_no_spaces + '_' + type).remove()
    hsl.animatedTransition()
  }

  hsl.clearAllSearchs = function () {
    _data.resetFilterAndSearch()
    d3.select('#searchByTermContainer').selectAll('*').remove()
    hsl.animatedTransition()
  }

  //////////////////////////////////////////////////////////////////////////////
  // NESTED SCENES
  //////////////////////////////////////////////////////////////////////////////
  hsl.expandScene = function (scene, callback = false) {
    const animationDuration = 500
    const selections = _uiSelectors.getAllSelections()
    var shiftDirection = 1
    var promises
    if (scene.isOpen) {
      const datasetName = _data.datasetName()
      const url = getNestedScenesURL(scene,
        selections,
        datasetName,
        _data.filteredEntities(),
        _data.searchedTermsForEntities(),
        _data.searchedTermsForRelationships())
      d3.json(url)
        .then(function (data) {
          hsl.constructScenes(data)
          const nestedScenes = data.scenes
          scene.nestedScenes = nestedScenes
          const isXDimAggr = _data.isXDimAggregated(selections.dim.x, selections.aggr.x)
          _chart.expandScene(
            scene,
            animationDuration,
            nestedScenes,
            _entityInteractions,
            _data.narrative(),
            isXDimAggr
          )
          promises = updateAfterExpandScene(
            scene,
            shiftDirection,
            animationDuration,
          )
          Promise.all(promises).then(() => {
            hsl.updateSelected()
            hsl.updateAfterFilterAndSearch()
            _chart.axes().yAxis().updateScroll()
          })
          hsl.addArticles(scene)
          // I really don't like this:
          if (callback) {
            callback()
          }
        })
    } else {
      scene.nestedScenes = undefined
      shiftDirection = -1
      promises = updateAfterExpandScene(
        scene,
        shiftDirection,
        animationDuration,
      )
      _chart.collapseScene(
        scene,
        animationDuration,
        _data.narrative(),
        selections,
      )
      hsl.removeArticles(scene)
      Promise.all(promises).then(() => {
        hsl.updateSelected()
        hsl.updateAfterFilterAndSearch()
        _chart.axes().yAxis().updateScroll()
      })
    }
  }

  function updateAfterExpandScene(scene, shiftDirection, animationDuration) {
    const shift = _chart.getShiftValuesExpandedScene(scene, shiftDirection)
    _data.narrative().unfoldOriginLayout(scene, shift[0], shift[1])
    const promises = _chart.updateShift(
      _data.narrative(),
      animationDuration,
      shift[0],
      shift[1],
    )
    return promises
  }

  hsl.expandAllScenes = function () {
    hsl.expandSceneRecursive(0, 0)
  }

  hsl.expandSceneRecursive = function (edge_index, scene_index) {
    const narrative = _data.narrative()
    const activeEdges = _chart.getAllExpandedScenes()
    const selections = _uiSelectors.getAllSelections()
    const aggregators = _data.getAllAggregators()
    // This is the one in charge of drawing
    // TODO: maybe move somewhere else
    if (activeEdges.length == 0 || edge_index >= activeEdges.length) {
      d3.select('#hsl-scene-svg').attr('visibility', 'visible')
      hsl.updateSelected()
      hsl.updateAfterFilterAndSearch()
      updateExtraLinks(narrative, 0)
      setTimeout(() => {
        _chart.axes().yAxis().updateScroll()
      }, 100);
      return
    }
    const edgeId = activeEdges[edge_index]
    const scenes = narrative.getSceneByOriginalEdgeId(edgeId)
    // CONTINUE WITH THE NEXT EDGE
    if (scene_index >= scenes.length) {
      return hsl.expandSceneRecursive(edge_index + 1, 0)
    }

    // IF NOT CONTINUE WITH THE NEXT SCENE OF THE SAME EDGE
    const scene = scenes[scene_index]

    // IF THAT SCENE IS ALREADY OPEN< TRY WITH THE NEXT SCENE
    if (scene.isOpen) {
      return hsl.expandSceneRecursive(edge_index, scene_index + 1)
    }
    scene.isOpen = true
    const callback = () => {
      hsl.expandSceneRecursive(edge_index, scene_index + 1)
    }
    hsl.expandScene(scene, callback)
  }

  hsl.reorderExpandedScenes = function () {
    const selections = _uiSelectors.getAllSelections()
    _chart.reorderExpandedScenes(_data, selections)
  }

  //////////////////////////////////////////////////////////////////////////////
  // ADD ARTICLES TEXT
  //////////////////////////////////////////////////////////////////////////////
  hsl.addArticles = function (scene) {
    const dims = _uiSelectors.getActiveDimensions()
    addArticles(_data, scene, dims.y)
  }
  hsl.removeArticles = function (scene) {
    removeArticles(scene)
  }

  //////////////////////////////////////////////////////////////////////////////
  // DATASET SELECTOR
  //////////////////////////////////////////////////////////////////////////////
  hsl.changeDataset = function (name) {
    const url = getDataURL(name)
    d3.json(url).then((data) => {
      hsl.data(name, data).reset().show()
      d3.select('#dataset-selector-label').node().innerHTML = name
    })
  }
  return hsl
}
