import hierarchicalAggregator from './hierarchical.js'

export default function temporalAggregator() {
  const levels_names = {
    aggr_article: 'Article',
    aggr_day: 'Day',
    aggr_month: 'Month',
    aggr_year: 'Year',
  }
  const _typeEntity = 'time'
  const aggregator = hierarchicalAggregator().init(_typeEntity, levels_names)
  
  aggregator.ignoredLevels = function(){
    return ['aggr_article']
  }
  return aggregator
}