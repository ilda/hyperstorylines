import hierarchicalAggregator from './hierarchical.js'

export default function defaultAggregator(_typeEntity) {
  const levels_names = {
    aggr_article: 'Article',
  }
  const aggregator = hierarchicalAggregator().init(_typeEntity, levels_names)
  aggregator.aggregateElement = function (element, aggregationType) {
    return element
  }
  return aggregator
}