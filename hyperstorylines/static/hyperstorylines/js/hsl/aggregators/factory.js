import temporalAggregator from './temporal.js'
import defaultAggregator from './default.js'
import locationsAggregator from './locations.js'


export default function aggregatorFactory(type) {
  if (type == 'time') {
    return temporalAggregator()
  }
  else if (type == 'locations') {
    return locationsAggregator()
  } else {
    return defaultAggregator(type)
  }
}