
export default function hierarchicalAggregator() {
  // THIS IS AN ABSTRACT "CLASS". 
  // Sorry if I'm breaking too many design patterns

  var aggregator = {}

  var _levels
  var _typeEntity

  var _aggregatedValues = {}

  // levels must be a list with the aggregation levels
  // They are ordered from the highest level to the lowest
  // The first element represents the higher level
  aggregator.init = function (typeEntity, levels) {
    _typeEntity = typeEntity
    _levels = levels
    return aggregator
  }

  aggregator.getLevels = function () {
    return _levels
  }

  aggregator.ignoredLevels = function () {
    return ['aggr_article']
  }

  aggregator.aggregateElement = function (element, aggregationLevel) {
    return element
  }

  aggregator.computeAggregatedHypergraph = function (datasetName, dim, aggr, tmpHypergraph, callback) {
    _aggregatedValues = {}
    const url = '/get_aggregated_values/?q=' + datasetName + '&a=' + aggr + '&t=' + dim
    d3.json(url)
      .then(function (data) {
        _aggregatedValues = data.data
        return aggregator.getAggregatedHypergraph(aggr, tmpHypergraph, callback)
      })
  }

  aggregator.getAggregatedHypergraph = function (aggr, tmpHypergraph, callback) {
    var _edges = {}
    var _nodes = {}
    const tmpEdges = tmpHypergraph.edges
    const tmpNodes = tmpHypergraph.nodes
    const toIgnore = aggregator.ignoredLevels()
    if (toIgnore.indexOf(aggr) >= 0) {
      for (let n in tmpNodes) {
        _nodes[n] = tmpNodes[n]
        _nodes[n].original_edges_ids = _nodes[n].related_edges
        _nodes[n].related_edges = []

      }
      for (let e in tmpEdges) {
        _edges[e] = tmpEdges[e]
        _edges[e].original_edges_ids = [e]
      }
    } else {
      //var _edgesBuiltRawMap = {}
      var _nodesBuiltRawMap = {}
      var _nodesRawBuiltMap = {}
      var _nodesRawBuiltMapAll = {}

      for (var nodeId in tmpNodes) {
        const node = tmpNodes[nodeId]
        const name = node.name
        if (node.type == _typeEntity) {
          const newDate = _aggregatedValues[nodeId]
          if (!(newDate in _nodesBuiltRawMap)) {
            _nodesBuiltRawMap[newDate] = []
            _nodes[newDate] = node
            _nodes[newDate].name = newDate
            _nodes[newDate].id = newDate
            _nodes[newDate].original_nodes = []
            _nodes[newDate].original_edges_ids = []
          }
          _nodesBuiltRawMap[newDate].push(node.id)
          _nodesRawBuiltMap[nodeId] = newDate
          _nodesRawBuiltMapAll[nodeId] = newDate
          _nodes[newDate].original_nodes.push(nodeId)
          _nodes[newDate].original_edges_ids = _nodes[newDate].original_edges_ids.concat(node.related_edges)
          _nodes[newDate].related_edges = []
          for (let key in _nodes[newDate].related_nodes) {
            _nodes[newDate].related_nodes[key] = _nodes[newDate].related_nodes[key].concat(node.related_nodes[key])
            _nodes[newDate].related_nodes[key] = Array.from(new Set(_nodes[newDate].related_nodes[key]))
          }
        } else {
          _nodes[nodeId] = tmpNodes[nodeId]
          _nodes[nodeId].original_edges_ids = _.cloneDeep(tmpNodes[nodeId].related_edges)
          _nodes[nodeId].original_nodes = [nodeId]
          _nodes[nodeId].related_edges = []
          _nodesRawBuiltMapAll[nodeId] = nodeId
        }
      }

      for (var edgeId in tmpEdges) {
        const edge = tmpEdges[edgeId]
        var key
        edge.nodes_ids.forEach(nodeId => {
          if (nodeId in _nodesRawBuiltMap)
            key = _nodesRawBuiltMap[nodeId]
        })
        if (!key)
          break // I should never reach this point bacause each edge has ONE date!
        if (!(key in _edges)) {
          _edges[key] = tmpEdges[edgeId]
          _edges[key].id = key
          _edges[key].node_types_ids[_typeEntity] = [key]
          //_edges[key].nodes_ids = _edges[key].nodes_ids.push(key)
          _edges[key].nodes_ids.push(key)
          _edges[key].original_edges_ids = [edgeId]
        } else {
          _edges[key].original_edges_ids.push(edgeId)
          _edges[key].nodes_ids = _edges[key].nodes_ids.concat(edge.nodes_ids)
          _edges[key].sources = _edges[key].sources.concat(edge.sources)
          _edges[key].nodes_ids = Array.from(new Set(_edges[key].nodes_ids)) //.push(...(edge.nodes_ids))
          for (let key2 in _edges[key].node_types_ids) {
            _edges[key].node_types_ids[key2] = _edges[key].node_types_ids[key2].concat(edge.node_types_ids[key2])
            _edges[key].node_types_ids[key2] = Array.from(new Set(_edges[key].node_types_ids[key2]))
          }
        }
      }
    }
    for (var builtEdgeId in _edges) {
      var builtEdge = _edges[builtEdgeId]
      builtEdge.nodes_ids.forEach(n => {
        if (n in _nodes) {
          _nodes[n].related_edges.push(builtEdgeId)
          for (let type in builtEdge.node_types_ids) {
            _nodes[n].related_nodes[type] = _nodes[n].related_nodes[type].concat(builtEdge.node_types_ids[type])
          }
        }
      })
    }
    const hypergraph = {}
    hypergraph.edges = _edges
    hypergraph.nodes = _nodes
    //dataManager.setNewHypergraph(hsl, hypergraph)
    callback(hypergraph)
  }

  aggregator.getChildNodes = function (parent, nodes, aggr) {
    let results = []
    nodes.forEach(n => {
      if (aggregator.isChild(parent, n, aggr)) {
        results.push(n)
      }
    })
    return results
  }

  aggregator.isChild = function (parent, node, aggr) {
    //let transformed = aggregator.aggregateElement(node.name, aggr)
    //return transformed == parent.name
    return aggregator.areRelated(parent, node)
  }

  aggregator.areRelated = function (node1, node2) {
    var related = false
    if (node1.name in _aggregatedValues) {
      if (_aggregatedValues[node1.name] == node2.name)
        related = true
    }
    if (node2.name in _aggregatedValues) {
      if (_aggregatedValues[node2.name] == node1.name)
        related = true
    }
    /*_levels.forEach(l => {
      if (node1.name in _aggregatedValues)
      const r1 = aggregator.isChild(node1, node2, l)
      const r2 = aggregator.isChild(node2, node1, l)
      related = related || r1 || r2
    })*/
    return related
  }

  return aggregator
}