import hierarchicalAggregator from './hierarchical.js'

export default function locationsAggregator() {
  const levels_names = {
    aggr_article: 'Article',
    aggr_city: 'City',
    aggr_region: 'Region',
    aggr_country: 'Country',
  }
  const _typeEntity = 'locations'
  const aggregator = hierarchicalAggregator().init(_typeEntity, levels_names)
  aggregator.ignoredLevels = function(){
    return ['aggr_article', 'aggr_city']
  }
  return aggregator
}