import { getSourceURL } from "./hsl-utils.js"

// ADD ARTICLES TEXT
export function addArticles(_data, scene, yDim) {
  const sceneDiv = d3.select('.hsl-article').append('div').attr('id', 'article_' + scene.id)
  const showedEdges = {}
  const datasetName = _data.datasetName()
  scene.nestedScenes.forEach(e => {
    const edge = e.edge
    if(showedEdges[edge.id]) return
    // Don't show the articles that don't have entities in the active Y dimension
    if (_data.isEdgeVisible(edge, yDim)) return
    // UGLY HACK< IS THERE SOMETHING WRONG WITH THE GENERATION OF THE GRAPH?
    showedEdges[edge.id] = true

    const edgeDiv = sceneDiv.append('div')
    edgeDiv.append('h4').html('EDGE ID: ' + edge.id)

    const namesPerDim = _data.getAllEntitiesNamesPerDimension(edge)
    var allNames = []
    for (let dim in namesPerDim){
      allNames = allNames.concat(namesPerDim[dim])
      var title = dim.charAt(0).toUpperCase() + dim.slice(1);
      edgeDiv.append('h4').html(title + ': <b class=highlightEntity>' + namesPerDim[dim].join(', ')) + '</b>'
    }    
    edge.sources.forEach(p => {
      const url = getSourceURL(p, datasetName)
      d3.json(url).then(function (text) {
        text = JSON.stringify(text)
        const pubDiv = edgeDiv.append('div')
        pubDiv.append('h4').html('Source: ' + p)
        allNames.forEach(name => {
          var matchIndices = text.getAllIndices(name.toLowerCase())
          var next = matchIndices.next()
          var indexAccumulated = 0
          var startExtra = '<b class=highlightEntity>'
          var endExtra = '</b>'
          while (!next.done) {
            var index = next.value.index + indexAccumulated
            text = text.substring(0, index) + startExtra + text.substring(index, index + name.length) + endExtra + text.substring(index + name.length)
            indexAccumulated += startExtra.length + endExtra.length
            next = matchIndices.next()
          }
        })
        const terms = _data.searchedTermsForRelationships()
        terms.forEach(t => {
          var matchIndices = text.getAllIndices(t)
          var next = matchIndices.next()
          var indexAccumulated = 0
          var startExtra = '<b class=highlightKeyword>'
          var endExtra = '</b>'
          while (!next.done) {
            var index = next.value.index + indexAccumulated
            text = text.substring(0, index) + startExtra + text.substring(index, index + t.length) + endExtra + text.substring(index + t.length)
            indexAccumulated += startExtra.length + endExtra.length
            next = matchIndices.next()
          }
        })
        pubDiv.append('p').html(text)
      })
    })
  })
}

export function removeArticles(scene) {
  if (scene == undefined) {
    d3.selectAll('.hsl-article').selectAll('*').remove()
    return
  }
  d3.selectAll('#article_' + scene.id).remove()
}