export default function logInteraction(data) {
  var formatTime = d3.timeFormat('%Y-%m-%d %H:%M:%S.%L')
  data.timestamp = formatTime(new Date())
  $.ajax({
    'url': '/log_interaction',
    'type': 'post',
    'data': data,
    'success': () => { },
    'error': (e) => { console.log('THERE WAS AN ERROR!'); console.log(e)}
  })
}
