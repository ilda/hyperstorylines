export default function entityInteractions() {
  var interaction = {
    clickedOnce: false,
    timerClick: undefined
  }
  var _hsl
  interaction.hsl = function (hsl) {
    _hsl = hsl
    return interaction
  }
  interaction.click = function (source, entity, dim, toCopy=undefined) {
    d3.event.stopPropagation();
    if (interaction.clickedOnce) {
      interaction.runDoubleClick(source, entity, dim)
    } else {
      interaction.timerClick = setTimeout(function () {
        interaction.runSimpleClick(source, entity, dim, toCopy)
      }, 230)
      interaction.clickedOnce = true
    }
  }
  interaction.runSimpleClick = function (source, entity, dim, toCopy) {
    click(source, entity, dim, toCopy)
    interaction.clickedOnce = false
  }
  interaction.runDoubleClick = function (source, entity, dim) {
    interaction.clickedOnce = false
    clearTimeout(interaction.timerClick)
    doubleClick(source, entity, dim)
  }
  function click(source, entity, dim, toCopy) {
    _hsl.select(entity)
  }
  function doubleClick(source, entity, dim) {
    if (source == 'barchart') {
      _hsl.filter(entity, false)
    } else {
      _hsl.filter(entity, false)
    }
  }
  interaction.mouseOver = function (text) {
    d3.select('.tooltip')
      .transition()
      .duration(200)
      .style('opacity', 0.9)
    d3.select('.tooltip')
      .html(text)
      .style('left', d3.event.pageX + 'px')
      .style('top', d3.event.pageY - 28 + 'px')
  }
  interaction.mouseOut = function () {
    d3.select('.tooltip')
      .transition()
      .duration(500)
      .style('opacity', 0)
  }
  return interaction
}
