export default function scenesInteractions() {
  var interaction = {
    clickedOnce: false,
    timerClick: undefined
  }
  var _hsl
  interaction.hsl = function (hsl) {
    _hsl = hsl
    return interaction
  }
  interaction.click = function (scene) {
    d3.event.stopPropagation();
    scene.isOpen = !scene.isOpen
    _hsl.expandScene(scene)
  }
  return interaction
}
