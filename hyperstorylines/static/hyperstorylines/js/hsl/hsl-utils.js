export function getTranslateCoordinates(node) {
  const transform = node.getAttribute('transform')
  const indexStartParentesis = transform.indexOf('(')
  const indexComma = transform.indexOf(',')
  const result = {}
  result.x = Number(transform.substring(indexStartParentesis + 1, indexComma))
  result.y = Number(transform.substring(indexComma + 1, transform.length - 1))
  return result
}

export function getLinkGroupId(d, i) {

  var result = 'link-S-'
  var source = d.source
  if (source.scene) {
    result += 'scene-' + source.scene.id + '-'
  }
  result += 'char-' + source.character.id
  result += '-T-'
  var target = d.target
  if (target.scene) {
    result += 'scene-' + target.scene.id + '-'
  }
  result += 'char-' + target.character.id
  return result
}

function escapeRegExp(string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export function replaceAll(str, find, _replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), _replace)
}

export function removeSpecialCharacters(string) {
  // TODO: I bet this is ugly 
  const specialChars = ["'", ".", ",", " ",]
  specialChars.forEach(c => {
    string = replaceAll(string, c, '')
  })
  return string
}


export function nameCutter(name) {
  return name.substring(0, 20)
}

String.prototype.getAllIndices = function (strFind) {
  const regex = new RegExp(strFind, "ig");
  return this.matchAll(regex);
};

export function getScenesURL(selections, datasetName, filteredEntities, termsForEntities, termsForRelationships) {
  var url = '/hyperstorylines/get_scenes/?name=' + datasetName
  for (let d in selections.dim) {
    url += '&' + d + 'Dim=' + selections.dim[d]
  }
  for (let a in selections.aggr) {
    url += '&' + a + 'Aggr=' + selections.aggr[a]
  }
  url += '&xDimOrder=' + selections.order.x

  // FILTERED ENTITIES
  if (filteredEntities.length > 0) {
    url += '&filteredEntities='
    for (let i in filteredEntities) {
      const entityId = filteredEntities[i]
      url += entityId
      if (i < filteredEntities.length - 1) {
        url += '+'
      }
    }
  }

  // SEARCHED TERMS FOR ENTITIES
  if (termsForEntities.length > 0) {
    url += '&termsForEntities='
    for (let i in termsForEntities) {
      const entityTerm = termsForEntities[i]
      url += entityTerm
      if (i < termsForEntities.length - 1) {
        url += '+'
      }
    }
  }

  // SEARCHED TERMS FOR RELATIONSHIPS
  if (termsForRelationships.length > 0) {
    url += '&termsForRelationships='
    for (let i in termsForRelationships) {
      const entityId = termsForRelationships[i]
      url += entityId
      if (i < termsForRelationships.length - 1) {
        url += '+'
      }
    }
  }
  return url
}

export function getNestedScenesURL(scene, selections, datasetName, filteredEntities, termsForEntities, termsForRelationships) {
  var url = '/hyperstorylines/get_nested_scenes/?name=' + datasetName
  for (let d in selections.dim) {
    url += '&' + d + 'Dim=' + selections.dim[d]
  }
  for (let a in selections.aggr) {
    url += '&' + a + 'Aggr=' + selections.aggr[a]
  }
  url += '&scene_edges='
  for (let s in scene.edge.original_edges_ids) {
    const edge_id = scene.edge.original_edges_ids[s]
    url += edge_id
    if (s < scene.edge.original_edges_ids.length - 1) {
      url += '+'
    }
  }
  url += '&ZDimOrder=' + selections.order.z
  // TODO: is there a smarter way to have this info?
  url += '&keyEntityName=' + scene.keyEntity.name

  // FILTERED ENTITIES
  if (filteredEntities.length > 0) {
    url += '&filteredEntities='
    for (let i in filteredEntities) {
      const entityId = filteredEntities[i]
      url += entityId
      if (i < filteredEntities.length - 1) {
        url += '+'
      }
    }
  }

  // SEARCHED TERMS FOR ENTITIES
  if (termsForEntities.length > 0) {
    url += '&termsForEntities='
    for (let i in termsForEntities) {
      const entityTerm = termsForEntities[i]
      url += entityTerm
      if (i < termsForEntities.length - 1) {
        url += '+'
      }
    }
  }

  // SEARCHED TERMS FOR RELATIONSHIPS
  if (termsForRelationships.length > 0) {
    url += '&termsForRelationships='
    for (let i in termsForRelationships) {
      const entityId = termsForRelationships[i]
      url += entityId
      if (i < termsForRelationships.length - 1) {
        url += '+'
      }
    }
  }
  return url
}

export function getBarchartDataURL(datasetName, dim) {
  var url = '/hyperstorylines/get_barchart_data/?name=' + datasetName
  url += '&dim=' + dim
  return url
}

export function getDataURL(name){
  return '/hyperstorylines/get_data/?q=' + name
}

export function getDatasetsAvailableURL(){
  return '/hyperstorylines/datasets-available'
}

export function getSourceURL(sourceName, datasetName){
  return '/hyperstorylines/get_source/?q=' + sourceName + '&i=' + datasetName
}


export function loading() {
  d3.selectAll('.hsl-container').style('opacity', 0.7).style('pointer-events', 'none')
  d3.select('.loading').style('visibility', 'visible')
}

export function ready() {
  d3.selectAll('.hsl-container').style('opacity', 1).style('pointer-events', 'all')
  d3.select('.loading').style('visibility', 'hidden')
}