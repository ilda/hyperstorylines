export default function dimensions() {
  var dimensions = {}

  var _active
  var _hsl
  var _dimsConfig
  var _allDimensions

  dimensions.hsl = function (hsl) {
    _hsl = hsl
    _active = {}
    _active['x'] = undefined
    _active['y'] = undefined
    _active['z'] = undefined
    createContainers()
    return dimensions
  }

  // INITIALIZATION
  function createContainers() {
    // X DIMENSION
    const bgXMain = d3.select('.hsl-xselector').append('div').attr('class', 'btn-group')
    const bgX = bgXMain.append('div').attr('class', 'btn-group')
    bgX
      .append('button')
      .attr('type', 'button')
      .attr('class', 'btn btn-secondary dropdown-toggle')
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .attr('id', 'xDimSelects_label')
    const btXDrop = bgX.append('div').attr('class', 'dropdown-menu').attr('id', 'xDimSelects')
    bgXMain
      .append('button')
      .attr('class', 'btn btn-secondary')
      .attr('id', 'reorderX')
      .attr('value', 'time')
      .html('Order X by abc')

    // Y DIMENSION
    const bgY = d3.select('.hsl-yselector').append('div').attr('class', 'btn-group')
    bgY
      .append('button')
      .attr('type', 'button')
      .attr('class', 'btn btn-secondary dropdown-toggle')
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .attr('id', 'yDimSelects_label')
    const btYDrop = bgY.append('div').attr('class', 'dropdown-menu').attr('id', 'yDimSelects')

    // Z DIMENSION
    const bgZMain = d3.select('.hsl-zselector').append('div').attr('class', 'btn-group')
    const bgZ = bgZMain.append('div').attr('class', 'btn-group')
    bgZ
      .append('button')
      .attr('type', 'button')
      .attr('class', 'btn btn-secondary dropdown-toggle')
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .attr('id', 'zDimSelects_label')
    const btZDrop = bgZ.append('div').attr('class', 'dropdown-menu').attr('id', 'zDimSelects')
    bgZMain
      .append('button')
      .attr('class', 'btn btn-secondary')
      .attr('id', 'reorderZ')
      .attr('value', 'time')
      .html('Order nested entities by abc')
  }

  function addListeners() {
    for (let d in _active) {
      d3.select('#' + d + 'DimSelects').selectAll('.dropdown-item')
        .on('click', function () {
          d3.select('#' + d + 'DimSelects').selectAll('.dropdown-item').classed('active', false)
          d3.select(this).classed('active', true)
          _active[d] = d3.select(this).node().id
          updateAllDimensions()
          _hsl.changeDimension()
        })
    }
    $('#reorderX').on('click', function (event) {
      var button = d3.select(this).node()
      if (button.value == 'time') {
        button.value = 'abc'
        button.innerText = 'Order X by time'
      } else {
        button.value = 'time'
        button.innerText = 'Order X by abc'
      }
      _hsl.changeOrder()
    })
    $('#reorderZ').on('click', function (event) {
      var button = d3.select(this).node()
      if (button.value == 'time') {
        button.value = 'abc'
        button.innerText = 'Order nested entities by time'
      } else {
        button.value = 'time'
        button.innerText = 'Order nested entities by abc'
      }
      _hsl.reorderExpandedScenes()
    })
  }
  dimensions.setConfig = function (dims) {
    _dimsConfig = dims
    _allDimensions = []
    for (let d in dims) {
      _allDimensions.push(d)
    }
    initActive()
    return dimensions
  }

  function initActive() {    
    for (let d in _dimsConfig) {
      const dim = _dimsConfig[d]
      _active[dim.default] = d
      for (let i in dim.available) {
        const a = dim.available[i]
        d3.select('#' + a + 'DimSelects')
          .append('a')
          .attr('class', 'dropdown-item')
          .attr('id', d)
          .html(d)
      }
    }
    addListeners()
    updateAllDimensions()
  }

  function updateAllDimensions() {
    clearAllDimensions()
    d3.select('#xDimSelects').select('#' + _active['y']).classed('disabled', true)
    d3.select('#yDimSelects').select('#' + _active['x']).classed('disabled', true)
    d3.select('#zDimSelects').select('#' + _active['y']).classed('disabled', true)
    d3.select('#zDimSelects').select('#' + _active['x']).classed('disabled', true)
    d3.select('#xDimSelects').select('#' + _active['x']).classed('active', true)
    d3.select('#yDimSelects').select('#' + _active['y']).classed('active', true)
    d3.select('#zDimSelects').select('#' + _active['z']).classed('active', true)
    d3.select('#xDimSelects_label').node().innerHTML = 'Related-by entities (' + _active['x'] + ') →'
    d3.select('#yDimSelects_label').node().innerHTML = '↓ Story entities (' + _active['y'] + ')'
    d3.select('#zDimSelects_label').node().innerHTML = '<text class="inner-button-text"> &nbsp Nested entities (' + _active['z'] + ') </text>'
  }

  function clearAllDimensions() {
    for (var index in _allDimensions) {
      var currentDim = _allDimensions[index]
      d3.select('#xDimSelects').select('#' + currentDim).classed('disabled', false)
      d3.select('#yDimSelects').select('#' + currentDim).classed('disabled', false)
      d3.select('#zDimSelects').select('#' + currentDim).classed('disabled', false)
      d3.select('#xDimSelects').select('#' + currentDim).classed('active', false)
      d3.select('#yDimSelects').select('#' + currentDim).classed('active', false)
      d3.select('#zDimSelects').select('#' + currentDim).classed('active', false)
    }
    if (_active['z'] == _active['x'] || _active['z'] == _active['y']) {
      _active['z'] = ''
      for (var dimIndex in _allDimensions) {
        var dim = _allDimensions[dimIndex]
        if (dim !== _active['x'] && dim !== _active['y']) {
          _active['z'] = dim
          break
        }
      }
    }
  }

  // GETTERS
  dimensions.getAll = function () {
    return _allDimensions
  }

  dimensions.active = function (axis) {
    return _active[axis]
  }
  dimensions.activeAll = function () {
    return _active
  }
  
  return dimensions
}