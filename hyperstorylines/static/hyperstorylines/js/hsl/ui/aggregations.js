export default function aggregations() {
  var aggregations = {}

  var _active
  var _activeType
  var _hsl
  var _AggrsConfig
  var _allAggregations
  var _uiSelectors

  aggregations.init = function (uiSelectors) {
    //_hsl = hsl
    _uiSelectors = uiSelectors
    _activeType = undefined
    _active = {}
    _active['x'] = undefined
    _active['z'] = undefined
    createContainers()
    return aggregations
  }

  // INITIALIZATION
  function createContainers() {
    // X DIMENSION
    const bgXAgg = d3
      .select('.hsl-xselector')
      .select('.btn-group')
      .append('div')
      .attr('class', 'btn-group')
    bgXAgg
      .append('button')
      .attr('type', 'button')
      .attr('class', 'btn btn-secondary dropdown-toggle')
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .attr('id', 'xAggrSelects_label')
      .html('Aggregation')

    const btXAggrDrop = bgXAgg
      .append('div')
      .attr('class', 'dropdown-menu')
      .attr('id', 'xAggrSelects')

    // Z DIMENSION
    const bgZAgg = d3
      .select('.hsl-zselector')
      .select('.btn-group')
      .append('div')
      .attr('class', 'btn-group')
    bgZAgg
      .append('button')
      .attr('type', 'button')
      .attr('class', 'btn btn-secondary dropdown-toggle')
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .attr('id', 'zAggrSelects_label')
      .html('Aggregation')

    const btZAggrDrop = bgZAgg
      .append('div')
      .attr('class', 'dropdown-menu')
      .attr('id', 'zAggrSelects')
  }

  function addListeners() {
    for (let d in _active) {
      d3.select('#' + d + 'AggrSelects')
        .selectAll('.dropdown-item')
        .on('click', function () {
          const activeDim = _uiSelectors.getActiveDimensions()
          d3.select('#' + d + 'AggrSelects')
            .selectAll('.dropdown-item')
            .classed('active', false)
          d3.select(this).classed('active', true)
          _active[d] = d3.select(this).node().id
          d3.select('#' + d + 'AggrSelects_label').node().innerHTML =
            'Aggregation (' + _AggrsConfig[activeDim[d]][_active[d]] + ')'
          _uiSelectors.changeAggregation()
        })
    }
  }

  aggregations.setConfig = function (aggrs) {
    _AggrsConfig = aggrs
    _allAggregations = []
    for (let a in aggrs) {
      _allAggregations.push(a)
    }
    initActive()
  }

  aggregations.updateAggregators = function () {
    initActive()
  }

  function initActive() {
    const activeDim = _uiSelectors.getActiveDimensions()
    // X
    const xType = activeDim['x']
    const btXAggrDrop = d3
      .selectAll('.hsl-xselector')
      .selectAll('#xAggrSelects')
    btXAggrDrop.selectAll('.dropdown-item').remove()
    var first = true
    for (let a in _AggrsConfig[xType]) {
      var cssClass = 'dropdown-item'
      if (first) {
        cssClass += ' active'
        _active['x'] = a
        first = false
      }
      btXAggrDrop
        .append('a')
        .attr('class', cssClass)
        .attr('id', a)
        .html(_AggrsConfig[xType][a])
    }
    d3.select('#xAggrSelects_label').node().innerHTML =
      'Aggregation (' + _AggrsConfig[activeDim['x']][_active['x']] + ')'
    // Z
    const zType = activeDim['z']
    const btZAggrDrop = d3
      .selectAll('.hsl-zselector')
      .selectAll('#zAggrSelects')
    btZAggrDrop.selectAll('.dropdown-item').remove()
    var first = true
    for (let a in _AggrsConfig[zType]) {
      var cssClass = 'dropdown-item'
      if (first) {
        cssClass += ' active'
        _active['z'] = a
        first = false
      }
      btZAggrDrop
        .append('a')
        .attr('class', cssClass)
        .attr('id', a)
        .html(_AggrsConfig[zType][a])
    }
    d3.select('#zAggrSelects_label').node().innerHTML =
      'Aggregation (' + _AggrsConfig[activeDim['z']][_active['z']] + ')'
    addListeners()
  }

  // GETTERS
  aggregations.getAll = function () {
    return _allAggregations
  }

  aggregations.active = function (axis) {
    return _active[axis]
  }
  aggregations.activeAll = function () {
    return _active
  }
  return aggregations
}
