export function helpText() {
  var result = `
  <h2>Associated publication:</h2>
		<ul>
			<li>
				<a href="https://dx.doi.org/10.1177/14738716211045007" target="_blank">DOI</a>
			</li>
			<li>
				<a href="https://hal.inria.fr/hal-03352276/document" target="_blank">PDF @ HAL</a>
			</li>
      <li>
      <a href="https://ilda.saclay.inria.fr/hyperstorylines/" target="_blank">Sup. Material</a>
    </li>
		</ul>

		<h2>Software</h2>
		<ul>
			<li>
				<a href="https://gitlab.inria.fr/ilda/hyperstorylines"> Source code @ gitlab.inria.fr</a>
			</li>
		</ul>

  <h2> Description of the tool</h2>
  
  <ul> 
      <li>
        Each line in the Y dimension (vertical axis) represents the 
        <b class="highlightText">story</b> of an entity that progresses 
        in the X dimension (horizontal axis).
      </li>
      <br>
      <li>
        The lines curve to join together into a <b class="highlightText">
        relationship</b> bar when they are related.
        When clicked, the relationship bar expands to show the 
        <b class="highlightText">nested entities</b> that give details on the relationship. 
        The type of entities inside the relationship bar is determined 
        by the UI selection under ‘inner entities'. 
        By default the nested entities are locations. 
        You can have multiple relationships bars open. 
        You can click anywhere inside an expanded relationship bar 
        to close it. 
        Remember that these relationships are due to news articles, 
        for which sometimes you will see very similar relationships 
        but on different dates. 
      </li>
      <br>
      <li>
        The <b class="highlightText">number of nested entities</b> inside 
        a relationship is given by either the border or a number above it: <br>
        <b>No nested entities</b> (i.e., it does not include entities of 
        the type given by the selector 'inner entities') are indicated 
        by a dashed border. 
        <br>
        <b>Only one nested entity</b> is indicated by a solid border and 
        no number above it.
        <br>
        <b>More than one nested entity</b> is indicated by a solid border 
        and the number of nested entities above its relationship bar 
      </li>
      <br>
      <li> 
        You can change the <b class="highlightText">type of entity</b>
        for the X, Y and nested entities in the selectors in the upper
        part of the interface. 
        Additionally, you can change the order of entities for the X 
        dimension and nested entities inside open relationships. 
      </li>
      <br>
      <li>
        <b class="highlightText"> Single click - Selecting </b>
        <br> 
        You can <b>select</b> an entity, in the X dimension, Y 
        dimension, and nested entities by clicking on its name or 
        any section of its line. The entity and all its associated
        relationships will be highlighted in blue. 
        The entities related to the selected one will be in a darker
        grey, while those that are not related will be slightly faded. 
        The relationships of the related entities that do not involve 
        the selected entity will also be in a darker grey than the rest.
        <br> 
        As you saw, you can select several entities at the same time 
        just by clicking on them. By clicking on a selected entity 
        again you will unselect it.
        <br> 
        When you click on any entity or date, their text will be 
        copied in the clipboard. 
        You can use this feature to copy answers or search for information.`
  /*if (!demo) {
    result += 'Click once on the background to <b>clear</b> all your selections.'
  }*/
  result += `</li>
      <br>
      <li>
        <b class="highlightText"> Double click - Filtering </b>
        <br> 
        You can <b>filter</b> an entity by double clicking on its 
        name or any section of its line. 
        This will change the visualization to only keep visible the 
        entities and relationships that are related to the one you 
        filtered. 
        The filtered entity will have a blue rectangle. 
        When using double click, you can only filter one entity at a time.
        <br> 
        You can also <b>search and filter</b> entities by their 
        name using the Filter bar at the top. 
        If more than one entity includes the input text in their 
        name, then all the entities that match the text will be 
        filtered on the visualization. 
        For questions that require to find a particular entity, you 
        can copy the name in the question and paste it in the search
        input.`
  /*if (demo) {*/
    result += '<br> <br> Double click on the background to <b> clear </b> all your selected and filtered entities.'
  /*} else {
    result += 'Double click on the background to <b> clear</b> your filtered selection.'
  }*/
  result += `</li >
    <br>
    <li>
      Use the <b class="highlightText">scroll bars</b> to navigate 
      vertically and horizontally. 
      Selected and related entities are shown as thin vertical lines 
      in the internal part of the scrollbars.
      Open relationships are shown as small 
      rectangles in the external part of the scrollbars. 
      The color of both the lines and the rectangles indicate if they
      are selected (blue) or related (grey). 
    </li>
    <li>
      You can zoom out with the browser to have a better overview of
      the visualization and then zoom in again to see more details.
    </li>
    <li>
      If a question requires an answer with several entities or 
      dates, separate them by commas in the input field.
    </li>
    </ul>`
  return result
}