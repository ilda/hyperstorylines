import dimensions from './dimensions.js'
import aggregations from './aggregations.js'
import order from './order.js'
import datasets from './dataset.js'
import { helpText } from './help.js'

export default function uiSelectors() {
  var _dimensions
  var _aggregations
  var _order
  var _hsl
  var uiSelectors = {}

  // INIT
  uiSelectors.hsl = function (hsl) {
    _hsl = hsl
    _dimensions = dimensions().hsl(hsl)
    _aggregations = aggregations().init(uiSelectors)
    _order = order().init()
    datasets().hsl(hsl)
    return uiSelectors
  }

  uiSelectors.dimensions = function (values) {
    _dimensions.setConfig(values)
    return uiSelectors
  }

  uiSelectors.aggregations = function (values) {
    _aggregations.setConfig(values)
    return uiSelectors
  }

  uiSelectors.updateAggregators = function(){
    _aggregations.updateAggregators()
    return uiSelectors
  }

  // GETTERS
  uiSelectors.getAllDimensions = function () {
    return _dimensions.getAll()
  }

  uiSelectors.getActiveDimensions = function () {
    return _dimensions.activeAll()
  }

  uiSelectors.getActiveAggregations = function () {
    return _aggregations.activeAll()
  }

  uiSelectors.getAllSelections = function () {
    const r = {}
    r['dim'] = _dimensions.activeAll()
    r['aggr'] = _aggregations.activeAll()
    r['order'] = _order.activeAll()
    return r
  }

  uiSelectors.changeAggregation = function () {
    _hsl.changeAggregation()
  }

  uiSelectors.addHelp = function () {
    d3.select('body')
      .selectAll('#helpContent')
      .data([1])
      .enter()
      .append('section')
      .attr('id', 'helpContent')
      .html(helpText())
    $('#helpContent').popup({
      pagecontainer: '#hsl-main',
      escape: false,
    })
    return uiSelectors
  }

  uiSelectors.reset = function () {
    d3.select('.datasets-panel').remove()
  }
  return uiSelectors
}
