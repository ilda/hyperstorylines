export default function datasets() {

  var datasets = {}
  var _hsl

  datasets.hsl = function (hsl) {
    _hsl = hsl
    createContainers()
    loadDatasetList()
  }

  function createContainers() {
    const container = d3
      .select('.toolbar')
      .append('div')
      .attr('class', 'datasets-panel')
      .append('div').attr('class', 'btn-group')


    container
      .append('button')
      .attr('type', 'button')
      .attr('class', 'btn btn-secondary dropdown-toggle')
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .attr('id', 'dataset-selector-label')

    container
      .append('div')
      .attr('class', 'dropdown-menu')
      .attr('id', 'dataset-selector')
  }

  function loadDatasetList() {
    d3.json('/hyperstorylines/datasets-available').then(function (data) {
      d3.select('#dataset-selector')
        .selectAll('a')
        .data(data.datasets)
        .enter()
        .append('a')
        .attr('class', 'dropdown-item')
        .attr('id', d => d)
        .html(d => d)
        .on('click', d => _hsl.changeDataset(d))
    })
  }

  return datasets
}