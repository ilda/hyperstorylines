export default function order() {
    var order = {}

    order.init = function () {
        return order
    }

    order.getX = function () {
        return d3.select('#reorderX').node().value
    }

    order.getZ = function () {
        return d3.select('#reorderZ').node().value
    }

    order.activeAll = function () {
        const result = {}
        result['x'] = order.getX()
        result['z'] = order.getZ()
        return result
    }

    return order
}