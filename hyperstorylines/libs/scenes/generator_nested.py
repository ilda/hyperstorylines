
import copy
import locale
import functools
import hyperstorylines.libs.hypergraph.transformer as hypergraph_transformer
import json
from django.templatetags.static import static
from hyperstorylines.libs.index import Index
from hyperstorylines.libs.scenes.ids_cleaner import IdsCleaner
from hyperstorylines.libs.scenes.generator_abstract import ScenesGeneratorAbstract


class NestedScenesGenerator(ScenesGeneratorAbstract):

    def get_scenes(self, dataset_name, dim_x, dim_y, dim_z,
                   filtered_entities, terms_for_entities, terms_for_relationships,
                   aggr_x, aggr_z, order_z, scene_edges, scene_keynode_name):
        # 1) LOAD THE DATA
        file_data = open(static('hyperstorylines/datasets/storylines/' + dataset_name))
        data = json.load(file_data)
        # TODO: this is kinda ugly
        base_node = data['nodes'][0]

        # 2) FILTER THE DATA
        index = Index(dataset_name[:-5])
        sources_ids = index.get_Sources_ids(terms_for_relationships)
        self._filter_data(data, dim_x, dim_y, dim_z, filtered_entities,
                          terms_for_entities, terms_for_relationships, sources_ids)

        # 3) COMPUTE AGGREGATION
        self._compute_hypergraph(data, scene_edges, dim_y, dim_z, aggr_z)

        # 4) ADD TERM NODES IF NECESSARY
        self._process_searched_terms_for_relationships(
            terms_for_relationships, dim_y, base_node, sources_ids, {})

        # 5) RETURN COMPUTED NESTED SCENES
        ignored_levels = hypergraph_transformer.get_ignored_levels(None, dim_x)
        if aggr_x in ignored_levels:
            scene_keynode_name = None
        return self._compute_scenes(dim_x, dim_y, dim_z, aggr_x,
                                    order_z, scene_keynode_name)

    def _compute_hypergraph(self, data, scene_edges, dim_y, dim_z, aggr_z):
        # 1) CHECK WHICH EDGES TO SHOW (HAS NODES IN DIM_Y)
        edges_ori_dict = {}
        scene_nodes = []
        hyp_nodes = []
        hyp_edges = []
        for edge in data['edges']:
            if edge['id'] in scene_edges and len(edge['node_types_ids'][dim_y]) > 0:
                edges_ori_dict[edge['id']] = edge
                scene_nodes = scene_nodes + edge['nodes_ids']
                hyp_edges.append(edge)

        # 2) ADD NECESSRY NODES
        scene_nodes = list(set(scene_nodes))
        nodes_ori_dict = {}
        for node in data['nodes']:
            if node['id'] in scene_nodes:
                nodes_ori_dict[node['id']] = node
                hyp_nodes.append(node)

        # 3) ASSIGN NEW NODES AND EDGES + RECOMPUTE RELATIONSHIPS
        data['edges'] = hyp_edges
        data['nodes'] = hyp_nodes
        self._compute_relation_matrix(data)

        ori_hyp = {}
        ori_hyp['edges'] = edges_ori_dict
        ori_hyp['nodes'] = nodes_ori_dict
        ori_hyp['relations_matrix'] = data['relations_matrix']
        self.original_hyp = ori_hyp

        # 4) COMPUTE AGGREGATED HYPERGRAPH
        # INNER AGGREGATION IS JUST REPLACEMENT OF CHILD NODES
        # FOR PARENT, DO NOT AGG EDGES
        self.aggr_hyp = self._get_aggregated(
            data, nodes_ori_dict, dim_z, aggr_z)

    def _compute_scenes(self, dim_x, dim_y, dim_z, aggr_x, order_z, scene_keynode_name):
        extra_node_z_id = 'no_' + dim_z
        extra_node_z = copy.deepcopy(self.aggr_hyp['nodes'][0])
        extra_node_z['isEmptyEntityType'] = True
        extra_node_z['name'] = extra_node_z_id
        extra_node_z['id'] = extra_node_z_id
        extra_node_z['type'] = dim_z
        for t in extra_node_z['related_nodes']:
            extra_node_z['related_nodes'][t] = []
        extra_node_z['related_edges'] = []
        self.aggr_hyp['nodes'].append(extra_node_z)

        nodes = self.aggr_hyp['nodes']
        edges = self.aggr_hyp['edges']
        # AUXILIARY DICTIONARY
        nodes_dict = {}
        related_nodes_y = []
        for node in nodes:
            related = node['related_nodes'][dim_y]
            related_nodes_y += related
            nodes_dict[node['id']] = node

        # CREATE ONE OR MORE SCENES PER EDGE
        scenes = []
        for edge in edges:
            edge_id = edge['id']
            # IGNORE ANY EDGE WITHOUT ENTITIES IN THE Y DIM
            y_dim_nodes = edge['node_types_ids'][dim_y]
            if len(y_dim_nodes) == 0:
                continue
            chtrs = []
            for node_id in y_dim_nodes:
                # if node_id in related_nodes_y:
                chtrs.append(node_id)
            chtrs = sorted(chtrs)

            # CREATE ONE SCENE PER X AND Z COMBINATION
            x_dim_ids = edge['node_types_ids'][dim_x]
            z_dim_ids = edge['node_types_ids'][dim_z]
            if len(z_dim_ids) == 0:
                z_dim_ids = [extra_node_z_id]
                for t in nodes_dict[extra_node_z_id]['related_nodes']:
                    nodes_dict[extra_node_z_id]['related_nodes'][t] = nodes_dict[extra_node_z_id]['related_nodes'][t] + \
                        edge['node_types_ids'][t]
                    nodes_dict[extra_node_z_id]['related_nodes'][t] = list(
                        set(nodes_dict[extra_node_z_id]['related_nodes'][t]))
            for node_x_id in x_dim_ids:
                if node_x_id not in nodes_dict:
                    continue
                node_x = nodes_dict[node_x_id]
                if scene_keynode_name != None:
                    aggr_value = hypergraph_transformer.aggregate_element(
                        dim_x, node_x, aggr_x)
                    if aggr_value != scene_keynode_name:
                        continue
                for node_z_id in z_dim_ids:
                    # TODO: NOT SURE HOW I COULD REACH THIS POINT
                    if node_z_id not in nodes_dict:
                        continue
                    node_z = nodes_dict[node_z_id]
                    date = self._get_date_for_scene(edge, nodes_dict)
                    new_scene = self._create_scene(
                        date, edge, chtrs, node_x, node_z)
                    new_scene['edge'] = edge
                    scenes.append(new_scene)
        scenes = self._sort_scenes(scenes, order_z)
        return (scenes, nodes_dict)

    def _get_aggregated(self, data, nodes_dict, dim_z, aggr_z):
        new_nodes = {}
        nodes_ids_to_replace = {}

        # I create one new node for each parent
        for node in data['nodes']:
            if node['type'] == dim_z:
                node_id = node['id']
                original_node = nodes_dict[node_id]
                new_node_value = hypergraph_transformer.aggregate_element(
                    dim_z, original_node, aggr_z)
                if new_node_value == original_node['name']:
                    continue
                if new_node_value not in new_nodes:
                    new_node = copy.deepcopy(original_node)
                    new_node['name'] = new_node_value
                    new_node['id'] = 'node_parent' + node_id
                    # TODO: not sure why I need to do this
                    new_node['related_edges'] = []
                    for key in new_node['related_nodes']:
                        new_node['related_nodes'][key] = []
                    new_node['aggregated_nodes_ids'] = []
                    new_nodes[new_node_value] = new_node
                else:
                    new_node = new_nodes[new_node_value]
                nodes_ids_to_replace[node_id] = new_node['id']
                new_node['related_edges'] = original_node['related_edges'] + \
                    new_node['related_edges']
                new_node['related_edges'] = list(
                    set(new_node['related_edges']))
                for key in new_node['related_nodes']:
                    new_node['related_nodes'][key] = original_node['related_nodes'][key] + \
                        new_node['related_nodes'][key]
                    new_node['related_nodes'][key] = list(
                        set(new_node['related_nodes'][key]))

        for new_node in new_nodes.values():
            data['nodes'].append(new_node)

        # Replace nodes in the edges:
        for e in data['edges']:
            for old_node_id, new_node_id in nodes_ids_to_replace.items():
                if old_node_id in e['node_types_ids'][dim_z]:
                    e['node_types_ids'][dim_z].remove(old_node_id)
                    e['node_types_ids'][dim_z].append(new_node_id)
                if old_node_id in e['nodes_ids']:
                    e['nodes_ids'].remove(old_node_id)
                    e['nodes_ids'].append(new_node_id)
            # Remove any repetitions
            e['node_types_ids'][dim_z] = list(set(e['node_types_ids'][dim_z]))
            e['nodes_ids'] = list(set(e['nodes_ids']))

        # Replace in the relationship matrix
        self._compute_relation_matrix(data)
        return data

    def _create_scene(self, date, edge, chtrs, keynode_dim_x, keynode_dim_z):
        new_scene = {}
        new_scene['charactersIds'] = chtrs
        # TODO: WHY DO I NEED THE KEY AND THE NODE? REDUNDANT INFO
        new_scene['keyZ'] = keynode_dim_z['name']
        new_scene['keyEntityZ'] = keynode_dim_z
        new_scene['keyX'] = keynode_dim_x['name']
        new_scene['keyEntityX'] = keynode_dim_x
        new_scene['date'] = date
        new_scene['first_appearance'] = keynode_dim_x['first_appearance']
        new_scene['keyEntity'] = keynode_dim_z
        new_scene['id'] = edge['id'] + '_' + \
            keynode_dim_x['id'] + '_' + keynode_dim_z['id']
        new_scene['edgeId'] = edge['id']
        new_scene['isOpen'] = False
        new_scene['allRelatedEntities'] = edge['nodes_ids']
        new_scene['isFake'] = False
        new_scene['sources'] = edge['sources']
        # TODO: ADD INFO OF FAKE CHARACTERS
        # chtrs.forEach(n => {
        # if (n != undefined) {
        # if (n.isFake) {
        # newItem.isFake = true
        # }
        # }
        # })
        return new_scene

    # TODO: CHECK IF THE ORDER Z ACTUALLY WORKS
    def _sort_scenes(self, scenes, order):
        def compare_time(a, b):
            if 'No ' in a['keyZ']:
                return -1
            if 'No ' in b['keyZ']:
                return 1
            # KEYZ IS THE FIRST ORDER CRITERIA, THEN order AND THEN KEYX
            if a['KeyZ'] == b['KeyZ']:
                if a['date'] == b['date']:
                    return locale.strcoll(a['keyX'], b['keyX'])
                else:
                    return locale.strcoll(a['date'], b['date'])
            else:
                return locale.strcoll(a['KeyZ'], b['KeyZ'])

        def compare(a, b):
            if 'No ' in a['keyZ']:
                return -1
            if 'No ' in b['keyZ']:
                return 1
            if a['keyZ'] == b['keyZ']:
                return locale.strcoll(a['keyX'], b['keyX'])
            else:
                return locale.strcoll(a['keyZ'], b['keyZ'])
        keynode_type = scenes[0]['keyEntity']['type']
        if order == 'time' and keynode_type != 'time':
            return sorted(scenes, key=functools.cmp_to_key(compare_time))
        else:
            return sorted(scenes, key=functools.cmp_to_key(compare))
