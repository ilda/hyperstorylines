import json
class IdsCleaner:

    @staticmethod
    def clean(scenes, nodes_dict):
        new_nodes_dict = json.dumps(nodes_dict)
        new_nodes_dict = IdsCleaner.remove_special_characters(new_nodes_dict)
        new_nodes_dict = json.loads(new_nodes_dict)

        new_scenes = json.dumps(scenes)
        new_scenes = IdsCleaner.remove_special_characters(new_scenes)
        new_scenes = json.loads(new_scenes)

        return (new_scenes, new_nodes_dict)

    @staticmethod
    def remove_special_characters(string):
        special_chars = ["'", ".", ",", " "]
        for c in special_chars:
            string = string.replace(c, '')
        return string