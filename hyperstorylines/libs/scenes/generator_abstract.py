from abc import abstractmethod
from hyperstorylines.libs.index import Index
import copy
from hyperstorylines.libs.scenes.ids_cleaner import IdsCleaner


class ScenesGeneratorAbstract:

    # ABSTRACT METHODS NOT USED BUT ADDED AS REFERENCE
    @abstractmethod
    def get_scenes(self, dataset_name, dim_x, dim_y, dim_z,
                   filtered_entities, terms_for_entities, terms_for_relationships,
                   **kwargs):
        pass
    # 5) RETURN COMPUTED NESTED SCENES
    @abstractmethod
    def _compute_scenes(self, dim_x, dim_y, dim_z, order_x, aggr_x):
        pass

    @abstractmethod
    def _create_scene(self, chtrs, node, date, edge, **kwargs):
        pass

    @abstractmethod
    def sort_scenes(self, scenes, order):
        pass

    # 3) COMPUTE AGGREGATION
    @abstractmethod
    def _compute_hypergraph(self, **kwargs):
        pass

    # 2) FILTER THE DATA
    def _filter_data(self, data, dim_x, dim_y, dim_z, filtered_entities, terms_for_entities, terms_for_relationships, sources_ids):
        # IF THERE IS NOTHING TO FILTER THEN JUST RETURN
        if len(filtered_entities) == 0 and len(terms_for_entities) == 0 and len(terms_for_relationships) == 0:
            return  # data

        # FIRST FILTER ENTITIES
        nodes_ids_to_consider = filtered_entities
        new_nodes = []
        new_edges = []

        for node in data['nodes']:
            for term in terms_for_entities:
                if term in node['name'].lower():
                    filtered_entities.append(node['id'])

        filtered_entities = list(set(filtered_entities))

        # GET ALL THE RELATED SOURCES TO THE FILTERED TERMS
        all_sources = []
        for sources_ids in sources_ids.values():
            all_sources = all_sources + sources_ids
        all_sources = list(set(all_sources))

        other_nodes = []

        # GET NEW LIST OF EDGES
        for edge in data['edges']:
            add_edge = False
            for node_id in edge['nodes_ids']:
                # IF THE EDGE LINKS THE RELATED NODES
                if node_id in filtered_entities:
                    add_edge = True
                    break
            # OR IF ONE OF ITS SOURCES CONTAINS A TERM
            for source in edge['sources']:
                if source in all_sources:
                    add_edge = True
                    break
            if add_edge:
                new_edges.append(edge)
                other_nodes = other_nodes + edge['node_types_ids'][dim_x] + edge['node_types_ids'][dim_y] + edge['node_types_ids'][dim_z]
        nodes_ids_to_consider = filtered_entities + list(set(other_nodes))

        for node in data['nodes']:
            if node['id'] in nodes_ids_to_consider:
                new_nodes.append(node)

        # THIRD: APPLY THE FILTER BY REPLACING THE LIST OF NODES AND EDGES
        data['nodes'] = new_nodes
        data['edges'] = new_edges
        data['relations_matrix'] = self._compute_relation_matrix(data)

    def _compute_relation_matrix(self, data):
        relations_matrix = {}
        for dim1 in data['dimensions']:
            relations_matrix[dim1] = {}
            for dim2 in data['dimensions']:
                relations_matrix[dim1][dim2] = []
        for edge in data['edges']:
            edge_id = edge['id']
            for dim1 in edge['node_types_ids']:
                for dim2 in edge['node_types_ids']:
                    if len(edge['node_types_ids'][dim1]) > 0 and len(edge['node_types_ids'][dim2]) > 0:
                        relations_matrix[dim1][dim2].append(edge_id)
                        relations_matrix[dim2][dim1].append(edge_id)
        for dim1 in data['dimensions']:
            for dim2 in data['dimensions']:
                relations_matrix[dim1][dim2] = list(
                    set(relations_matrix[dim1][dim2]))
        return relations_matrix

    # 4) ADD TERM NODES IF NECESSARY
    def _process_searched_terms_for_relationships(self, terms_for_relationships, dim_y, base_node, sources_ids, sources_edges):
        new_nodes = []
        # CREATE A NEW NODE FOR EACH TERM
        terms_nodes = {}
        for term in terms_for_relationships:
            new_node = copy.deepcopy(base_node)
            new_node['type'] = dim_y
            new_node['name'] = term
            new_node['id'] = 'term_node_' + \
                IdsCleaner.remove_special_characters(term)
            new_node['first_appearance'] = '3000-01-01'
            new_node['related_edges'] = []
            new_node['isTerm'] = True
            for t in new_node['related_nodes']:
                new_node['related_nodes'][t] = []
            terms_nodes[term] = new_node
            new_nodes.append(new_node)
            new_node['aggregated_nodes_ids'] = []
            new_node['all_related_edges'] = []

        all_sources = []
        source_terms = {}
        for term, sources_ids in sources_ids.items():
            term_node = terms_nodes[term]
            for s_id in sources_ids:
                # COLLECT ALL FOUND EDGES
                if s_id in sources_edges:
                    edge_id = sources_edges[s_id]
                    term_node['all_related_edges'].append(edge_id)

                # JUST COLLECT ALL SOURCES
                source_terms[s_id] = term
                all_sources.append(s_id)

        all_sources = list(set(all_sources))

        # GET FIRST APPAERANCE DATES (PROBABLY UNNECESARY)
        dates = {}
        for node in self.aggr_hyp['nodes']: 
            if node['type'] == 'time':
                dates[node['id']] = node ['name']
        # GET THE RELEVANT EDGES
        term_nodes_ids = {}
        nodes_ids_terms = {}
        for e in self.aggr_hyp['edges']:
            for source in e['sources']:
                if source in all_sources:
                    term_node = terms_nodes[source_terms[source]]
                    e['nodes_ids'].append(term_node['id'])
                    e['node_types_ids'][dim_y].append(term_node['id'])
                    for d in term_node['related_nodes']:
                        term_node['related_nodes'][d] = term_node['related_nodes'][d] + \
                            e['node_types_ids'][d]
                        term_node['related_nodes'][d] = list(
                            set(term_node['related_nodes'][d]))
                    term_node['related_edges'].append(e['id'])
                    term_nodes_ids[term_node['id']] = e['nodes_ids']
                    for n_id in e['nodes_ids']:
                        if n_id not in nodes_ids_terms:
                            nodes_ids_terms[n_id] = []
                        nodes_ids_terms[n_id].append(term_node['id'])
                    # I ASSUME TIME IS ALWAYS A DIMENSION
                    for n_id in e['node_types_ids']['time']:
                        if n_id in dates and dates[n_id] < term_node['first_appearance']:
                            term_node['first_appearance'] = dates[n_id]
                    break

        for node in new_nodes:
            self.aggr_hyp['nodes'].append(node)

        for node in self.aggr_hyp['nodes']:
            node_id = node['id']
            if node_id in nodes_ids_terms:
                term_id = nodes_ids_terms[n_id]
                node['related_nodes'][dim_y] = node['related_nodes'][dim_y] + term_id
            node['related_nodes'][dim_y] = list(
                set(node['related_nodes'][dim_y]))

    def _compute_sources_edges(self, data):
        r = {}
        for edge in data['edges']:
            for s in edge['sources']:
                r[s] = edge['id']
        return r

    def _get_date_for_scene(self, edge, nodes_dict):
        # Assuming each edge has only one date, this might not be generalizable
        # TODO: MODIFY WHEN INCLUDING THE FILTERED WORDS
        # if edge['isFake']:
        # if False:
        #    return edge['id']
        # else:
        possible_dates = []
        for node_id in edge['node_types_ids']['time']:
            if node_id in nodes_dict:
                possible_dates.append(nodes_dict[node_id]['name'])
        possible_dates.sort()
        return possible_dates[0]


