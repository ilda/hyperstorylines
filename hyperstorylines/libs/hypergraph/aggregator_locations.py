from .aggregator import HypergraphAggregator
from hyperstorylines.models import City


class HypergraphLocationsAggregator(HypergraphAggregator):

    def __init__(self, hyp):
        levels = {'aggr_article': 'Article',
                  'aggr_city': 'City',
                  'aggr_region': 'Region',
                  'aggr_country': 'Country'}
        entity_type = 'locations'
        super().__init__(hyp, levels, entity_type)

    def ignored_levels(self):
        return ['aggr_article', 'aggr_city']

    def aggregate_element(self, elto, aggr_level):
        name = elto['name']
        if aggr_level == 'aggr_city' or aggr_level == 'aggr_article':
            return name
        else:
            records = City.objects.filter(name=name)
            if not records:
                return 'NOT AGGR ' + name
            else:
                # I assume cityies are unique in the DB
                a_city = records[0]
                if aggr_level == 'aggr_region':
                    return a_city.region
                elif aggr_level == 'aggr_country':
                    return a_city.country
                else:
                    return 'NOT LEVEL ' + name