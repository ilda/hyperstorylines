from .aggregator_time import HypergraphTimeAggregator
from .aggregator_locations import HypergraphLocationsAggregator
from .aggregator_default import HypergraphDefaultAggregator


def get_aggregation(hypergraph, entity_type, aggr_level):
    if entity_type == 'time':
        return HypergraphTimeAggregator(hypergraph).apply(aggr_level)
    elif entity_type == 'locations':
        return HypergraphLocationsAggregator(hypergraph).apply(aggr_level)
    else:
        return HypergraphDefaultAggregator(hypergraph, entity_type).apply(aggr_level)

def get_ignored_levels(hypergraph, entity_type):
    if entity_type == 'time':
        return HypergraphTimeAggregator(hypergraph).ignored_levels()
    elif entity_type == 'locations':
        return HypergraphLocationsAggregator(hypergraph).ignored_levels()
    else:
        return HypergraphDefaultAggregator(hypergraph, entity_type).ignored_levels()

def aggregate_element(entity_type, elto, aggr_level):
    if entity_type == 'time':
        return HypergraphTimeAggregator(None).aggregate_element(elto, aggr_level)
    elif entity_type == 'locations':
        return HypergraphLocationsAggregator(None).aggregate_element(elto, aggr_level)
    else:
        return HypergraphDefaultAggregator(None, entity_type).aggregate_element(elto, aggr_level)

