
from django.core.management.base import BaseCommand
from django.core.management import call_command

def populate_db():
    call_command('makemigrations')
    call_command('migrate')
    call_command('create_locations_hierarchy')

class Command(BaseCommand):
    def handle(self, **options):
        populate_db()