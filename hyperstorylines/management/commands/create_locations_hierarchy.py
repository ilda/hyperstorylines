
from os import name
from django.core.management.base import BaseCommand
from datetime import datetime
from hyperstorylines.models import City
import pandas as pd
from urllib.request import urlopen
from io import BytesIO, StringIO
from zipfile import ZipFile


def create_hierarchy():

    # LOAD CITIES
    url_cities = 'http://download.geonames.org/export/dump/cities15000.zip'
    resp = urlopen(url_cities)
    zipfile = ZipFile(BytesIO(resp.read()))
    fname = zipfile.namelist()[0]
    fname = 'cities15000.txt'
    cities = pd.read_csv(zipfile.open(fname), delimiter="	", header=None)
    columns = ['geonameid', 'name', 'asciiname', 'alternatenames', 'latitude', 'longitude', 'feature_class', 'feature_code', 'country_code', 'cc2',
               'admin1_code', 'admin2_code', 'admin3_code', 'admin4_code', 'population', 'elevation', 'dem', 'timezone', 'modification_date']
    cities.columns = columns

    # LOAD REGIONS
    url_regions = 'http://download.geonames.org/export/dump/admin1CodesASCII.txt'
    with urlopen(url_regions) as response:
        regions_data_raw = response.read()
    regions_data = BytesIO(regions_data_raw)
    columns = ['code', 'name', 'name_ascii', 'geonameid']
    regions = pd.read_csv(regions_data, delimiter="	", header=None)
    regions.columns = columns

    # LOAD COUNTRIES
    url_countries = 'http://download.geonames.org/export/dump/countryInfo.txt'
    response = urlopen(url_countries)
    countries_data = ''
    for l in response.readlines():
        s = str(l, 'utf-8')
        if s[0] != '#':
            countries_data += s + '\n'
    countries = pd.read_csv(StringIO(countries_data),
                            delimiter="	", header=None)
    columns = ['ISO', 'ISO3', 'ISO-Numeric', 'fips', 'Country', 'Capital', 'Area(in sq km)', 'Population',
               'Continent', 'tld', 'CurrencyCode', 'CurrencyName', 'Phone', 'Postal Code Format', 'Postal Code Regex',
               'Languages', 'geonameid', 'neighbours', 'EquivalentFipsCode']
    countries.columns = columns

    # SOME USEFUL FUNCTIONS
    def search_city_record(name):
        return cities[cities.name.str.lower() == name.lower()]

    def search_region_record(name):
        return regions[regions.code == name]

    def search_country_record(name):
        return countries[countries.ISO == name]

    def search_hierarchy(name):
        tmp = {}

        # Select all possible records by name
        cities = search_city_record(name)
        if cities.empty:
            return tmp
        tmp['city'] = name

        # Select the one with the highest population
        cities = cities.loc[cities['population'] == cities['population'].max()]
        city = cities.iloc[0]

        # get admin 1 code
        admin1_code = str(city['country_code'])+'.'+str(city['admin1_code'])
        region = search_region_record(admin1_code)
        if region.empty:
            return None
        region = region.iloc[0]
        tmp['region'] = region['name']

        # get country code
        country_code = city['country_code']
        country = search_country_record(country_code)
        if country.empty:
            return None
        country = country.iloc[0]
        tmp['country'] = country['Country']

        return tmp

    for index, row in cities.iterrows():
        city_name = row['name']
        hierarchy = search_hierarchy(city_name)
        if hierarchy is None:
            continue
        new_city = City(
            name=city_name, region=hierarchy['region'], country=hierarchy['country'])
        new_city.save()


class Command(BaseCommand):
    def handle(self, **options):
        create_hierarchy()
