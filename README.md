# HyperStorylines

![](hsl.mp4)

## Associated publication

- [DOI](https://dx.doi.org/10.1177/14738716211045007)
- [PDF @ HAL](https://hal.inria.fr/hal-03352276/document)
- [Sup. material](https://ilda.saclay.inria.fr/hyperstorylines/)

## Demo

A running version of the tool can be found [here](https://hyperstorylines.lisn.upsaclay.fr/).

## Installing  

Below are the instructions to install the tool using Django in a virtual enviroment. Check out our [dockerise branch](https://gitlab.inria.fr/ilda/hyperstorylines/-/tree/dockerise) if you want to install it with Docker.

### Requirements

- Python 3
- pipenv (https://pipenv.pypa.io/en/latest/)
- Elasticsearch for the text search (not necessary to start the visualization itself). Minimun version 7.11.1


### 1.- Create the virtual environment

**To install**:

`pipenv install`

**To enter in the virtual environment**

`pipenv shell`

### 2.- Populate the database with the necessary info to aggregate data

To collect the data for the locations aggregation, execute:

`python manage.py populate_db`

The method for getting the aggregation of geographical locations is a simple heuristic that relies on full match on a city name and returns the one with the highest population. Toponym resolution is a complex task that needs to consider the context of the name of the entity. We do not consider the context of the other locations mentioned in the hypergraph nor the text were the location entity was extracted. 

The data used for this is collected from geonames.org: 

http://download.geonames.org/export/dump/

By default, the tool collects the cities names from the file `cities15000.zip` that contains the name of cities with a population > 15000 or capitals. It also uses the files `admin1CodesASCII.txt` to get the regions and `countryInfo.txt` to get the countries. 

The command `/hyperstorylines/management/commands/create_locations_hierarchy.py` contains the functions to create the relations between cities, regions and countries. It is stored in the database `db.sqlite3` using the model defined in `/hyperstorylines/models.py`. The file `/hyperstorylines/libs/hypergraph/aggregator_locations.py` then queries the database to aggregate each individual location entity. The command defined in `/hyperstorylines/management/commands/create_locations_hierarchy.py` is later called in `/hyperstorylines/management/commands/populate_db.py` in addition to other commands to create the `db.sqlite3` file and generate the tables defined in `/hyperstorylines/models.py`.

You can follow this example to create your own customized aggregation functions according to your needs.

### 4.- Index the datasets in Elasticsearch

If you are going to use text search, execute the following lines to index the available datasets after installing Elasticsearch

`python manage.py index_default_datasets`

### 5.- Collect static files (including datasets)

`python manage.py collectstatic`

### 6.- Run the server

`python manage.py runserver`

## Create new datasets

By default, Nested Storylines comes with 3 datasets:

- A subset of the DBLP database with articles from IEEEVIS, EUROVIS and CHI.
- An anonymized news articles dataset, based on the work with data journalists described in the paper
- A co-authorship graph of ILDA, AVIZ and people from other teams. This dataset doesn't have text to index so text search queries by the name of the entities.

To create a new dataset, check out the dataset_generation folder.
It contains necessary libraries to create a hypergraph (`lib/hypergraph.py`) that can be
exported to Nested Storylines to read it (`hypergraph_instance.export_to_json(filename)`).
It also contains some compatibility methods to export them in the PAOH vis format.

For the DBLP sample dataset and the news articles dataset, there is a folder in the
`dataset_generation` folder with example scripts on how to create them.




