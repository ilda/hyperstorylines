from pathlib import Path
import json

txt_folder = Path('./data/processed/').rglob('*.json')
files = [x for x in txt_folder]

for f in files:
    with open(f, encoding='utf-8') as F:
        dblp_e = json.loads(F.read())
    vis_confs = ['IEEE VIS (Short Papers)', 'SciVis', 'InfoVis', 'Information Visualization', '3DVis@IEEE VIS', 'VIS', 'Scientific Visualization', 'IEEE Visualization' ]
    if 'booktitle' in dblp_e and dblp_e['booktitle'] and dblp_e['booktitle']:
        if dblp_e['booktitle'] in vis_confs:
          with open('data/vis/' + f.name, 'w') as fp:
            json.dump(dblp_e, fp)
    del dblp_e
