from pathlib import Path
import json

txt_folder = Path('./data/processed/').rglob('*.json')
files = [x for x in txt_folder]

booktitles = set()
for f in files:
    with open(f, encoding='utf-8') as F:
        dblp_e = json.loads(F.read())
    if 'booktitle' in dblp_e and dblp_e['booktitle']:
        booktitles.add(dblp_e['booktitle'])
    del dblp_e

with open('./data/venues.txt', 'w') as f:
  for b in list(booktitles):
    f.write(b + '\n')