# -*- coding: utf-8 -*-
from lxml import etree
import os
import sys
from io import TextIOWrapper
from nltk.tokenize import RegexpTokenizer
import json
import datetime
import uuid

# INSPIRED BY::
# https://gist.github.com/tomonari-masada/1568b8affc124490df85c35c5f461574

with open('tags.txt') as f:
    collaborations = f.read().splitlines()

def fast_iter(context, func):
    print('Starting iteration')
    tmp_elto = None

    #read chunk line by line
    #we focus author and title
    for event, elem in context:
        if elem.tag == 'author':
            if elem.text and tmp_elto:
                tmp_elto['authors'].append(elem.text)

        if tmp_elto and elem.tag not in tmp_elto and elem.tag not in collaborations:
            if elem.text:
                tmp_elto[elem.tag] = elem.text

        if elem.tag in collaborations:
            # We only store if is a conference or journal paper for simplicity:
            if tmp_elto is not None and (tmp_elto['type'] == 'inproceedings'or tmp_elto['type'] == 'article'):
                func(tmp_elto)
            del tmp_elto
            tmp_elto = create_dblp_element()
            tmp_elto['type'] = elem.tag
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context

def create_dblp_element():
    elto = {}
    elto['authors'] = []
    return elto

def dump_element(data):
    new_id = uuid.uuid4()
    with open('processed/' + str(new_id)[:6] + '.json', 'w') as fp:
        json.dump(data, fp)

if __name__ == "__main__":
    start = datetime.datetime.now()
    print('Start: ' + str(start))
    context = etree.iterparse('dblp-2021-02-01.xml', load_dtd=True, html=True)
    fast_iter(context, dump_element)
    end = datetime.datetime.now()
    print('End: ' + str(end))
    print(end - start)
