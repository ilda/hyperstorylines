################################################################################
# 1.- Dimensions
# Create dimensions for the header of the file
# key: name of the dimension
# available: in which dimension will be possible to accesss it
# default: when loading the vis for the first time, in which dimension will it appear

conf_name = 'vis'

dataset_name = 'dblp_' + conf_name

dims = {}
dims['time'] = {}
dims['time']['available'] = ['x', 'z']
dims['time']['default'] = 'x'
dims['authors'] = {}
dims['authors']['available'] = ['x', 'y', 'z']
dims['authors']['default'] = 'y'
dims['booktitle'] = {}
dims['booktitle']['available'] = ['x', 'y', 'z']
dims['booktitle']['default'] = 'z'

################################################################################
# 2.- Create graph and index in Elasticsearch
import sys
sys.path.append('../')
from lib.hypergraph import Hypergraph
from lib.index import Index
import os, shutil

g = Hypergraph(dims, dataset_name)
index = Index(dataset_name)
index.delete()
index.create()

################################################################################
# 3.- Create directory to store the sources of the relationships
path = os.path.dirname(os.path.dirname(os.getcwd())) + '/hyperstorylines/static/hyperstorylines/datasets/sources/' + dataset_name + '/'

print(path)

try:
  #os.rmdir(path)
  shutil.rmtree(path)
except OSError:
  print('Cannot delete non existing directory')

try:
    os.mkdir(path)
except OSError:
    print ('Cannot create new directory')

################################################################################
# 4.- Load DBLP elements and add them to the graph
# This is the part to modify depending on your dataset

from pathlib import Path
import json
txt_folder = Path('./' + conf_name).rglob('*.json')
files = [x for x in txt_folder]

node_id = 0
edge_id = 0
existing_names = {}
for t in g.node_types:
    existing_names[t] = []

for f in files:
    with open(f, encoding='utf-8') as F:
        dblp_e = json.loads(F.read())
    
    # Filter a bit because DBLP is too big!
    if dblp_e['year'] is None: # or int(dblp_e['year']) < 2010:
    	continue

    shutil.copy(f, path)
    if dblp_e['title'] is not None and dblp_e['title'] != '':
      index.insert(f.name, dblp_e['title'])
        
    # A) change key 'year' for 'time'. 
    # Also all values of nodes must be strings
    dblp_e['time'] = str(dblp_e['year'])
    del dblp_e['year']
    
    source_id = f.name
    # B) Create new empty edge
    edge = g.get_empty_edge(edge_id)
    
    # C) Ensure more than one entity besides year
    n_entities = 0
    for t in g.node_types:
        if t == 'time':
            continue
        if not t in dblp_e:
            dblp_e[t] = []
        if isinstance(dblp_e[t], list):
            n_entities += len(dblp_e[t])
        elif dblp_e[t] != None and dblp_e[t] != '':
            n_entities += 1
    if n_entities <= 1:
        continue    
        
    # D) Create new node or lookup for existing one
    # and add it to the edge    
    for k in dblp_e:
        if k not in g.node_types or dblp_e[k] is None:
            continue
            
        d = dblp_e[k]
        if not isinstance(d, list):
            d = [d]            
        for element in d:
            if element not in existing_names[k]:
                node = g.get_empty_node(node_id, element, k)
                node.first_appearance = dblp_e['time']
                g.add_new_node(node)
                existing_names[k].append(element)
                node_id += 1
            else:
                node = g.get_node_name(element)
            edge.add_node(node)
        
    # E) Add new ede or update if it already existed
    edge_existed = g.add_or_update_new_edge(edge, source_id)    
    if not edge_existed:
        edge_id += 1

# 5.- Filter if desired and add relationship matrix
filter_exceptions = ['time', 'booktitle']
g.filter_nodes(200, filter_exceptions, 50)
g.update_nodes_edges()
g.build_relation_matrix()

# 6.- Export to file
g.export_to_json('../data/' + dataset_name + '.json')

# 7.- Copy to the Django app
shutil.copy('../data/' + dataset_name + '.json', '../../hyperstorylines/static/hyperstorylines/datasets/storylines/')