This folder contains the scripts and notebooks to generate a dataset in the format of Nested Storylines from the DBLP dataset. Make sure you have all the dependencies of pipenv installed and the environment activated.

1.- Download the dataset from: https://dblp.org/faq/1474681.html and unzip it here. The scripts were done using the release of February 2021: https://dblp.org/xml/release/dblp-2021-02-01.xml.gz

2.- Execute the following line to extract the types of publications:

```cat dblp.xml | awk '{if(substr($1,1,2)=="</"){split($1,a,">");print substr(a[1],3,length(a[1]))}}' | uniq | sort | uniq > tags.txt```

3.- Create a directory called ```processed``` and execute to parse the whole dataset:

```python dblp_parser.py```

This will create one json file per element in the DBLP database and store it in that folder.

4.- Create folders with subsets of the publications. Examples to do so are in the scripts `get_eurovis.py` and `get_vis.py`. We provide a extraction of the DBLP database for both

5.- Execute the following to go through all those created files, select part of them and convert them to a file in the format of Nested Storylines:

```python dblp_to_nsl.py```

In that script, modify the variable `conf_name` variable with the name of the folder of the subset you create

5.- In the parent folder, execute the following for the app to see the new dataset:

```python manage.py collectstatic```