This folder contains the scripts and notebooks to generate a dataset of publications from Inria HAl from teams ILDA and AVIZ. Inspired by the one from PAOH vis: 

https://www.aviz.fr/paohvis


1.- Execute the following to obtain the team per author:

`python get_team_members.py -ft list_hci.csv > teams_hci.csv`

Script created by Paola Valdivia, Thank you for her help!

2.- Execute the following to generate the dataset:

`python inria_to_nsl.py`

3.- In the parent folder, execute the following for the app to see the new dataset:

`python manage.py collectstatic`