
################################################################################
# Convert Inria HAL TO NSL
# 2021 Vanessa Pena-Araya
# Thanks to Paola Valdivia for the code to generate the dictionary
# of inria teams and authors
################################################################################


################################################################################
# 1.- Dimensions
# Create dimensions for the header of the file
# key: name of the dimension
# available: in which dimension will be possible to accesss it
# default: when loading the vis for the first time, in which dimension will it appear

dataset_name = 'hci-coauthorship_all'

dims = {}
dims['time'] = {}
dims['time']['available'] = ['x', 'z']
dims['time']['default'] = 'x'
dims['ILDA'] = {}
dims['ILDA']['available'] = ['x', 'y', 'z']
dims['ILDA']['default'] = 'y'
dims['AVIZ'] = {}
dims['AVIZ']['available'] = ['x', 'y', 'z']
dims['AVIZ']['default'] = 'z'
# dims['EX-SITU'] = {}
# dims['EX-SITU']['available'] = ['x', 'y', 'z']
# dims['EX-SITU']['default'] = ''
# dims['GRAPHDECO'] = {}
# dims['GRAPHDECO']['available'] = ['x', 'y', 'z']
# dims['GRAPHDECO']['default'] = ''
# dims['MFX'] = {}
# dims['MFX']['available'] = ['x', 'y', 'z']
# dims['MFX']['default'] = ''
# dims['LOKI'] = {}
# dims['LOKI']['available'] = ['x', 'y', 'z']
# dims['LOKI']['default'] = ''
# dims['ALICE'] = {}
# dims['ALICE']['available'] = ['x', 'y', 'z']
#dims['ALICE']['default'] = ''
other_teams = ['EX-SITU', 'ALICE', 'GRAPHDECO', 'HYBRID', 'IMAGINE', 'MANAO', 'MAVERICK', 
               'MIMETIC', 'MINT', 'POTIOC', 'TITANE', 'LOKI', 'MFX']
for t in other_teams:
  dims[t] = {}
  dims[t]['available'] = ['x', 'y', 'z']
  dims[t]['default'] = ''
#dims['Other'] = {}
#dims['Other']['available'] = ['x', 'y', 'z']
#dims['Other']['default'] = ''

################################################################################
# 2.- Create graph and index in Elasticsearch
import sys
sys.path.append('../')
from lib.hypergraph import Hypergraph
from lib.index import Index
import os, shutil

g = Hypergraph(dims, dataset_name)
index = Index(dataset_name)
index.delete()
index.create()

################################################################################
# 3.- Create directory to store the sources of the relationships
path = os.path.dirname(os.path.dirname(os.getcwd())) + '/hyperstorylines/static/hyperstorylines/datasets/sources/' + dataset_name + '/'

print(path)

try:
  #os.rmdir(path)
  shutil.rmtree(path)
except OSError:
  print('Cannot delete non existing directory')

try:
    os.mkdir(path)
except OSError:
    print ('Cannot create new directory')

################################################################################
# 4.- Load Inria HAL papers elements and add them to the graph
# This is the part to modify depending on your dataset

import requests
import json
import pandas as pd

# LOAD TEAMS DICTIONARY
# Authors with different teams get more than one node with different types
teams_names = pd.read_csv('teams_hci.csv', names=['last_name', 'first_name', 'team', 'year'], delimiter=';')
teams_names['full_name'] = teams_names['first_name'] + ' ' + teams_names['last_name']

def get_author_team(name, year):
    # Ugly hack because Pao has a lot of names!
    if name == 'Paola Valdivia':
      name = 'Paola Tatiana Llerena Valdivia'
    r = teams_names[(teams_names['full_name'] == name) & (teams_names['year'] == year)]
    if r.empty:
      return ''
    else:
      return r.iloc[0]['team'].upper()

#url = 'https://api.archives-ouvertes.fr/search/?q=rteamStructAcronym_s:(AVIZ%20OR%20ILDA%20OR%EX-SITU%20OR%POTIOC)&fq=producedDateY_i:[2005%20TO%202020]&fl=authFullName_s,rteamStructAcronym_s,title_s,journalTitle_s,conferenceTitle_s,producedDate_tdate,docid&start=0&rows=1000'
#response = json.loads(requests.get(url).text)

with open('api.archives-ouvertes_all.fr.json', 'r') as f:
  response = json.load(f)

node_id = 0
edge_id = 0
existing_names = {}
for t in g.node_types:
    existing_names[t] = []

for d in response['response']['docs']:
    date = d['producedDate_tdate'][:10]
    year = int(date[:4])
    source_id = str(d['docid'])

    # A) Create new edge and create time node
    edge = g.get_empty_edge(edge_id)
    edge.sources.append(source_id)

    if date not in existing_names['time']:
      node = g.get_empty_node(node_id, date, 'time')
      node.first_appearance = date
      g.add_new_node(node)
      existing_names['time'].append(date)
      node_id += 1
    else:
      node =  g.get_node_name_type(date, 'time')
    edge.add_node(node)

    # B) Create new node or lookup for existing one
    for name in d['authFullName_s']:
      team = get_author_team(name, year)
      #if team not in ['ILDA', 'AVIZ', 'EX-SITU', 'GRAPHDECO']:
      #  continue
      #if team == '' or team is None:
      #  team = 'Other'
      #  pass
      if team != '' and team is not None:
        team = team.upper()
        if name not in existing_names[team]:
          node = g.get_empty_node(node_id, name, team)
          node.first_appearance = date
          g.add_new_node(node)
          existing_names[team].append(name)
          node_id += 1
        else:
          node =  g.get_node_name_type(name, team)

        if node.first_appearance > date:
          node.first_appearance = date

        # C) Update edge nodes info
        edge.add_node(node)

    # E) Check if there was already an equal edge
    edge_existed = g.add_or_update_new_edge(edge, source_id)
    if not edge_existed:
      edge_id += 1

    # F) Index the paragraph
    index.insert(source_id, d['title_s'])
    with open(path + source_id, 'w') as f:
      json.dump(d, f)

    edge.sources = list(set(edge.sources))

# 5.- Filter if desired and add relationship matrix
#filter_exceptions = ['time']
#g.filter_nodes(50, filter_exceptions, 50)
g.update_nodes_edges()
g.build_relation_matrix()

# 6.- Export to file
g.export_to_json('../data/' + dataset_name + '.json')

# 7.- Copy to the Django app
shutil.copy('../data/' + dataset_name + '.json', '../../hyperstorylines/static/hyperstorylines/datasets/storylines/')
    