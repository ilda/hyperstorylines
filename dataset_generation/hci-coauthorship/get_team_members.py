#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 15:37:40 CEST 2018

@author: pao
"""

import sys
import argparse
from urllib.request import urlopen
from urllib.parse import quote
from urllib.error import HTTPError
import xml.etree.ElementTree as ET


def download_data(url):
    try:
        response = urlopen(url)
    except HTTPError:
        # print('Could not download page')
        return None
    else:
        return response.read()


def parse_xml(xml_data):
    team_members = []
    root = ET.fromstring(xml_data)
    for team in root.findall('team'):
        for member in team:
            first_name = member.find('firstname').text
            last_name = member.find('lastname').text
            team_members.append((first_name, last_name))
    return team_members


def get_data(url):
    xml_data = download_data(url)
    if xml_data:
        return parse_xml(xml_data)


def get_url(team, year=2017):
    url_f = lambda team_name, y: 'https://raweb.inria.fr/rapportsactivite/RA{}/{}/{}.xml'.format(y, team_name,
                                                                                                    team_name)
    if team == 'TAO':
        if year < 2017:
            return url_f('tao', str(year))
        else:
            return url_f('tau', str(year))
    else:
        return url_f(team.lower(), str(year))


def members_through_years(team, from_year, to_year):
    members_years = {}
    for year in range(from_year, to_year + 1):
        url = get_url(team, year)
        team_members = get_data(url)
        if team_members:
            members_years[year] = team_members
    return members_years


def join_members(members):
    list_members = []
    for _, m in members.items():
        list_members.extend(m)
    return list(set(list_members))


def print_list(members, team, joined=True):
    team = team.lower()
    if joined:
        for f_n, l_n in members:
            print("{};{};{}".format(l_n, f_n, team))
    else:
        for y, m in members.items():
            for f_n, l_n in m:
                print("{};{};{};{}".format(l_n, f_n, team, y))


def main(teams, from_year, to_year, join_them=True):
    for team in teams:
        members = members_through_years(team, from_year, to_year)
        if join_them:
            members = join_members(members)
        print_list(members, team, join_them)


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description='Get publications from HAL for a team.')
    arg_parser.add_argument('-team', dest='team', default='AVIZ', help='team name (default: AVIZ)')
    arg_parser.add_argument('-from', dest='from_year', default=2015, help='team name (default: 2015)')
    arg_parser.add_argument('-to', dest='to_year', default=2020, help='team name (default: 2020)')
    arg_parser.add_argument('-ft', dest='file_teams', default=None, help='teams names file')

    join = False

    args = arg_parser.parse_args()
    team = args.team
    from_year = int(args.from_year)
    to_year = int(args.to_year)

    teams = [team]
    teams_file = args.file_teams
    if teams_file:
        with open(teams_file) as f:
            teams = f.read().split(' ')

    main(teams, from_year, to_year, join)
