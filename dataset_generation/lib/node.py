################################################################
# Graph node class
# 2021 Vanessa Pena-Araya
################################################################


class Node:    
    def __init__(self, idcount, name, type, nodes_types):
        # Constants        
        self.id = (str(idcount));
        self.name = name
        self.type = type
        self.data = []
        self.init_related(nodes_types)
        self.first_appearance = None
    
    def init_related(self, nodes_types):
        self.related_edges = []
        self.related_nodes = {}
        for t in nodes_types:
            self.related_nodes[t] = set()
            
    def add_related_nodes(self, node_types_ids):
        for t, ids in node_types_ids.items():
            for i in ids:
                if i != self.id:
                    (self.related_nodes[t]).add(i)
            
    def get_n_complex_relation_nodes(self, exceptions):
        n = 0
        for t, ids in self.related_nodes.items():
            if t in exceptions:
                continue
            else:
                n += len(ids)
        return n
    
    def get_n_same_type_related_nodes(self):
        return len(self.related_nodes[self.type])

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Node):
            return self.id == other.id
        return False