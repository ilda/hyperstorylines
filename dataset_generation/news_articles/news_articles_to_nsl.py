################################################################################
# 1.- Dimensions
# Create dimensions for the header of the file
# key: name of the dimension
# available: in which dimension will be possible to accesss it
# default: when loading the vis for the first time, in which dimension will it appear

dataset_name = 'articles'

dims = {}
dims['time'] = {}
dims['time']['available'] = ['x', 'z']
dims['time']['default'] = 'x'
dims['people'] = {}
dims['people']['available'] = ['x', 'y', 'z']
dims['people']['default'] = 'y'
dims['locations'] = {}
dims['locations']['available'] = ['x', 'y', 'z']
dims['locations']['default'] = 'z'

################################################################################
# 2.- Create graph and index in Elasticsearch
import sys
sys.path.append('../')
from lib.hypergraph import Hypergraph
from lib.index import Index
import os, shutil

g = Hypergraph(dims, dataset_name)
index = Index(dataset_name)
index.delete()
index.create()

################################################################################
# 3.- Create directory to store the sources of the relationships
path = os.path.dirname(os.path.dirname(os.getcwd())) + '/hyperstorylines/static/hyperstorylines/datasets/sources/' + dataset_name + '/'

try:
  shutil.rmtree(path)
except OSError:
  print('Cannot delete non existing directory')

try:
    os.mkdir(path)
except OSError:
    print ('Cannot create new directory')

################################################################################
# 4.- Load DBLP elements and add them to the graph
# This is the part to modify depending on your dataset

import json

with open('news_articles.json', 'r') as f:
  data = json.load(f)

node_id = 0
edge_id = 0
existing_names = {}
for t in g.node_types:
    existing_names[t] = []

for source_id, publication in data.items():
  # A) Create new edge
  edge = g.get_empty_edge(edge_id)

  # B) Check that there is more than one entity besides time + compatibility
  publication['time'] = publication['metatime']

  n_entities = 0
  for et in g.node_types: 
    names = publication[et]                
    if type(names) is str: # metatime is string
      continue
    n_entities += len(names)
  if n_entities <= 1:
    continue

  # C) Create new node or lookup for existing one
  for et in g.node_types:
    names = publication[et]
    if type(names) is str: # Transforming a str into a list for compability
      names = [names]
    for name in names:
      if name not in existing_names[et]:
        node = g.get_empty_node(node_id, name, et)
        node.first_appearance = publication['time']
        g.add_new_node(node)
        existing_names[et].append(name)
        node_id += 1
      else:
        node =  g.get_node_name(name)
      
      if node.first_appearance > publication['time']:
        node.first_appearance = publication['time']
        
      # D) Update edge nodes info
      edge.add_node(node)

  # E) Check if there was already an equal edge
  edge_existed = g.add_or_update_new_edge(edge, source_id)
  if not edge_existed:
        edge_id += 1
  
  # F) Index the paragraph
  index.insert(source_id, publication['text'])
  with open(path + source_id, 'w') as f:
    json.dump(publication['text'], f)

# 5.- Filter if desired and add relationship matrix
#filter_exceptions = ['time']
#g.filter_nodes(50, filter_exceptions, 50)
g.update_nodes_edges()
g.build_relation_matrix()

# 6.- Export to file
g.export_to_json('../data/' + dataset_name + '.json')

# 7.- Copy to the Django app
shutil.copy('../data/' + dataset_name + '.json', '../../hyperstorylines/static/hyperstorylines/datasets/storylines/')
