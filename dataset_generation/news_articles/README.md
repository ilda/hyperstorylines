This folder contains the scripts and notebooks to generate a dataset in the format of Nested Storylines from a set of news articles. 

1.- Execute 

```python articles_to_nsl.py```

To convert the dataset articles.json to Nested Storylines format

2.- In the parent folder, execute the following for the app to see the new dataset:

```python manage.py collectstatic```